public class Offer10 {
    public int fib(int n) {
        int mod = (int) (1e9+7);
        int a = 0, b = 1, c = 0;
        if(n == 0){
            return a;
        }
        if(n == 1){
            return b;
        }
        for (int i = 2; i <= n; i++) {
            c = (a+b)%mod;
            a = b;
            b = c;
        }
        return c;
    }
}
