import java.util.Iterator;

class PeekingIterator implements Iterator<Integer> {
    private Iterator<Integer> it;
    private Integer nextValue;

    public PeekingIterator(Iterator<Integer> iterator) {
        // initialize any member here.
        it = iterator;
        nextValue = it.next();
    }

    // Returns the next element in the iteration without advancing the iterator.
    public Integer peek() {
        return nextValue;
    }

    // hasNext() and next() should behave the same as in the Iterator interface.
    // Override them if needed.
    @Override
    public Integer next() {
        Integer retValue = nextValue;
        nextValue = it.hasNext()?it.next():null;
        return retValue;
    }

    @Override
    public boolean hasNext() {
        return nextValue != null;
    }
}