public class Solution55 {
    public boolean canJump(int[] nums) {
        int maxReach = 0;
        for(int i = 0; i < nums.length && i <= maxReach; i++) {
            int tmpMaxReach = i + nums[i];
            maxReach = Math.max(maxReach, tmpMaxReach);
        }
        return maxReach >= nums.length - 1;
    }
}
