public class Solution1734 {
    public int[] decode(int[] encoded) {
        int n=encoded.length+1;
        for (int i = 1; i <= n; i++) {
            int[] arr=doDecode(encoded,i);
            if(arr[encoded.length]!=0){return arr;}
        }
        return new int[n];
    }

    public int[] doDecode(int[] encoded, int first) {
        int n=encoded.length+1;
        int[] arr=new int[n];
        arr[0]=first;
        for (int i = 0; i < encoded.length; i++) {
            int tmpNum=arr[i]^encoded[i];
            if(tmpNum>n||tmpNum<1){break;}
            arr[i+1]=tmpNum;
        }
        return arr;
    }
}
