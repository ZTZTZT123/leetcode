import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Solution448 {
    public List<Integer> findDisappearedNumbers(int[] nums) {
        List<Integer> res=new LinkedList<>();
        Boolean[] mark=new Boolean[nums.length+1];
        for (int i = 0; i < mark.length; i++) {
            mark[i]=false;
        }
        for (int num:nums) {
            mark[num]=true;
        }
        for (int i = 1; i < mark.length; i++) {
            if(mark[i]==false){
                res.add(i);
            }
        }
        return res;
    }
}
