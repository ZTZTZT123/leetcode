public class Solution1154 {

    private int[] monthDay = new int[]{0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334};

    private int[] leapMonthDay = new int[]{0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335};

    public int dayOfYear(String date) {
        int year = Integer.parseInt(date.substring(0, 4));
        int month = Integer.parseInt(date.substring(5, 7));
        int day = Integer.parseInt(date.substring(8));
        if(Integer.valueOf(year) % 4 == 0){
            return leapMonthDay[Integer.valueOf(month) - 1] + Integer.valueOf(day);
        }
        return monthDay[Integer.valueOf(month) - 1] + Integer.valueOf(day);
    }
}
