import java.util.Arrays;

public class Solution1658 {

//    class TargetWindow{
//        int left;
//        int right;
//        int windowsTmpNum;
//        int windowsTargetNum;
//        int[] nums;
//
//        public void init(int windowsTargetNum, int[] nums){
//            this.left = 0;
//            this.right = 0;
//            this.windowsTmpNum = 0;
//            this.windowsTargetNum = windowsTargetNum;
//            this.nums = nums;
//        }
//
//        public int getLeft() {
//            return left;
//        }
//
//        public int getRight() {
//            return right;
//        }
//
//        public boolean toNextTargetWindow(){
//            for(; right < nums.length; right++) {
//                windowsTmpNum += nums[right];
//                while(left <= right && windowsTmpNum > windowsTargetNum){
//                    windowsTmpNum -= nums[left++];
//                }
//                if(windowsTmpNum == windowsTargetNum){
//                    return true;
//                }
//            }
//            return false;
//        }
//
//    }
//    public int minOperations(int[] nums, int x) {
//        int sum = Arrays.stream(nums).reduce(0, (num1, num2) -> num1 + num2);
//        int windowsTarget = sum - x;
//        int targetWindowsMaxLen = -1;
//
//        TargetWindow window = new TargetWindow();
//
//        window.init(windowsTarget, nums);
//
//        while(window.toNextTargetWindow()){
//            targetWindowsMaxLen = Math.max(targetWindowsMaxLen, window.getRight() - window.getLeft() + 1);
//        }
//
//        return targetWindowsMaxLen == -1 ? -1 : nums.length - targetWindowsMaxLen;
//    }


    public int minOperations(int[] nums, int x) {
        int sum = Arrays.stream(nums).reduce(0, (num1, num2) -> num1 + num2);
        int windowsTarget = sum - x;
        int targetWindowsMaxLen = -1;

        int windowsNum = 0;
        for(int left = 0, right = 0; right < nums.length; right++) {
            windowsNum += nums[right];
            while(left <= right && windowsNum > windowsTarget){
                windowsNum -= nums[left++];
            }
            if(windowsNum == windowsTarget){
                targetWindowsMaxLen = Math.max(targetWindowsMaxLen, right - left + 1);
            }
        }
        return targetWindowsMaxLen == -1 ? -1 : nums.length - targetWindowsMaxLen;
    }

    public static void main(String[] args){
        Solution1658 s = new Solution1658();
        s.minOperations(new int[]{5,2,3,1,1},
        5);
    }
}
