public class Solution1455 {

    private boolean isPrefix(String sentence, int left, int right, String searchWord){
        if(right - left + 1 < searchWord.length()){
            return false;
        }
        for (int i = 0; i < searchWord.length(); i++) {
            if(sentence.charAt(left + i) != searchWord.charAt(i)){
                return false;
            }
        }
        return true;
    }

    public int isPrefixOfWord(String sentence, String searchWord) {
        sentence += ' ';
        int left = 0, right = 0, ind = 1;
        for ( ; right < sentence.length(); right++) {
            if(sentence.charAt(right) == ' '){
                if(isPrefix(sentence, left, right - 1, searchWord)){
                    return ind;
                }
                left = right + 1;
                ind++;
            }
        }
        return -1;
    }
}
