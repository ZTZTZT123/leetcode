import java.util.*;

public class Solution1438 {
    public int longestSubarray(int[] nums, int limit) {
        int result=0;
        int left=0;
        int right=0;
        SortedSet<Integer> tmpNums=new TreeSet<>();
        while(right< nums.length){
            int tmp=nums[right];
            tmpNums.add(tmp);
            while(tmpNums.last()-tmpNums.first()>limit){
                int eraseTmp=nums[left];
                tmpNums.remove(eraseTmp);
                left++;
            }
            result=Math.max(result,right-left+1);
            right++;
        }
        return result;
    }
}
