import java.util.List;

public class Solution385 {
    class NestedInteger {

        NestedInteger(){}

        NestedInteger(int num){}

        boolean isInteger(){}

        Integer getInteger(){}

        void setInteger(int value){}

        void add(NestedInteger ni){}

        List<NestedInteger> getList(){}
    }

    int index = 0;

    public NestedInteger deserialize(String s) {
        if(s.charAt(index) == '['){
            index++;
            NestedInteger nestedInteger = new NestedInteger();
            while(s.charAt(index) != ']'){
                nestedInteger.add(deserialize(s));
                if(s.charAt(index) == ','){
                    index++;
                }
            }
            index++;
            return nestedInteger;
        }
        else{
            boolean isNeg = false;
            int num = 0;
            if(s.charAt(index) == '-'){
                isNeg = true;
                index++;
            }
            while(index < s.length()){
                char numC = s.charAt(index);
                if(numC == ',' || numC == ']'){
                    break;
                }
                num = num * 10 + (numC - '0');
                index++;
            }
            return new NestedInteger(isNeg ? -num : num);
        }
    }
}
