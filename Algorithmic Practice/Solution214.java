public class Solution214 {
    public boolean doJudge(String s){
        int n=s.length();
        for (int i = n/2; i < n; i++) {
            if(s.charAt(i)!=s.charAt(n-i-1)){
                return false;
            }
        }    
        return true;
    }
    
    public String shortestPalindrome(String s) {
        if(doJudge(s)){
            return s;
        }
        StringBuilder sb=new StringBuilder(s);
        for (int i = 0; i < s.length(); i++) {
            char tmp=s.charAt(s.length()-1-i);
            sb.insert(i,tmp);
            if(doJudge(sb.toString())){
                break;
            }
        }
        return sb.toString();
    }
}
