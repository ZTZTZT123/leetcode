public class Solution482 {
    public String licenseKeyFormatting(String s, int k) {
        StringBuilder res = new StringBuilder();
        int len = 0;
        for (int i = s.length()-1; i >=0; i--) {
            char tmpC = s.charAt(i);
            if(tmpC == '-'){
                continue;
            }
            res.append(Character.toUpperCase(tmpC));
            len++;
            if(len % k == 0 ){
                res.append('-');
            }
        }
        if (res.length() > 0 && res.charAt(res.length() - 1) == '-') {
            res.deleteCharAt(res.length() - 1);
        }
        return res.reverse().toString();
    }

    public static void main(String[] args) {
        Solution482 s =new Solution482();
        System.out.println(s.licenseKeyFormatting("--a-a-a-a--",
                2));
    }
}
