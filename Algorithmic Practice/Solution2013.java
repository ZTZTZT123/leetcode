class DetectSquares {

    private int[][] plane;

    private final int sideLen = 1001;

    private int doCount2(final int[] point1){
        int x = point1[0], y = point1[1];
        int[] row = plane[y];
        int squareCount = 0;
        for (int tmpX = 0; tmpX < row.length; tmpX++) {
            if(tmpX == x){
                continue;
            }
            int count = row[tmpX];
            if(count > 0){
                squareCount += doCount3(point1, new int[]{tmpX, y, count});
            }
        }
        return squareCount;
    }

    private int doCount3(final int[] point1, final int[] point2){
        int x2 = point2[0], y2 = point2[1];
        int len = Math.abs(point1[0] - x2);
        int squareCount = 0;
        if(y2 + len < plane.length && plane[y2 + len][x2] > 0) {
            squareCount += doCount4(point1, point2, new int[]{x2, y2 + len, plane[y2 + len][x2]});
        }
        if(y2 - len >= 0 && plane[y2 - len][x2] > 0){
            squareCount += doCount4(point1, point2, new int[]{x2, y2 - len, plane[y2 - len][x2]});
        }
        return squareCount;
    }

    private int doCount4(final int[] point1, final int[] point2, final int[] point3){
        return plane[point3[1]][point1[0]] * point3[2] * point2[2] * point1[2];
    }

    public DetectSquares() {
        plane = new int[sideLen][sideLen];
    }

    public void add(int[] point) {
        plane[point[1]][point[0]]++;
    }

    public int count(int[] point) {
        return doCount2(new int[]{point[0], point[1], 1});
    }
}

/**
 * Your DetectSquares object will be instantiated and called as such:
 * DetectSquares obj = new DetectSquares();
 * obj.add(point);
 * int param_2 = obj.count(point);
 */