public class Solution383 {

    private int[] getCount(String str){
        int[] count = new int[26];
        for (int i = 0; i < str.length(); i++) {
            count[str.charAt(i) - 'a']++;
        }
        return count;
    }

    public boolean canConstruct(String ransomNote, String magazine) {
        int[] noteCount = getCount(ransomNote), magCount = getCount(magazine);
        for (int i = 0; i < noteCount.length; i++) {
            if(noteCount[i] > magCount[i]){
                return false;
            }
        }
        return true;
    }
}
