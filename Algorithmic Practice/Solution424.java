import java.util.*;

public class Solution424 {
    class Mark{
        Map<Character,Integer> mark;
        int sum;

        public Mark(Map<Character, Integer> mark) {
            this.mark = mark;
        }

        public char getMostChar(){
            char mostKey='0';
            int mostCount=0;
            for (Map.Entry<Character,Integer> et: mark.entrySet()) {
                if(et.getValue()>mostCount){
                    mostCount=et.getValue();
                    mostKey=et.getKey();
                }
            }
            return mostKey;
        }

        public int getAllCount(){
            return sum;
        }

        public int getOtherCount(){
            return sum-mark.get(getMostChar());
        }
    }



    public int characterReplacement(String s, int k) {
        int left=0;
        int right=0;
        int result=0;
        Mark mk=new Mark(new HashMap<>());
        while(right<s.length()){
            char tmpChar=s.charAt(right);
            if(mk.mark.containsKey(tmpChar)==false){
                mk.mark.put(tmpChar,1);
                mk.sum++;
            }
            else{
                mk.mark.put(tmpChar,mk.mark.get(tmpChar)+1);
                mk.sum++;
            }
            while(mk.getOtherCount()>k){
                char tmpC=s.charAt(left);
                mk.mark.put(tmpC, mk.mark.get(tmpC)-1);
                mk.sum--;
                left++;
            }
            int tmpCount=mk.getAllCount();
            result=tmpCount>result? tmpCount:result;
            right++;
        }
        return result;
    }

    public static void main(String[] args) {
        Solution424 s=new Solution424();
        System.out.println(s.characterReplacement("ABAB",
                2));
    }
}
