public class Solution713 {
    public int numSubarrayProductLessThanK(int[] nums, int k){
        int count = 0, tmpProduct = 1;
        for(int left = 0, right = 0; right < nums.length; right++){
            tmpProduct *= nums[right];
            while(left < right && tmpProduct >= k){
                tmpProduct /= nums[left++];
            }
            if(tmpProduct < k){
                count += (right - left + 1);
            }
        }
        return count;
    }

    public static void main(String[] args) {
        Solution713 s = new Solution713();
        s.numSubarrayProductLessThanK(new int[]{10, 5, 2, 6}, 100);
    }
}
