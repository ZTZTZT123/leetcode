import java.util.Arrays;public class Solution6 {

    public String convert(String s, int numRows) {
        if(numRows == 1){
            return s;
        }

        StringBuilder[] sbArr = new StringBuilder[numRows];
        for(int i = 0; i < sbArr.length; i++) {
            sbArr[i] = new StringBuilder();
        }

        int tmpRow = 0, flag = 1;
        for(int i = 0; i < s.length(); i++, tmpRow += flag) {
            sbArr[tmpRow].append(s.charAt(i));
            if((flag == 1 && tmpRow == numRows - 1) || (flag == -1 && tmpRow == 0)){
                flag = -flag;
            }
        }

        return Arrays.stream(sbArr).reduce(new StringBuilder(), (a, b) -> a.append(b)).toString();
    }


    public static void main(String[] args){
        Solution6 s = new Solution6();
        System.out.println(s.convert("AB", 1));
    }

}
