public class Solution221 {

    public int maximalSquare(char[][] matrix) {
        int m = matrix.length, n = matrix[0].length;
        int[][] dpInfo = new int[m + 1][n + 1];
        int maxSideLen = 0;
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if(matrix[i - 1][j - 1] == '1'){
                    int tmpDpNum = Math.min(dpInfo[i - 1][j - 1], Math.min(dpInfo[i][j - 1], dpInfo[i - 1][j])) + 1;
                    maxSideLen = Math.max(tmpDpNum, maxSideLen);
                    dpInfo[i][j] = tmpDpNum;
                }
            }
        }
        return maxSideLen * maxSideLen;
    }
}
