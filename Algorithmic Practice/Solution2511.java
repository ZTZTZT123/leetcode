import java.util.TreeMap;
import java.util.TreeSet;

public class Solution2511 {

    public int captureForts(int[] forts) {
        int res = 0;

        int lastEmpty = -1, lastControl = -1;

        for (int i = 0; i < forts.length; i++) {
            int val = forts[i];
            if(val == -1){
                if(lastControl != -1){
                    res = Math.max(res, i - lastControl - 1);
                    lastControl = -1;
                }
                lastEmpty = i;
            } else if(val == 1){
                if(lastEmpty != -1){
                    res = Math.max(res, i - lastEmpty - 1);
                    lastEmpty = -1;
                }
                lastControl = 1;
            }
        }

        return res;
    }
}
