package com.company;

import java.util.HashMap;

public class Solution205 {

    public boolean isIsomorphic(String s, String t) {
        HashMap<Character,Integer> s_mark= new HashMap<>();
        int[] s_vec=new int[s.length()];
        for (int i = 0; i < s.length(); i++) {
            if(s_mark.get(s.charAt(i))==null){
                s_mark.put(s.charAt(i),i);
            }
            s_vec[i]=s_mark.get(s.charAt(i));
        }
        HashMap<Character,Integer> t_mark= new HashMap<>();
        int[] t_vec=new int[t.length()];
        for (int i = 0; i < t.length(); i++) {
            if(t_mark.get(t.charAt(i))==null){
                t_mark.put(t.charAt(i),i);
            }
            t_vec[i]=t_mark.get(t.charAt(i));
        }
        for (int i = 0; i < s.length(); i++) {
            if(s_vec[i]!=t_vec[i]){
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
	    Solution205 s=new Solution205();
        System.out.println(s.isIsomorphic("asbg","asdd"));
    }
}
