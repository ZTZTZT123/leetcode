package com.company;
import java.util.*;

public class Solution1046 {
    public int lastStoneWeight(int[] stones) {
        List<Integer> stoneList= new ArrayList<Integer>();
        for(int i=0;i<stones.length;i++){
            stoneList.add(stones[i]);
        }
        stoneList.sort(Comparator.naturalOrder());
        while(stoneList.size()>1){
            int x=stoneList.get(stoneList.size()-2);
            int y=stoneList.get(stoneList.size()-1);
            stoneList.remove(stoneList.size()-1);
            stoneList.remove(stoneList.size()-1);
            if(x==y){
                continue;
            }
            stoneList.add(y-x);
            stoneList.sort(Comparator.naturalOrder());
        }
        if(stoneList.size()==1)
            return stoneList.get(0);
        return 0;
    }
}
