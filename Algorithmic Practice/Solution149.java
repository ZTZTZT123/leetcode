public class Solution149 {
    public int maxPoints(int[][] points) {
        int res=1;
        for (int i = 0; i < points.length; i++) {
            int[] pointI=points[i];
            for (int j = i+1; j < points.length; j++) {
                int[] pointJ=points[j];
                int count=2;
                for (int k = j+1; k < points.length; k++) {
                    int[] pointK=points[k];
                    int k1=(pointK[1]-pointI[1])*(pointJ[0]-pointI[0]);
                    int k2=(pointJ[1]-pointI[1])*(pointK[0]-pointI[0]);
                    if(k1==k2){
                        count++;
                    }
                }
                res=Math.max(res,count);
            }
        }
        return res;
    }
}
