public class Solution132 {
    public boolean isPalindromic(String str){
        int tvLen=str.length()/2;
        for (int i = 0; i < tvLen; i++) {
            char left=str.charAt(i);
            char right=str.charAt(str.length()-1-i);
            if(left!=right){
                return false;
            }
        }
        return true;
    }

    public int minCut(String s) {
        int N=s.length();
        int[] DP=new int[N];
        for (int i = 0; i < DP.length; i++) {
            DP[i]=N;
        }
        for (int i = 0; i < N; i++) {
            if(isPalindromic(s.substring(0,i+1))){
                DP[i]=0;
                continue;
            }
            for (int j = 0; j < i; j++) {
                if(isPalindromic(s.substring(j+1,i+1))){
                    DP[i]=Math.min(DP[i],DP[j]+1 );
                }
            }
        }
        return DP[N-1];
    }
}
