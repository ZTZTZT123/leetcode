import java.util.ArrayList;
import java.util.List;

public class Solution969 {

    private int findMaxNumInd(int[] arr, int start, int end){
        int maxInd = -1, maxNum = Integer.MIN_VALUE;
        for(int i = start; i <= end; i++){
            int tmpNum = arr[i];
            if(tmpNum > maxNum){
                maxInd = i;
                maxNum = tmpNum;
            }
        }
        return maxInd;
    }


    private void reverseArr(int[] arr, int start, int end){
        for( ; start < end; start++, end--){
            int tmpNum = arr[start];
            arr[start] = arr[end];
            arr[end] = tmpNum;
        }
    }


    public List<Integer> pancakeSort(int[] arr) {
        List<Integer> res = new ArrayList<>();
        int len = arr.length;
        for(int end = len - 1; end > 0; end--) {
            int tmpMaxInd = findMaxNumInd(arr, 0, end);
            reverseArr(arr, 0, tmpMaxInd);
            res.add(tmpMaxInd + 1);
            reverseArr(arr, 0, end);
            res.add(end + 1);
        }
        return res;
    }
}
