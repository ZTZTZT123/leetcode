public class Solution38 {

    private String getDescribe(String str){
        char lastC = str.charAt(0);
        int repeatTime = 1;
        StringBuilder sb = new StringBuilder();
        for (int i = 1; i < str.length(); i++, repeatTime++) {
            char tmpC = str.charAt(i);
            if(lastC != tmpC){
                sb.append(String.valueOf(repeatTime) + lastC);
                repeatTime = 0;
            }
            lastC = tmpC;
        }
        sb.append(String.valueOf(repeatTime) + lastC);
        return sb.toString();
    }

    public String countAndSay(int n) {
        String lastRes = "1";
        for (int i = 2; i <= n; i++){
            lastRes = getDescribe(lastRes);
        }
        return lastRes;
    }

    public static void main(String[] args) {
        Solution38 s = new Solution38();
        System.out.println(s.countAndSay(4));
    }
}
