public class Solution807 {
    public int maxIncreaseKeepingSkyline(int[][] grid) {
        int m = grid.length, n = grid[0].length;
        int[] rowMaxArr = new int[m], colMaxArr = new int[n];
        for (int i = 0; i < m; i++) {
            int[] tmpRow = grid[i];
            for (int j = 0; j < n; j++) {
                int tmpNum = tmpRow[j];
                rowMaxArr[i] = Math.max(rowMaxArr[i], tmpNum);
                colMaxArr[j] = Math.max(colMaxArr[j], tmpNum);
            }
        }

        int increase = 0;

        for (int i = 0; i < m; i++) {
            int[] tmpRow = grid[i];
            for (int j = 0; j < n; j++) {
                int tmpNum = tmpRow[j];
                increase += Math.min(rowMaxArr[i], colMaxArr[j]) - tmpNum;
            }
        }

        return increase;
    }
}
