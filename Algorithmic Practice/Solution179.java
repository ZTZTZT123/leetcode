import java.util.Arrays;
import java.util.Comparator;

public class Solution179 {
    public String largestNumber(int[] nums) {
        String[] strs=new String[nums.length];
        for (int i = 0; i < nums.length; i++) {
            strs[i]=String.valueOf(nums[i]);
        }
        Arrays.sort(strs, new Comparator<String>() {
            @Override
            public int compare(String s, String t1) {
                return (t1+s).compareTo(s+t1);
            }
        });
        StringBuilder sb=new StringBuilder();
        boolean flag=false;
        for (int i = 0; i < strs.length; i++) {
            String tmpStr=strs[i];
            if(tmpStr.equals("0")==false||flag) {
                sb.append(tmpStr);
                flag=true;
            }
        }
        String res=sb.toString();
        return res.equals("")? "0":res;
    }
}
