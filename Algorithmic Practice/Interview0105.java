public class Interview0105 {

    private boolean lenEquJudge(String target, String str){
        int n = target.length();
        boolean isDiff = false;
        for(int i = 0; i < n; i++){
            if(target.charAt(i) != str.charAt(i)){
                if(isDiff){
                    return false;
                }
                isDiff = true;
            }
        }
        return true;
    }

    private boolean lenDiffJudge(String target, String str){
        int n = str.length();
        int diff = 0;
        for (int i = 0; i < n; i++) {
            if(target.charAt(i + diff) != str.charAt(i)){
                if(diff == 1){
                    return false;
                }
                i--;
                diff = 1;
            }
        }
        return true;
    }

    public boolean oneEditAway(String first, String second) {
        int firstLen = first.length(), secondLen = second.length();
        if(firstLen == secondLen){
            return lenEquJudge(first, second);
        }
        else if(firstLen == secondLen + 1){
            return lenDiffJudge(first, second);
        }
        else if(secondLen == firstLen + 1){
            return lenDiffJudge(second, first);
        }
        else{
            return false;
        }
    }

    public static void main(String[] args) {
        Interview0105 i = new Interview0105();
        i.oneEditAway("teacher",
                "bleacher");
    }
}
