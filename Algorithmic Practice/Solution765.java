import java.util.HashMap;

public class Solution765 {
    private class UnionUtil{
        HashMap<Integer,Integer> Util;
        int Count;
        UnionUtil(){
            Util=new HashMap<>();
            Count=0;
        }
        int getCount(){
            return Count;
        }
        int find(int element){
            if(Util.containsKey(element)==false){
                Util.put(element,element);
                Count++;
            }
            if(Util.get(element)!=element){
                Util.put(element,find(Util.get(element)));
            }
            return Util.get(element);
        }
        boolean union(int x,int y){
            int rootx=find(x);
            int rooty=find(y);
            if(rootx==rooty){
                return false;
            }
            Util.put(rootx,rooty);
            Count--;
            return true;
        }
    }

    public int minSwapsCouples(int[] row) {
        int N= row.length/2;
        UnionUtil util=new UnionUtil();
        for (int i = 0; i < row.length-1; i++) {
            util.union(row[i]/2,row[i+1]/2 );
        }
        return N- util.Count;
    }

}
