class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode() {}
    TreeNode(int val) { this.val = val; }
    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}

public class Solution563 {

    int res = 0;

    private int travel(TreeNode tmpNode){
        if(tmpNode == null){
            return 0;
        }
        int leftSum = travel(tmpNode.left);
        int rightSum = travel(tmpNode.right);
        res += Math.abs((leftSum - rightSum));
        return leftSum + rightSum + tmpNode.val;
    }

    public int findTilt(TreeNode root) {
        travel(root);
        return res;
    }
}
