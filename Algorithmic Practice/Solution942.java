public class Solution942 {



    public int[] diStringMatch(String s) {
        int n = s.length(), low = 0, high = n;
        int[] res = new int[n + 1];
        for(int i = 0; i < s.length(); i++){
            res[i] = s.charAt(i) == 'D' ? high-- : low++;
        }
        res[n] = low;
        return res;
    }
}
