public class Solution495 {
    public int findPoisonedDuration(int[] timeSeries, int duration) {
        int last = timeSeries[0] + duration - 1, res = duration;
        for (int i = 1; i < timeSeries.length; i++) {
            int tmpStart = timeSeries[i];
            res += duration;
            if(tmpStart <= last)
                res -= (last - tmpStart + 1);
            last = tmpStart + duration - 1;
        }
        return res;
    }
}
