import java.util.TreeSet;

public class Solution1818 {
    public int minAbsoluteSumDiff(int[] nums1, int[] nums2) {
        int mod = (int) (1e9+7);
        int maxSub = 0;
        int res = 0;
        TreeSet treeSet = new TreeSet();
        for (int tmpNum:nums1) {
            treeSet.add(tmpNum);
        }
        for (int i = 0; i < nums1.length; i++) {
            int tmp1 = nums1[i];
            int tmp2 = nums2[i];
            int tmpAbs = Math.abs(tmp1-tmp2);
            Integer floor = (Integer) treeSet.floor(tmp2);
            Integer ceiling = (Integer) treeSet.ceiling(tmp2);
            int minAbs = Integer.MAX_VALUE;
            if(floor!=null&&ceiling!=null){
                minAbs = Math.min(Math.abs(floor-tmp2),Math.abs(ceiling-tmp2));
            }
            else if(floor!=null&&ceiling==null){
                minAbs = Math.abs(floor-tmp2);
            }
            else if(floor==null&&ceiling!=null){
                minAbs = Math.abs(ceiling-tmp2);
            }
            maxSub = Math.max(maxSub,tmpAbs-minAbs);
            res = (res+tmpAbs)%mod;
        }
        return ((res-maxSub+mod)%mod);
    }

    public static void main(String[] args) {
        Solution1818 s = new Solution1818();
        System.out.println(s.minAbsoluteSumDiff(new int[]{1,7,5},new int[]{2,3,5}));
    }
}
