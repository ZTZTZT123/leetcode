import java.util.HashSet;
import java.util.Objects;
import java.util.Random;
import java.util.Set;

public class Solution519 {

    BitMap[] map;
    Random random;
    int m;
    int n;

    public Solution519(int m, int n) {
        map = new BitMap[m];
        random = new Random();
        this.m = m;
        this.n = n;
    }

    public int[] flip() {
        int rowInd, colInd;
        while(true){
            rowInd = random.nextInt(m);
            colInd = random.nextInt(n);
            BitMap tmpRow = map[rowInd];
            if(tmpRow == null){
                tmpRow = new BitMap(n);
                map[rowInd] = tmpRow;
            }
            if(!tmpRow.contains(colInd)){
                tmpRow.put(colInd);
                break;
            }
        }
        return new int[]{rowInd,colInd};
    }

    public void reset() {
        map = new BitMap[m];
    }

    public static void main(String[] args) {
        Solution519 s = new Solution519(3,1);
        for (int i = 0; i < 5; i++) {
            s.flip();
        }
    }
}
