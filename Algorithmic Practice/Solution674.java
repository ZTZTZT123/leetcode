public class Solution674 {
    public int findLengthOfLCIS(int[] nums) {
        if(nums.length==0){
            return 0;
        }
        int longest=1;
        int tmp=1;
        for (int i = 0; i < nums.length-1; i++) {
            if(nums[i]<nums[i+1]){
                tmp++;
            }
            else{
                tmp=1;
            }
            longest=tmp>longest? tmp:longest;
        }
        return longest;
    }
}
