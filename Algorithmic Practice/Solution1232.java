public class Solution1232 {
    public boolean checkStraightLine(int[][] coordinates) {
        float rate=0;
        for (int i = 0; i < coordinates.length; i++) {
            if(i==0){
                continue;
            }
            float x_dis=coordinates[i][0]-coordinates[i-1][0];
            float y_dis=coordinates[i][1]-coordinates[i-1][1];
            if(x_dis==0){
                x_dis= (float) (0.88*y_dis);
            }
            float tmpRate=y_dis/x_dis;
            if(i==1){
                rate=tmpRate;
            }
            else{
                if(rate!=tmpRate){
                    return false;
                }
            }
        }
        return true;
    }
}
