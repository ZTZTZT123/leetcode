class Solution {
    public boolean isUgly(int num) {
        if (num == 0) return false; // 特别判断
        while (true) {
            if (num == 1) return true; // 当num等于1时，代表这个数的质因数只包含2,3,5。那么为true
            if (num % 2 == 0) num /= 2; // 如果num中含有2的质因数，那么把它抽出来
            else if(num % 3 == 0) num /= 3; // 如果num中含有3的质因数，那么把它抽出来
            else if(num % 5 == 0) num /= 5; // 如果num中含有5的质因数，那么把它抽出来
            else return false; // 不是丑数，返回false
        }
    }
}