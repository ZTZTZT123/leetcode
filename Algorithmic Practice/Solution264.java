import java.awt.*;
import java.util.*;
import java.util.List;

public class Solution264 {

    public void add(Set<Long> mark,PriorityQueue<Long> pq,Long n){
        if(!mark.contains(n)){
            pq.add(n);
            mark.add(n);
        }
    }

    public int nthUglyNumber(int n) {
        PriorityQueue<Long> pq = new PriorityQueue<Long>();
        Set<Long> mark=new HashSet<>();
        add(mark,pq, (long) 1);
        for (int i = 0; i < n; i++) {
            long tmp=pq.poll();
            if(i==n-1){
                return (int) tmp;
            }
            add(mark,pq,tmp*2);
            add(mark,pq,tmp*3);
            add(mark,pq,tmp*5);
        }
        return -1;
    }

    public static void main(String[] args) {
        Solution264 s=new Solution264();
        System.out.println(s.nthUglyNumber(10));
    }
}
