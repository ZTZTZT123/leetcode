public class Solution718 {
    public int findLength(int[] nums1, int[] nums2) {
        int[][] dp = new int[nums1.length][nums2.length];
        int res = 0;
        for (int i = 0; i < nums1.length; i++) {
            for (int j = 0; j < nums2.length; j++) {
                if(nums1[i] == nums2[j]){
                    int lastNum = (i > 0 && j > 0)? dp[i-1][j-1]:0;
                    int tmpNum = lastNum + 1;
                    res = Math.max(tmpNum, res);
                    dp[i][j] = tmpNum;
                }
            }
        }
        return res;
    }

    public static void main(String[] args) {
        Solution718 s = new Solution718();
        s.findLength(new int[]{1,2,3,2,1}, new int[]{3,2,1,4,7});
    }
}
