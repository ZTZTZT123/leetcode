import sun.reflect.generics.tree.Tree;

public class Solution700 {

    private TreeNode findNode(TreeNode node, int val){
        if(node == null){
            return null;
        }
        if(node.val == val){
            return node;
        }
        else if(node.val < val){
            return findNode(node.right, val);
        }
        else{
            return findNode(node.left, val);
        }
    }

    public TreeNode searchBST(TreeNode root, int val) {
        return findNode(root, val);
    }
}
