import java.util.HashSet;
import java.util.Set;

public class Solution73 {
    public int[][] deepCopy(int[][] matrix){
        int m=matrix.length;
        int n=matrix[0].length;
        int[][] newMatrix=new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                newMatrix[i][j]=matrix[i][j];
            }
        }
        return newMatrix;
    }

    public void makeRowZero(int[][] matrix,int row){
        for (int i = 0; i < matrix[0].length; i++) {
            matrix[row][i]=0;
        }
    }

    public void makeColZero(int[][] matrix,int col){
        for (int i = 0; i < matrix.length; i++) {
            matrix[i][col]=0;
        }
    }

    public void setZeroes(int[][] matrix) {
        int[][] oldMatrix=deepCopy(matrix);
        Set<Integer> markCols=new HashSet<>();
        for (int i = 0; i < oldMatrix.length; i++) {
            for (int j = 0; j < oldMatrix[i].length; j++) {
                if(oldMatrix[i][j]==0){
                    makeRowZero(matrix,i);
                    if(markCols.contains(j)==false){
                        makeColZero(matrix,j);
                        markCols.add(j);
                    }
                }
            }
        }
    }

    public static void main(String[] args) {
        Solution73 s=new Solution73();
        s.setZeroes(new int[][]{{0,1}});
    }
}
