import java.util.List;

public class Solution650 {
    int minStep = Integer.MAX_VALUE;

    private void travel(int step, int tmpNum, final int target, int lastCopy){
        if(step >= minStep){
            return;
        }
        if(tmpNum >= target) {
            if (tmpNum == target) {
                minStep = step;
            }
            return;
        }
        travel(step+1, tmpNum+lastCopy, target, lastCopy);
        travel(step+2, tmpNum+tmpNum, target, tmpNum);
    }

    public int minSteps(int n) {
        if(n == 1){
            return 0;
        }
        travel(1, 1, n, 1);
        return minStep;
    }

    public static void main(String[] args) {
        Solution650 s = new Solution650();
        System.out.println(s.minSteps(5));
    }
}
