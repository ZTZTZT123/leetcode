public class Solution598 {
    public int maxCount(int m, int n, int[][] ops) {
        Integer X_min=m;
        Integer Y_min=n;
        for (int i = 0; i < ops.length; i++) {
            int tmpX=ops[i][0];
            int tmpY=ops[i][1];
            X_min=X_min>tmpX? tmpX:X_min;
            Y_min=Y_min>tmpY? tmpY:Y_min;
        }
        return X_min*Y_min;
    }
}
