public class Solution1744 {
    public long[] getPrefix(int[] candiesCount){
        long[] prefix=new long[candiesCount.length];
        long sum=0;
        for (int i = 0; i < candiesCount.length; i++) {
            sum+=candiesCount[i];
            prefix[i]=sum;
        }
        return prefix;
    }

    public boolean doCanEat(long[] prefix,int[] query){
        long leftEdge=query[1]+1;
        long rightEdge=(long)leftEdge*query[2];
        int favType=query[0];
        long ndLeft=(favType==0? 0:prefix[favType-1])+1;
        long ndRight=prefix[favType];
        return (leftEdge<=ndRight&&rightEdge>=ndLeft);
    }

    public boolean[] canEat(int[] candiesCount, int[][] queries) {
        long[] prefix=getPrefix(candiesCount);
        boolean[] res=new boolean[queries.length];
        for (int i = 0; i < queries.length; i++) {
            res[i]=doCanEat(prefix,queries[i]);
        }
        return res;
    }

    public static void main(String[] args) {
        Solution1744 s=new Solution1744();
        System.out.println(s.canEat(new int[]{7,4,5,3,8},
new int[][]{{0,2,2},{4,2,4},{2,13,1000000000}}));
    }
}
