import java.util.HashMap;
import java.util.Map;

class TopVotedCandidate {

    int[][] searchArr;

    private int binarySearch(int time){
        int left = 0, right = searchArr.length - 1;
        while(left <= right){
            int mid = left + (right - left) / 2;
            int midTime = searchArr[mid][0];
            if(midTime == time){
                return searchArr[mid][1];
            }
            else if(midTime > time){
                right = mid - 1;
            }
            else{
                left = mid + 1;
            }
        }
        return searchArr[right][1];
    }

    public TopVotedCandidate(int[] persons, int[] times) {
        int n = persons.length;
        searchArr = new int[n][2];
        int[] counter = new int[n];
        searchArr[0] = new int[]{times[0], persons[0]};
        counter[persons[0]] = 1;
        int leadPerson = persons[0], leadCount = 1;
        for (int i = 1; i < n; i++) {
            int tmpPerson = persons[i], tmpTime = times[i];
            int newCount = counter[tmpPerson] + 1;
            counter[tmpPerson] = newCount;
            if(leadCount <= newCount){
                leadCount = newCount;
                leadPerson = tmpPerson;
            }
            searchArr[i] = new int[]{tmpTime, leadPerson};
        }
    }

    public int q(int t) {
        return binarySearch(t);
    }
}
