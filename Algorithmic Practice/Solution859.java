import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Solution859 {
    public boolean buddyStrings(String s, String goal) {
        if(s.length() != goal.length()){
            return false;
        }
        List<Integer> diffInd = new ArrayList<>();
        for (int i = 0; i < s.length(); i++) {
            if(s.charAt(i) != goal.charAt(i)){
                diffInd.add(i);
                if(diffInd.size() > 2){
                    return false;
                }
            }
        }
        if(diffInd.size() == 2){
            int diffFirst = diffInd.get(0), diffLast = diffInd.get(1);
            if(s.charAt(diffFirst) == goal.charAt(diffLast) && s.charAt(diffLast) == goal.charAt(diffFirst)){
                return true;
            }
        }
        if(diffInd.size() == 0){
            boolean[] set = new boolean[26];
            for (int i = 0; i < goal.length(); i++) {
                Character tmpC = goal.charAt(i);
                if(set[tmpC - 'a']){
                    return true;
                }
                set[tmpC - 'a'] = true;
            }
        }
        return false;
    }
}
