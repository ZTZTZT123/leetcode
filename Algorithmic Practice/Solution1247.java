public class Solution1247 {

    public int minimumSwap(String s1, String s2) {
        if(s1.length() == s2.length()){

            int length = s1.length();

            int xy = 0, yx = 0;

            for (int i = 0; i < length; i++) {
                char c1 = s1.charAt(i), c2 = s2.charAt(i);
                if(c1 == 'x' && c2 == 'y'){
                    xy++;
                } else if (c1 == 'y' && c2 == 'x') {
                    yx++;
                }
            }

            int res = 0;

            if((xy + yx) % 2 == 0){
                res += (xy / 2);
                res += (yx / 2);
                res += (xy % 2) + (yx % 2);
                return res;
            }
        }

        return -1;
    }
}
