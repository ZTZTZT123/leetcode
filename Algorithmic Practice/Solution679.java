import java.util.*;

public class Solution679 {


    private static List<List<Character>> opPermutation;

    static {
        opPermutation = new ArrayList<>();
        opTravel(opPermutation, Arrays.asList('+', '-', '*', '/'), new ArrayList<>());
    }

    private static void opTravel(List<List<Character>> res, List<Character> card, List<Character> chosen){
        if(chosen.size() == card.size() - 1){
            res.add(new ArrayList<>(chosen));
            return;
        }
        for (int i = 0; i < card.size(); i++) {
            chosen.add(card.get(i));
            opTravel(res, card, chosen);
            chosen.remove(chosen.size()-1);
        }
    }

    private void pick2Travel(List<List<Double>> res, List<Double> tmpPick, List<Double> availableList, int ind){
        if(tmpPick.size() == 2){
            res.add(new ArrayList<>(tmpPick));
        }
        for (int i = ind; i < availableList.size(); i++) {
            tmpPick.add(availableList.get(i));
            pick2Travel(res, tmpPick, availableList, i+1);
            tmpPick.remove(tmpPick.size()-1);
        }
    }

    private List<List<Double>> pick2(List<Double> availableList){
        List<List<Double>> res = new ArrayList<>();
        pick2Travel(res, new ArrayList<>(), availableList, 0);
        int tmpSize = res.size();
        for (int i = 0; i < tmpSize; i++) {
            List<Double> reverseList = new ArrayList<>(res.get(i));
            Collections.reverse(reverseList);
            res.add(reverseList);
        }
        return res;
    }

    private double doOperate(double a, double b, char opChar){
        if(opChar == '+'){
            return a+b;
        }
        else if(opChar == '-'){
            return a-b;
        }
        else if(opChar == '*'){
            return a*b;
        }
        else{
            return a/b;
        }
    }

    private boolean doJudgeTravel(List<Double> availableList, List<Character> opList){
        if(availableList.size() == 1){
            Double resNum = availableList.get(0);
            if(23.999999 < resNum && resNum < 24.000001){
                return true;
            }
            return false;
        }
        List<List<Double>> pick2 = pick2(availableList);
        for (List<Double> tmp2Num : pick2) {
            int tmpInd = 4 - availableList.size();
            List<Double> doList = new ArrayList<>(availableList);
            Double a = tmp2Num.get(0);
            Double b = tmp2Num.get(1);
            doList.remove(a);
            doList.remove(b);
            doList.add(doOperate(a, b, opList.get(tmpInd)));
            if(doJudgeTravel(doList, opList)){
                return true;
            }
        }
        return false;
    }

    public boolean judgePoint24(int[] cards) {
        List<Double> availableList = new ArrayList<>(cards.length);
        for (int num:cards) {
            availableList.add((double) num);
        }
        for (List<Character> tmpOps : opPermutation) {
            if(doJudgeTravel(availableList, tmpOps)){
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        Solution679 s = new Solution679();
        System.out.println(s.pick2(Arrays.asList(1d,2d,3d,4d)));
    }
}
