public class Solution831 {



    private String repeatStr(int n, String str){
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < n; i++) {
            sb.append(str);
        }
        return sb.toString();
    }

    private boolean isEmailCount(String s){
        return s.contains("@");
    }


    private String emailCountDoMask(String s){
        s = s.toLowerCase();
        String first = s.substring(0, 1);
        String second = s.substring(s.indexOf("@") - 1);
        return first + "*****" + second;
    }

    private String phoneNumberDoMask(String s){
        String clearStr = s.replace("+", "")
                .replace("-", "")
                .replace("(", "")
                .replace(")", "")
                .replace(" ", "");
        int extLen = clearStr.length() - 10;
        String headString = extLen > 0 ? "+" : "";
        String middleString = repeatStr(extLen, "*") + (extLen > 0 ? "-" : "") + "***-" + "***-";
        String tailString = clearStr.substring(clearStr.length() - 4);
        return headString + middleString + tailString;
    }

    public String maskPII(String s) {
        if(isEmailCount(s)){
            return emailCountDoMask(s);
        }else {
            return phoneNumberDoMask(s);
        }
    }
}
