import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class Solution496 {
    public int[] nextGreaterElement(int[] nums1, int[] nums2) {
        Map<Integer, Integer> indMap = new HashMap<>();
        for (int i = 0; i < nums2.length; i++) {
            indMap.put(nums2[i], i);
        }
        int[] res = new int[nums1.length];
        for (int i = 0; i < nums1.length; i++) {
            res[i] = -1;
            int tmpNum = nums1[i];
            for (int ind = indMap.get(tmpNum) + 1; ind < nums2.length; ind++) {
                if(nums2[ind] > tmpNum){
                    res[i] = nums2[ind];
                    break;
                }
            }
        }
        return res;
    }
}
