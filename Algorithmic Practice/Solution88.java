import java.util.Arrays;

public class Solution88 {

    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int minLen=Math.min(m,n);
        int[] sortArr=new int[m+n];
        int ind1=0,ind2=0;
        int allInd=0;
        while(ind1!=m&&ind2!=n){
            int tmpNum1=nums1[ind1];
            int tmpNum2=nums2[ind2];
            if(tmpNum1<tmpNum2){
                sortArr[allInd]=tmpNum1;
                ind1++;
            }
            else{
                sortArr[allInd]=tmpNum2;
                ind2++;
            }
            allInd++;
        }
        while(ind1!=m||ind2!=n){
            if(ind1!=m-1){
                sortArr[allInd]=nums1[ind1];
                ind1++;
            }
            else{
                sortArr[allInd]=nums2[ind2];
                ind2++;
            }
            allInd++;
        }
        for (int i = 0; i < nums1.length; i++) {
            nums1[i]=sortArr[i];
        }
    }
}
