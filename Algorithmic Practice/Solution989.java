import java.util.LinkedList;
import java.util.List;

public class Solution989 {
    public List<Integer> addToArrayForm(int[] A, int K) {
        List<Integer> KArray=new LinkedList<>();
        while(K>0){
            KArray.add(0,K%10);
            K=K/10;
        }
        List<Integer> result=new LinkedList<>();
        int Kindex=KArray.size()-1;
        int Aindex=A.length-1;
        int carry=0;
        while(Kindex>=0||Aindex>=0){
            int aTmp=Aindex>=0? A[Aindex]:0;
            int kTmp=Kindex>=0? KArray.get(Kindex):0;
            int sum=aTmp+kTmp+carry;
            int tmp=sum%10;
            result.add(0,tmp);
            carry=sum/10;
            Aindex--;
            Kindex--;
        }
        if(carry>0){
            result.add(0,carry);
        }
        return result;
    }
}
