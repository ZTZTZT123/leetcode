import java.util.ArrayList;
import java.util.List;

public class Solution438 {

    private void initSet(int[] set, String str, int len){
        for (int i = 0; i < len; i++) {
            set[str.charAt(i) - 'a']++;
        }
    }

    private boolean isEqual(int[] pSet, int[] sSet){
        for (int i = 0; i < pSet.length; i++) {
            if(pSet[i] != sSet[i]){
                return false;
            }
        }
        return true;
    }

    public List<Integer> findAnagrams(String s, String p) {
        List<Integer> res = new ArrayList<>();
        int pLen = p.length(), sLen = s.length();
        if(sLen < pLen){
            return res;
        }
        int[] pSet = new int[26], sSet = new int[26];
        initSet(pSet, p, pLen);
        initSet(sSet, s, pLen);
        for (int i = pLen; i <= sLen; i++) {
            if(isEqual(pSet, sSet)){
                res.add(i - pLen);
            }
            if(i == sLen){break;}
            sSet[s.charAt(i) - 'a']++;
            sSet[s.charAt(i - pLen) - 'a']--;
        }
        return res;
    }


    public static void main(String[] args) {
        Solution438 s = new Solution438();
        s.findAnagrams("aaaaaaaaaa",
                "aaaaaaaaaaaaa");
    }
}
