public class Solution896 {
    public boolean isMonotonic(int[] A) {
        Boolean flag=null;
        for (int i = 0; i < A.length-1; i++) {
            int tmp=A[i];
            int next=A[i+1];
            if(next==tmp){
                continue;
            }
            else if(next>tmp){
                if(flag==null){
                    flag=true;
                }
                else{
                    if(flag==false){
                        return false;
                    }
                }

            }
            else{
                if(flag==null){
                    flag=false;
                }
                else{
                    if(flag==true){
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
