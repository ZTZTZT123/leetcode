public class Solution551 {
    public boolean checkRecord(String s) {
        int aCount = 0;
        int lCount = 0;
        char lastC = ' ';
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if(c == 'L'){
                lCount++;
            }
            else{
                lCount = 0;
            }
            if(c == 'A'){
                aCount++;
            }
            if(aCount >= 2 || lCount >= 3){
                return false;
            }
            lastC = c;
        }
        return true;
    }

    public static void main(String[] args) {
        Solution551 s = new Solution551();
        System.out.println(s.checkRecord("PPALLL"));
    }
}
