
import java.util.ArrayList;
import java.util.Arrays;

public class Solution561 {
    public int arrayPairSum(int[] nums) {
        int result=0;
        final int Bias=10000;
        int[] sortArr=new int[20001];
        ArrayList<Integer> newList=new ArrayList<>();
        for (int i = 0; i < nums.length; i++) {
            ++sortArr[nums[i]+Bias];
        }
        for (int i = 0; i < sortArr.length; i++) {
            for (int j = 0; j < sortArr[i]; j++) {
                newList.add(i-Bias);
            }
        }

        for (int i = 0; i < nums.length; i+=2) {
            result+= newList.get(i);
        }
        return result;
    }
}
