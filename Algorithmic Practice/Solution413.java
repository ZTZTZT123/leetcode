public class Solution413 {
    public int numberOfArithmeticSlices(int[] nums) {
        int res = 0;
        for (int i = 0; i < nums.length-2; i++) {
            int sub = nums[i+1]-nums[i];
            for (int j = i+1; j < nums.length-1; j++) {
                int tmpSub = nums[j+1]-nums[j];
                if(sub == tmpSub){
                    res++;
                }
                else{
                    break;
                }
            }
        }
        return res;
    }

    public int numberOfArithmeticSlicesDP(int[] nums) {
        int res = 0;
        if(nums.length <= 2){
            return res;
        }
        int[] dp = new int[nums.length];
        dp[0] = 0;
        dp[1] = 0;
        for (int i = 2; i < nums.length; i++) {
            if((nums[i]-nums[i-1])==(nums[i-1]-nums[i-2])){
                dp[i] = dp[i-1]+1;
            }
            else{
                dp[i] = 0;
            }
            res+=dp[i];
        }
        return res;
    }
}
