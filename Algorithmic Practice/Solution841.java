import java.util.*;

public class Solution841 {
    int tvCount=1;
    public boolean travel(List<List<Integer>> rooms,int tmpLoc,boolean[] mark){
        if(tvCount==rooms.size()){
            return true;
        }
        List<Integer> tmpKeys=rooms.get(tmpLoc);
        for(int key:tmpKeys){
            if(mark[key]==true){
                continue;
            }
            mark[key]=true;
            tvCount++;
            if(travel(rooms,key,mark)==true){
                return true;
            }
        }
        return false;
    }

    public boolean canVisitAllRooms(List<List<Integer>> rooms) {
        boolean[] mark=new boolean[rooms.size()];
        mark[0]=true;
        return travel(rooms,0,mark);
    }

    public static void main(String[] args) {
        Solution841 s=new Solution841();
        List<List<Integer>> ll= new ArrayList<>();
        ll.add(Arrays.asList(new Integer[]{1}));
        System.out.println(s.canVisitAllRooms(Arrays.asList(Arrays.asList(1,3),Arrays.asList(3,0,1),Arrays.asList(2),Arrays.asList(0))));
    }
}
