import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Solution500 {

    static Set<Character>[] setArr;

    static void initSetArr(String str, int ind){
        Set<Character> tmpSet = new HashSet<>();
        for (int i = 0; i < str.length(); i++) {
            tmpSet.add(str.charAt(i));
        }
        setArr[ind] = tmpSet;
    }

    static {
        setArr = (HashSet<Character>[]) new HashSet<?>[3];
        initSetArr("qwertyuiopQWERTYUIOP", 0);
        initSetArr("asdfghjklASDFGHJKL", 1);
        initSetArr("zxcvbnmZXCVBNM", 2);
    }

    private boolean doJudge(String str){
        Set<Character> tmpSet = null;
        for (int i = 0; i < str.length(); i++) {
            Character tmpChar = str.charAt(i);
            if(tmpSet != null){
                if(!tmpSet.contains(tmpChar)){
                    return false;
                }
                continue;
            }
            for (Set<Character> set : setArr) {
                if(set.contains(tmpChar)){
                    tmpSet = set;
                    break;
                }
            }
        }
        return true;
    }

    public String[] findWords(String[] words) {
        List<String> resList = new ArrayList<>();
        for (String str : words) {
            if(doJudge(str)){
                resList.add(str);
            }
        }
        String[] res = new String[resList.size()];
        for (int i = 0; i < resList.size(); i++) {
            res[i] = resList.get(i);
        }
        return res;
    }
}
