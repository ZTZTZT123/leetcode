import java.util.*;

/**
 * @FileName Solution1041
 * @Description
 * @Author tong.zhou
 * @date 2025-02-22
 **/
public class Solution1041 {

    static class Direct{

        int x;
        int y;

        public Direct(int x, int y) {
            this.x = x;
            this.y = y;
        }

    }

    private static Map<Direct, Direct> LEFT_CONVERT;
    private static Map<Direct, Direct> RIGHT_CONVERT;

    static {
        LEFT_CONVERT = new HashMap<>();
        RIGHT_CONVERT = new HashMap<>();
        LEFT_CONVERT.put(new Direct(0, 1), new Direct(1, 0));
        LEFT_CONVERT.put(new Direct(1, 0), new Direct(0, -1));
        LEFT_CONVERT.put(new Direct(0, -1), new Direct(-1, 0));
        LEFT_CONVERT.put(new Direct(-1, 0), new Direct(0, 1));
        RIGHT_CONVERT.put(new Direct(0, 1), new Direct(-1, 0));
        RIGHT_CONVERT.put(new Direct(-1, 0), new Direct(0, -1));
        RIGHT_CONVERT.put(new Direct(0, -1), new Direct(1, 0));
        RIGHT_CONVERT.put(new Direct(1, 0), new Direct(0, 1));
    }


    private void moveLocate(Direct direct, int[] locate) {
        locate[0] = locate[0] + direct.x;
        locate[1] = locate[1] + direct.y;
    }

    private void changeDirect(Direct direct, Character c){
        if(c == 'L'){
            Direct nextDirect = LEFT_CONVERT.get(direct);
            direct.x = nextDirect.x;
            direct.y = nextDirect.y;
        } else if (c == 'R') {
            Direct nextDirect = RIGHT_CONVERT.get(direct);
            direct.x = nextDirect.x;
            direct.y = nextDirect.y;
        }
    }

    public boolean isRobotBounded(String instructions) {
        Direct direct = new Direct(0, 1);
        int[] locate = new int[]{0, 0};
        for (Character c : instructions.toCharArray()) {
            if (c == 'G') {
                moveLocate(direct, locate);
            } else {
                changeDirect(direct, c);
            }
        }
        if(locate[0] == 0 && locate[1] == 0){
            return true;
        }
        return !(direct.x == 0 && direct.y == 1);
    }
}
