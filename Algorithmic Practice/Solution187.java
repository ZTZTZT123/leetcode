import java.util.*;

public class Solution187 {
    public List<String> findRepeatedDnaSequences(String s) {
        List<String> res = new ArrayList<>();
        Map<String, Integer> countMap = new HashMap<>();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            sb.append(s.charAt(i));
            if(sb.length() > 10){
                sb.deleteCharAt(0);
            }
            String tmpStr = sb.toString();
            int tmpCount = countMap.getOrDefault(tmpStr, 0);
            if(tmpCount == 1){
                res.add(tmpStr);
            }
            countMap.put(tmpStr, tmpCount + 1);
        }
        return res;
    }
}
