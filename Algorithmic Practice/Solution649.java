import java.util.*;
public class Solution649 {
    int R_left=0;
    int D_left=0;
    public Boolean prohibition_power(String senate,ArrayList<Boolean> mark,char target,int index){
        for (int i = index+1; i < senate.length(); i++) {
            if (senate.charAt(i) == target && mark.get(i) == false) {
                mark.set(i, true);
                return true;
            }
        }
        for (int i = 0; i < index; i++) {
            if (senate.charAt(i) == target && mark.get(i) == false) {
                mark.set(i, true);
                return true;
            }
        }
        return false;
    }
    public String predictPartyVictory(String senate) {
        ArrayList<Boolean> mark=new ArrayList<>();
        for(int i=0;i<senate.length();++i) {
            char temp_char=senate.charAt(i);
            if(temp_char=='R'){
                R_left=R_left+1;
            }
            else if(temp_char=='D'){
                D_left=D_left+1;
            }
            mark.add(false);
        }
        while(true){
            for(int i=0;i<senate.length();++i){
                System.out.println(senate.charAt(i));
                if(mark.get(i)==true){
                    continue;
                }
                char temp_char=senate.charAt(i);
                if(temp_char=='R'){
                    if(D_left==0){
                        return "Radiant";
                    }
                    if(prohibition_power(senate,mark,'D',i)){
                        D_left=D_left-1;
                    }
                }
                else if(temp_char=='D'){
                    if(R_left==0){
                        return "Dire";
                    }
                    if(prohibition_power(senate,mark,'R',i)){
                        R_left=R_left-1;
                    }
                }
                System.out.println(D_left+" "+R_left);
                System.out.println(mark);
            }
        }
    }
    public static void main(String[] args) {
        Solution649 s=new Solution649();
        System.out.println(s.predictPartyVictory("DRRDRDRDRDDRDRDRDRDRDDDRDRRRRDDDRRRRDDDDDRRRRR"));
    }
}
