public class Solution2 {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode tmpNode1 = l1, tmpNode2 = l2;
        ListNode head = new ListNode(), tmpResNode = head;
        int lastAdd = 0;
        while(tmpNode1 != null || tmpNode2 != null || lastAdd > 0){
            int tmpVal1 = tmpNode1 == null ? 0 : tmpNode1.val, tmpVal2 = tmpNode2 == null ? 0 : tmpNode2.val;
            int addRes = tmpVal1 + tmpVal2 + lastAdd;
            int tmpRes = addRes % 10;
            tmpResNode.next = new ListNode(tmpRes);
            tmpResNode = tmpResNode.next;
            tmpNode1 = tmpNode1 == null ? null : tmpNode1.next;
            tmpNode2 = tmpNode2 == null ? null : tmpNode2.next;
            lastAdd = addRes / 10;
        }
        return head.next;
    }
}
