import java.util.ArrayList;
import java.util.List;

public class Solution89 {
    
    public List<Integer> grayCode(int n) {
        List<Integer> tmpRes = new ArrayList<>();
        tmpRes.add(0);
        for (int i = 1; i <= n; i++) {
            List<Integer> lastRes = new ArrayList<>(tmpRes);
            int lastResLen = lastRes.size();
            tmpRes = new ArrayList<>(lastResLen * 2);
            for (int j = 0; j < lastResLen; j++) {
                tmpRes.add((lastRes.get(j) << 1) + 0);
            }
            for (int j = lastResLen - 1; j >= 0; j--) {
                tmpRes.add((lastRes.get(j) << 1) + 1);
            }
        }
        return tmpRes;
    }
}
