import java.util.*;

public class Solution1239 {
    static class BitMap {
        private byte[] bits;
        private int size;

        public BitMap(long N){
            int initCap=getIndex(N)+1;
            bits=new byte[initCap];
        }

        public int getSize(){
            return size;
        }

        private int getIndex(long num){
            return (int)(num >> 3);
        }

        private int getLoc(long num){
            return (int)(num & 0x07);
        }

        private void expansion(long num){
            int tarSize=getIndex(num)+1;
            byte[] newBits=new byte[tarSize];
            for (int i = 0; i < bits.length; i++) {
                newBits[i]=bits[i];
            }
            bits=newBits;
        }

        public boolean contains(long num){
            int index=getIndex(num);
            if(index>=bits.length){
                return false;
            }
            return (bits[index] & 1 << getLoc(num)) != 0;
        }

        public void put(long num){
            int index=getIndex(num);
            if(index>=bits.length){
                expansion(num);
            }
            byte tmpSeg=bits[index];
            byte resSeg=(byte) (tmpSeg | 1 << getLoc(num));
            if(resSeg!=tmpSeg){
                bits[index]=resSeg;
                size++;
            }
        }

        public void remove(long num){
            int index=getIndex(num);
            if(index>=bits.length){
                return;
            }
            byte tmpSeg=bits[index];
            byte resSeg=(byte) (tmpSeg & ~(1 << getLoc(num)));
            if(resSeg!=tmpSeg){
                bits[index]=resSeg;
                size--;
            }
        }

        public long[] toNumArr(){
            long[] numArr=new long[size];
            int loc=0;
            long tmpNum=0;
            for (byte bit:bits) {
                for (int i = 0; i < 8; i++) {
                    if(0!=(bit & 1 << i)){
                        numArr[loc]=tmpNum;
                        loc++;
                    }
                    tmpNum++;
                }
            }
            return numArr;
        }

        public static boolean dupCheck(BitMap bm1, BitMap bm2){
            byte[] bits1=bm1.bits, bits2=bm2.bits;
            int minLen=Math.min(bits1.length,bits2.length);
            for (int i = 0; i < minLen; i++) {
                if((bits1[i]&bits2[i])!=0){
                    return true;
                }
            }
            return false;
        }
    }
    int res=0;

    public boolean dupCheck(BitMap tmpSet,String str){
        for (int i = 0; i < str.length(); i++) {
            int num=str.charAt(i)-'a';
            if(tmpSet.contains(num)){
                return true;
            }
        }
        return false;
    }

    public void addC(BitMap tmpSet,String str){
        for (int i = 0; i < str.length(); i++) {
            int num=str.charAt(i)-'a';
            tmpSet.put(num);
        }
    }

    public void remC(BitMap tmpSet,String str){
        for (int i = 0; i < str.length(); i++) {
            int num=str.charAt(i)-'a';
            tmpSet.remove(num);
        }
    }

    public void travel(BitMap tmpSet, List<String> arr, int ind){
        res=Math.max(tmpSet.getSize(),res);
        for (int i = ind; i < arr.size(); i++) {
            String tmpStr=arr.get(i);
            if(!dupCheck(tmpSet,tmpStr)){
                addC(tmpSet,tmpStr);
                travel(tmpSet,arr,i+1);
                remC(tmpSet,tmpStr);
            }
        }
    }

    public List<String> removeBadStr(List<String> arr){
        List<String> resList=new ArrayList<>();
        BitMap remInds=new BitMap(arr.size());
        outer:for (int i = 0; i < arr.size(); i++) {
            String tmpStr=arr.get(i);
            BitMap checkSet=new BitMap(26);
            for (int j = 0; j < tmpStr.length(); j++) {
                int num=tmpStr.charAt(j)-'a';
                if(checkSet.contains(num)){
                    remInds.put(i);
                    continue outer;
                }
                checkSet.put(num);
            }
        }
        for (int i = 0; i < arr.size(); i++) {
            if(remInds.contains(i)){
                continue;
            }
            resList.add(arr.get(i));
        }
        return resList;
    }

    public int maxLength(List<String> arr) {
        arr=removeBadStr(arr);
        travel(new BitMap(100),arr,0);
        return res;
    }
}
