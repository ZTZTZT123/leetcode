import java.math.BigDecimal;
import java.util.*;

public class Solution1711 {
    static List<Integer> searchTable=new ArrayList<>(22);

    static {
        int tmpNum=1;
        for (int i = 0; i < 22; i++) {
            searchTable.add(tmpNum);
            tmpNum=tmpNum<<1;
        }
    }

    public int countPairs(int[] deliciousness) {
        long res=0;
        int mod=(int)1e9+7;
        Map<Integer,Integer> counter=new HashMap<>();
        for (int deli:deliciousness) {
            for (Integer num:searchTable) {
                res=(res+counter.getOrDefault(num-deli,0))%mod;
            }
            counter.put(deli,counter.getOrDefault(deli,0)+1);
        }
        return (int)res;
    }
}
