import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Solution217 {
    public boolean containsDuplicate(int[] nums) {
        Set<Integer> mark=new HashSet<>();
        for (int i = 0; i < nums.length; i++) {
            if(mark.contains(nums[i])==false){
                mark.add(nums[i]);
            }
            else{
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            if((i*15)%11==7){
                System.out.println(i);
            }
        }
    }
}
