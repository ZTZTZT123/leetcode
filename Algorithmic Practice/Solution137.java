import java.util.HashMap;
import java.util.Map;

public class Solution137 {
    public int singleNumber(int[] nums) {
        Map<Integer,Integer> mark=new HashMap<>();
        for (Integer num:nums) {
            Integer tmpCount=mark.getOrDefault(num,0);
            mark.put(num,++tmpCount);
        }
        Integer result=-1;
        for (Map.Entry et:mark.entrySet()) {
            if((Integer) et.getValue()==1){
                result=(Integer) et.getKey();
            }
        }
        return result;
    }
}
