import java.util.Arrays;

public class Solution275 {

    public boolean doJudge(int[] citations,int mid){
        int loc=Arrays.binarySearch(citations,mid);
        if(loc<0){ loc=-(loc+1);}
        else{
            int fstLoc=loc;
            while(fstLoc>=1&&citations[fstLoc-1]==mid){
                fstLoc--;
            }
            loc=fstLoc;
        }
        return citations.length-loc>=mid;
    }

    public int hIndex(int[] citations) {
        int left=0;
        int right=citations[citations.length-1];
        Integer result=null;
        while(left<=right){
            int mid=(left+right)/2;
            if(doJudge(citations,mid)){
                result=mid;
                left=mid+1;
            }
            else{
                right=mid-1;
            }
        }
        return result==null? citations.length:result;
    }

    public static void main(String[] args) {
        Solution275 s=new Solution275();
        System.out.println(s.hIndex(new int[]{7,7,7,7,7,7,7}));
    }
}
