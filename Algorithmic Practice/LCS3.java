import java.util.LinkedList;
import java.util.List;

public class LCS3 {

    int[][] directArr = new int[][]{{0,1}, {1,0}, {-1,0}, {0,-1}};
    String[] grid;
    int m;
    int n;
    boolean[][] visited;
    boolean isConnect;
    int pathNum;

    private char getLocInfo(int i, int j){
        return grid[i].charAt(j);
    }

    private boolean isConnect(int i, int j){
        if(i == 0 || i == m - 1 || j == 0 || j == n - 1){
            return true;
        }
        for (int[] direct : directArr) {
            if(getLocInfo(i + direct[0], j + direct[1]) == '0'){
                return true;
            }
        }
        return false;
    }

    private void travel(int i, int j){
        if(isConnect(i, j)){
            isConnect = true;
        }
        pathNum++;
        visited[i][j] = true;
        for (int[] direct : directArr) {
            int newI = i + direct[0], newJ = j + direct[1];
            if(0 <= newI && newI < m && 0 <= newJ && newJ < n && getLocInfo(newI, newJ) == getLocInfo(i, j) && !visited[newI][newJ]){
                travel(newI, newJ);
            }
        }
    }

    public int largestArea(String[] grid) {
        this.grid = grid;
        this.m = grid.length;
        this.n = grid[0].length();
        visited = new boolean[m][n];
        int maxArea = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if(getLocInfo(i, j) != '0' && !visited[i][j]) {
                    isConnect = false;
                    pathNum = 0;
                    travel(i, j);
                    if(!isConnect){
                        maxArea = Math.max(maxArea, pathNum);
                    }
                }
            }
        }
        return maxArea;
    }

    public static void main(String[] args) {
        LCS3 l = new LCS3();
        l.largestArea(new String[]{"110","231","221"});
    }
}
