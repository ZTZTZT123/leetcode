import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Solution1004 {
    public int longestOnes(int[] A, int K) {
        int result=0;
        int zeroNum=0;
        int left=0;
        int right=0;
        while(right<A.length){
            if(A[right]==0){
                zeroNum++;
            }
            while(zeroNum>K){
                if(A[left]==0){
                    zeroNum--;
                }
                left++;
            }
            int len=right-left+1;
            result= len>result? len:result;
            right++;
        }
        return result;
    }

    public static void main(String[] args) {
        Solution1004 s=new Solution1004();
        System.out.println(s.longestOnes(new int[]{1,1,1,0,0,0,1,1,1,1,0},
        2));
    }
}
