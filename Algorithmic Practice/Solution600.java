import java.util.HashSet;
import java.util.Set;

public class Solution600 {
    int res = 1;
    int n;

    public int findIntegers(int n) {
        this.n = n;
        travel(1, true);
        return res;
    }

    private void travel(int tmpNum, boolean isOne){
        if(tmpNum > n){
            return;
        }
        res++;
        if(!isOne){
            travel((tmpNum<<1)+1, true);
        }
        travel(tmpNum<<1, false);
    }

    public static void main(String[] args) {
        Solution600 s = new Solution600();
        System.out.println(s.findIntegers(5));
    }
}
