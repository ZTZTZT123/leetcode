import java.util.HashSet;
import java.util.Set;

public class Solution1980 {



    private String toBinaryString(int num, int len){
        StringBuilder sb = new StringBuilder();
        while(num > 0){
            sb.insert(0, num&1);
            num>>=1;
        }
        while(sb.length() < len){
            sb.insert(0, 0);
        }
        return sb.toString();
    }


    private Integer binaryStrToInt(String num){
        int res = 0;
        int n = num.length()-1;
        for (int i = n; i >= 0 ; i--) {
            int loc = n - i;
            int locNum = num.charAt(loc) - '0';
            res += locNum<<i;
        }
        return res;
    }



    public String findDifferentBinaryString(String[] nums) {
        int n = (int) Math.pow(2, nums.length);
        Set<Integer> bm = new HashSet<>(n);
        for (String str:nums) {
            bm.add(binaryStrToInt(str));
        }
        for (int i = 0; i < n; i++) {
            if(bm.contains(i)){
                continue;
            }
            return toBinaryString(i, nums.length);
        }
        return "";
    }

    public static void main(String[] args) {
        Solution1980 s = new Solution1980();
        System.out.println(s.binaryStrToInt("00"));
        System.out.println(s.binaryStrToInt("01"));
    }
}
