import java.util.LinkedList;
import java.util.Queue;

public class Solution671 {
    class TreeNode {
       int val;
       TreeNode left;
       TreeNode right;
       TreeNode() {}
       TreeNode(int val) { this.val = val; }
       TreeNode(int val, TreeNode left, TreeNode right) {
           this.val = val;
           this.left = left;
           this.right = right;
       }
    }

    public int findSecondMinimumValue(TreeNode root) {
        int minVal = root.val;
        int res = 0;
        Queue<TreeNode> q = new LinkedList<>();
        q.offer(root);
        while(!q.isEmpty()){
            TreeNode tmpNode = q.poll();
            if(tmpNode==null){
                continue;
            }
            if(tmpNode.val>minVal){
                res = res==0? tmpNode.val : Math.min(res,tmpNode.val);
            }
            q.offer(tmpNode.left);
            q.offer(tmpNode.right);
        }
        return res>0? res:-1;
    }
}
