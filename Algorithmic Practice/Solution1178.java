import java.util.*;

public class Solution1178 {
    public int[] getMark(String str){
        int[] result=new int[26];
        for(int i=0;i<str.length();++i){
            char tmpChar=str.charAt(i);
            result[tmpChar-'a']=1;
        }
        return result;
    }

    public Boolean containChar(int[] mark,char c){
        if(mark[c-'a']==1){
            return true;
        }
        return false;
    }

    public  String sort(String str){
        TreeSet<Character> treeSet=new TreeSet<>();
        for (int i = 0; i < str.length(); i++) {
            treeSet.add(str.charAt(i));
        }
        String st ="";
        for(Character s:treeSet){
            st+=s;
        }
        return st;
    }

    public boolean check(String puzz,String word,int[] mark){
        if(puzz.length()<word.length()){
            return false;
        }
        for (int i = 0; i < word.length(); i++) {
            char c=word.charAt(i);
            if(containChar(mark,c)==false){
                return false;
            }
        }
        return true;
    }

    public List<Integer> findNumOfValidWords(String[] words, String[] puzzles) {
        List<Integer> result=new ArrayList<>();
        String[] newWords=new String[words.length];
        String[] newPuzzs=new String[puzzles.length];
        for (int i = 0; i < words.length; i++) {
            newWords[i]=sort(words[i]);
        }
        for (int i = 0; i < puzzles.length; i++) {
            newPuzzs[i]=sort(puzzles[i]);
        }
        for (int i = 0; i < puzzles.length; i++) {
            String firstChar=String.valueOf(puzzles[i].charAt(0));
            String tmpNewPuzz=newPuzzs[i];
            int[] mark=getMark(tmpNewPuzz);
            int tmpRes=0;
            Set<String> memory=new HashSet<>();
            for (int j = 0; j < words.length; j++) {
                String tmpNewWord=newWords[j];
                if(memory.contains(tmpNewWord)){
                    tmpRes++;
                    continue;
                }
                if(tmpNewWord.contains(firstChar)==false){
                    continue;
                }
                if(check(tmpNewPuzz,tmpNewWord,mark)==false){
                    continue;
                }
                memory.add(tmpNewWord);
                tmpRes++;
            }
            result.add(tmpRes);
        }
        return result;
    }

    public static void main(String[] args) {
        Solution1178 s=new Solution1178();
        System.out.println(s.findNumOfValidWords(new String[]{"aaaa","asas","able","ability","actt","actor","access"},
new String[]{"aboveyz","abrodyz","abslute","absoryz","actresz","gaswxyz"}));

    }
}
