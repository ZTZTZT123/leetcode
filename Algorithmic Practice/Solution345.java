public class Solution345 {

    char[] vowels = new char[]{'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'};

    boolean isVowel(char c){
        for (char vowel:vowels) {
            if(vowel == c){
                return true;
            }
        }
        return false;
    }

    public String reverseVowels(String s) {
        char[] charArr = s.toCharArray();
        int i = 0, j = charArr.length-1;
        while(true){
            while(i < charArr.length &&!isVowel(charArr[i])){
                i++;
            }
            while(j >= 0&&!isVowel(charArr[j])){
                j--;
            }
            if(i < j){
                char tmpC = charArr[i];
                charArr[i] = charArr[j];
                charArr[j] = tmpC;
            }
            else{
                break;
            }
            i++;
            j--;
        }
        return String.valueOf(charArr);
    }

    public static void main(String[] args) {
        Solution345 s = new Solution345();
        System.out.println(s.reverseVowels("hello"));
    }
}
