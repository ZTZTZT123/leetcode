public class Solution273 {

    final static String[] unitArr = new String[]{"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"};

    final static String[] decadeArr = new String[]{"", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"};

    final static String[] teensArr = new String[]{"Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"};

    final static String[] bigNumArr = new String[]{"", "Thousand", "Million", "Billion"};

    private String processInHundred(int num){
        if(1 <= num && num <= 9){
            return unitArr[num - 1];
        }
        if(11 <= num && num <= 19){
            return teensArr[num - 11];
        }
        if(num % 10 == 0){
            return decadeArr[num / 10];
        }
        int unitNum = num % 10;
        int decadeNum = num / 10;
        return decadeArr[decadeNum] + unitArr[unitNum - 1];
    }

    private String processInThousand(int num){
        int hundredNum = num/100;
        return (hundredNum == 0 ? "" : "" + unitArr[hundredNum - 1] + "Hundred") + processInHundred(num%100);
    }

    private String processRes(StringBuilder sb){
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < sb.length(); i++) {
            char tmpC = sb.charAt(i);
            if(Character.isUpperCase(tmpC) && i != 0){
                res.append(" ");
            }
            res.append(tmpC);
        }
        return res.toString();
    }

    public String numberToWords(int num) {
        if(num == 0){ return "Zero"; }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; num > 0; i++, num /= 1000){
            int tmpNum = num % 1000;
            sb.insert(0,tmpNum == 0? "" : processInThousand(tmpNum) + bigNumArr[i]);
        }
        return processRes(sb);
    }

    public static void main(String[] args) {
        Solution273 s = new Solution273();
        System.out.println(s.numberToWords(100000));
    }
}
