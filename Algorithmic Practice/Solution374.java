public class Solution374 {
    int guess(int num){
        return 0;
    }

    public int guessNumber(int n) {
        int left=0, right=n;
        while(left<=right){
            int mid=left+(right-left)/2;
            int tmpRes=guess(mid);
            if(tmpRes==0){
                return mid;
            }
            else if(tmpRes<0){
                right=mid-1;
            }
            else{
                left=mid+1;
            }
        }
        return -1;
    }
}
