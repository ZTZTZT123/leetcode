import java.util.LinkedHashSet;
import java.util.Set;

public class Solution83 {
    public ListNode deleteDuplicates(ListNode head) {
        ListNode ptr=head;
        while(ptr!=null){
            ListNode firstNode=ptr;
            while(ptr.next!=null&&ptr.val==ptr.next.val){
                ptr=ptr.next;
            }
            firstNode.next=ptr.next;
            ptr=ptr.next;
        }
        return head;
    }
}
