public class Solution705 {
    /** Initialize your data structure here. */
    int[] data;
    public Solution705() {
        data=new int[1000001];
    }

    public void add(int key) {
        data[key]=1;
    }

    public void remove(int key) {
        data[key]=0;
    }

    /** Returns true if this set contains the specified element */
    public boolean contains(int key) {
        return data[key]==1;
    }
}
