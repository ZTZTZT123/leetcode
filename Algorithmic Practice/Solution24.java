/**
 * @FileName Solution24
 * @Description
 * @Author tong.zhou
 * @date 2024-11-17
 **/
public class Solution24 {


    public class ListNode {
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int val) { this.val = val; }
        ListNode(int val, ListNode next) { this.val = val; this.next = next; }
    }

    public ListNode swapPairs(ListNode head) {
        ListNode first = head;
        if(first == null){
            return null;
        }
        ListNode mid = head.next;
        if(mid == null){
            return head;
        }
        ListNode last = mid.next;
        if(last == null){
            mid.next = first;
            first.next = null;
            return mid;
        }
        mid.next = first;
        last = swapPairs(last);
        first.next = last;
        return mid;
    }
}
