

public class Solution993 {
    Integer heiX=null;
    Integer heiY=null;
    TreeNode fatherX=null;
    TreeNode fatherY=null;

    public void processNode(TreeNode lastNode,TreeNode tmpNode,int height,int tarX,int tarY){
        if(tmpNode.val==tarX){
            heiX=height;
            fatherX=lastNode;
        }
        else if(tmpNode.val==tarY){
            heiY=height;
            fatherY=lastNode;
        }
    }

    public void travel(TreeNode lastNode,TreeNode tmpNode,int height,int tarX,int tarY){
        if(heiX!=null&&heiY!=null){
            return;
        }
        if(tmpNode==null){
            return;
        }

        processNode(lastNode,tmpNode, height, tarX, tarY);

        travel(tmpNode,tmpNode.left,height+1,tarX,tarY);
        travel(tmpNode,tmpNode.right,height+1,tarX,tarY);
    }

    public boolean isCousins(TreeNode root, int x, int y) {
        travel(null,root,1,x,y);
        if(heiY==heiX&&fatherY.equals(fatherX)==false){
            return true;
        }
        return false;
    }
}
