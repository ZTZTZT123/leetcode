import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Solution1218 {
    public int longestSubsequence(int[] arr, int difference) {
        int res = 1;
        Map<Integer, Integer> dp = new HashMap<>();
        for (int num : arr) {
            int tmpRes = dp.getOrDefault(num - difference, 0) + 1;
            dp.put(num, tmpRes);
            res = Math.max(res, tmpRes);
        }
        return res;
    }
}
