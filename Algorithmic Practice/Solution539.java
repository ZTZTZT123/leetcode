import java.util.*;

public class Solution539 {

    private int getMinuteNum(String strTime){
        return (strTime.charAt(0) - '0') * 600 + (strTime.charAt(1) - '0') * 60 + (strTime.charAt(3) - '0') * 10 + (strTime.charAt(4) - '0');
    }

    public int findMinDifference(List<String> timePoints) {
        List<Integer> timeNums = new ArrayList<>(timePoints.size());
        for (String strTime : timePoints) {
            timeNums.add(getMinuteNum(strTime));
        }
        Collections.sort(timeNums);
        int res = Integer.MAX_VALUE;
        int lastTimeNum = timeNums.get(0);
        for (int i = 1; i < timeNums.size(); i++) {
            int tmpTimeNum = timeNums.get(i);
            res = Math.min(res, tmpTimeNum - lastTimeNum);
            lastTimeNum = tmpTimeNum;
        }
        res = Math.min(res, timeNums.get(0) + 1440 - timeNums.get(timeNums.size() - 1));
        return res;
    }

    public static void main(String[] args) {
        Solution539 s = new Solution539();
        System.out.println(s.findMinDifference(Arrays.asList("23:59", "00:00")));
    }
}
