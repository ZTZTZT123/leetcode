public class Solution300 {
    public int lengthOfLIS(int[] nums) {
        int[] DP=new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            DP[i]=1;
        }
        for (int i = 1; i < nums.length; i++) {
            int tmp=nums[i];
            for (int j = 0; j < i; j++) {
                if(tmp>nums[j]){
                    DP[i]=Math.max(DP[i],DP[j]+1 );
                }
            }
        }
        int maxLen=0;
        for (int i = 0; i < nums.length; i++) {
            if(DP[i]>maxLen){
                maxLen=DP[i];
            }
        }
        return maxLen;
    }

    public static void main(String[] args) {
        Solution300 s=new Solution300();
        System.out.println(s.lengthOfLIS(new int[]{
                0,1,0,3,2,3
        }));
    }
}
