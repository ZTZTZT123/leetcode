import java.util.regex.*;
public class Solution65 {

    private static Pattern intPattern=Pattern.compile("^[+-]?\\d+$");

    private static Pattern decimalPattern=Pattern.compile("^[+-]?((\\d+\\.\\d*)|(\\d*\\.\\d+))$");

    public boolean isInt(String strNum){
        return intPattern.matcher(strNum).matches();
    }

    public boolean isDecimal(String strNum){
        return decimalPattern.matcher(strNum).matches();
    }

    public String[] splitNumber(String strNum){
        for (int i = 0; i < strNum.length(); i++) {
            char tmpChar=strNum.charAt(i);
            if(tmpChar=='e'||tmpChar=='E'){
                return new String[]{strNum.substring(0,i),strNum.substring(i+1)};
            }
        }
        return null;
    }

    public boolean isNumber(String s) {
        if(isInt(s)||isDecimal(s)){
            return true;
        }
        else{
            String[] strArr=splitNumber(s);
            if(strArr==null){return false;}
            return (isDecimal(strArr[0])||isInt(strArr[0]))&&isInt(strArr[1]);
        }
    }

    public static void main(String[] args) {
        Solution65 s=new Solution65();
        System.out.println(s.isDecimal("e9"));
    }
}
