import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Solution376 {
    public int wiggleMaxLengthFake(int[] nums) {
        Integer[] DP=new Integer[nums.length];
        DP[0]=1;
        char last_state='N';
        for (int i = 1; i < nums.length; i++) {
            if(nums[i-1]==nums[i]){
                DP[i]=1;
                continue;
            }
            if(DP[i-1]==1){
                DP[i]=2;
                if(nums[i]>nums[i-1]){
                    last_state='B';
                }
                else{
                    last_state='S';
                }
                continue;
            }
            if(nums[i]>nums[i-1]){
                if(last_state=='S') {
                    DP[i] = DP[i - 1] + 1;
                    last_state='B';
                }
                else {
                    DP[i] = 1;
                }
            }
            else if(nums[i]<nums[i-1]){
                if(last_state=='B') {
                    DP[i] = DP[i - 1] + 1;
                    last_state='S';
                }
                else{
                    DP[i]=1;
                }
            }
        }
        System.out.println(Arrays.asList(DP));
        return (int) Collections.max(Arrays.asList(DP));
    }
    public int wiggleMaxLength(int[] nums) {
        if(nums.length==0){
            return 0;
        }
        int DP=1;
        int last_num=nums[0];
        char last_state='N';
        for (int i = 1; i < nums.length; i++) {
            if(nums[i]==last_num){
                continue;
            }
            if(DP==1){
                DP=2;
                last_num=nums[i];
                if(nums[i]>nums[i-1]){
                    last_state='B';
                }
                else{
                    last_state='S';
                }
                continue;
            }
            if(last_state=='S'){
                if(nums[i]>last_num){
                    DP=DP+1;
                    last_state='B';
                }
                last_num=nums[i];
            }
            else if(last_state=='B'){
                if(nums[i]<last_num){
                    DP=DP+1;
                    last_state='S';
                }
                last_num=nums[i];
            }
        }
        return DP;
    }

    public static void main(String[] args) {
        Solution376 s=new Solution376();
        System.out.println(s.wiggleMaxLength(new int[]{1,2,3,4,5,6,7,8,9}));
    }
}
