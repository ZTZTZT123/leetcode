public class Solution50 {
    public double doPow(double tmpNum,double x,long tmpN,long n){
        if(n==0||x==1){
            return 1;
        }
        if(x==-1){
            return n%2==1? -1:1;
        }
        if(-0.00000001<tmpNum&&tmpNum<0.00000001){
            return 0;
        }
        if(tmpN<n){
            if(tmpN*2<=n){
                return doPow(tmpNum*tmpNum,x,tmpN*2,n);
            }
            else{
                return doPow(tmpNum*x,x,tmpN+1,n);
            }
        }
        return tmpNum;
    }

    public double myPow(double x, long n) {
        return Math.pow(x,n);
    }

    public static void main(String[] args) {
        Solution50 s=new Solution50();
        System.out.println(s.myPow(2.00000,
                -2147483648));
    }
}
