public class Solution738 {
    public int monotoneIncreasingDigits(int N) {
        if(N<=10){
            return N-1;
        }
        int result=N;
        int split_bit=10;
        while((N/split_bit)!=0){
            int split_r=result%split_bit;
            int split_l=(result%(split_bit*10))/split_bit;
            int index=String.valueOf(N).length()-String.valueOf(split_bit/10).length();
            int split_r_top=Integer.parseInt(String.valueOf(result).substring(index).substring(0,1));
            if(split_l>split_r_top){
                result=result-(split_r+1);
            }
            split_bit=10*split_bit;
        }
        return result;
    }

    public static void main(String[] args) {
        Solution738 s=new Solution738();
        System.out.println(s.monotoneIncreasingDigits(101));
    }
}
