public class Solution453 {

    private int findMinNum(int[] nums){
        int minNum = Integer.MAX_VALUE;
        for (int num:nums) {
            if(num < minNum){
                minNum = num;
            }
        }
        return minNum;
    }

    private int getSubSum(int[] nums, int minNum){
        int res = 0;
        for (int num : nums) {
            res += (num - minNum);
        }
        return res;
    }

    public int minMoves(int[] nums) {
        return getSubSum(nums, findMinNum(nums));
    }

    public static void main(String[] args) {
        Solution453 s = new Solution453();
        System.out.println(s.minMoves(new int[]{1,2,3}));
    }
}
