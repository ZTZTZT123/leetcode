public class Solution476 {
    public int findComplement(int num) {
        String strNum = Integer.toBinaryString(num);
        int res = 0, ratio = 1;
        for (int i = strNum.length() - 1; i >= 0; i--) {
            int tmpNum = strNum.charAt(i) - '0';
            res += ratio * ((tmpNum + 1) & 1);
            ratio *= 2;
        }
        return res;
    }

    public static void main(String[] args) {
        Solution476 s = new Solution476();
        System.out.println(s.findComplement(2));
    }
}
