import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Solution165 {

    public List getIntVec(String version){
        int len=version.length();
        List<Integer> mark=new ArrayList<>();
        String tmpStr="";
        for (int i = 0; i < len; i++) {
            char tmpChar=version.charAt(i);
            if(tmpChar=='.'){
                mark.add(Integer.parseInt(tmpStr));
                tmpStr="";
                continue;
            }
            tmpStr+=tmpChar;
        }
        mark.add(Integer.parseInt(tmpStr));
        return mark;
    }

    public int compareVersion(String version1, String version2) {
        List<Integer> mark_1=getIntVec(version1);
        List<Integer> mark_2=getIntVec(version2);
        while(mark_1.size()!=mark_2.size()){
            if(mark_1.size()<mark_2.size()){
                mark_1.add(0);
            }
            else{
                mark_2.add(0);
            }
        }
        for (int i = 0; i < mark_1.size(); i++) {
            int tmp_1=mark_1.get(i);
            int tmp_2=mark_2.get(i);
            if(tmp_1==tmp_2){
                continue;
            }
            else if(tmp_1<tmp_2){
                return -1;
            }
            else{
                return 1;
            }
        }
        return 0;
    }

    public static void main(String[] args) {
        Solution165 s=new Solution165();
        System.out.println(s.compareVersion("1.01","1.001"));
    }
}
