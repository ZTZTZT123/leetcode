import java.util.HashMap;

public class Solution290 {
    public boolean wordPattern(String pattern, String s) {
        HashMap<Character,String> char_mark=new HashMap<>();
        HashMap<String,Character> string_mark=new HashMap<>();
        String[] s_vec=s.split(" ");
        if(pattern.length()!= s_vec.length){
            return false;
        }
        for (int i = 0; i < pattern.length(); i++) {
            String map_val=char_mark.get(pattern.charAt(i));
            if(map_val==null){
                char_mark.put(pattern.charAt(i),s_vec[i]);
            }
            else if(!map_val.equals(s_vec[i])){
                return false;
            }
            Character map_value=string_mark.get(s_vec[i]);
            if(map_value==null){
                string_mark.put(s_vec[i],pattern.charAt(i));
            }
            else if(!map_value.equals(pattern.charAt(i))){
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Solution290 s=new Solution290();
        System.out.println(s.wordPattern("abba",
                "dog dog dog dog"));
    }
}
