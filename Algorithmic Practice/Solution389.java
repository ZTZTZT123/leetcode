import java.util.HashMap;

public class Solution389 {
    public char findTheDifference(String s, String t) {
        HashMap<Character,Integer> s_mark= new HashMap<>();
        HashMap<Character,Integer> t_mark=new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            if(s_mark.get(s.charAt(i))==null){
                s_mark.put(s.charAt(i),1);
                continue;
            }
            s_mark.put(s.charAt(i),s_mark.get(s.charAt(i))+1);
        }
        for (int i = 0; i < t.length(); i++) {
            if(t_mark.get(t.charAt(i))==null){
                t_mark.put(t.charAt(i),1);
                continue;
            }
            t_mark.put(t.charAt(i),t_mark.get(t.charAt(i))+1);
        }
        for(Character c:s_mark.keySet()){
            t_mark.put(c,t_mark.get(c)-s_mark.get(c));
        }
        for(Character c:t_mark.keySet()){
            if(t_mark.get(c)!=0){
                return c;
            }
        }
        return ' ';
    }
}
