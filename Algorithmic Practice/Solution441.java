public class Solution441 {
    public static int arrangeCoins(int n) {
        int i = 1;
        while(n - i >= 0){
            n -= i++;
        }
        return i-1;
    }

    public static void main(String[] args) {
        System.out.println(arrangeCoins(5));
    }
}
