public class Offer22 {
    class ListNode {
        int val;
        ListNode next;
        ListNode(int x) { val = x; }
    }
    public ListNode getKthFromEnd(ListNode head, int k) {
        ListNode tmpNode = head;
        int size = 0;
        while(tmpNode != null){
            size++;
            tmpNode = tmpNode.next;
        }
        tmpNode = head;
        for (int i = 0; i < size - k; i++) {
            tmpNode = tmpNode.next;
        }
        return tmpNode;
    }
}
