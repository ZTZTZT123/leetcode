import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class
Solution338 {
    public int updateMark(List<Integer> mark,int tarCount){
        boolean flag=true;
        for (int i = mark.size()-1; i >=0 ; i--) {
            if(flag==false){
                break;
            }
            int tmp= mark.get(i)+1;
            if(tmp>1){
                mark.set(i,0);
                flag=true;
                tarCount--;
            }
            else{
                mark.set(i,tmp);
                flag=false;
                tarCount++;
            }
        }
        if(flag){
            mark.add(0,1);
            tarCount++;
        }
        return tarCount;
    }
    public int[] countBits(int num) {
        int tarCount=0;
        List<Integer> mark=new ArrayList<>();
        int[] result=new int[num+1];
        for (int i = 0; i <= num; i++) {
            if(i==0){
                result[i]=0;
                continue;
            }
            result[i]=updateMark(mark,tarCount);
            tarCount=result[i];
        }
        return result;
    }

    public static void main(String[] args) {
        Solution338 s=new Solution338();
        System.out.println(s.countBits(2));
    }
}
