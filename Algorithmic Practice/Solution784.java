import java.util.ArrayList;
import java.util.List;

public class Solution784 {

    private boolean isLetter(char c){
        return ('a' <= c && c <= 'z') ||
                ('A' <= c && c <= 'Z');
    }

    private void dfs(List<String> res, char[] charArr, int ind){
        for(; ind < charArr.length && !isLetter(charArr[ind]); ind++);
        if(ind >= charArr.length){
            res.add(new String(charArr));
            return;
        }
        charArr[ind] = Character.toLowerCase(charArr[ind]);
        dfs(res, charArr, ind + 1);
        charArr[ind] = Character.toUpperCase(charArr[ind]);
        dfs(res, charArr, ind + 1);
    }

    public List<String> letterCasePermutation(String s) {
        List<String> res = new ArrayList<>();
        dfs(res, s.toCharArray(), 0);
        return res;
    }

    public static void main(String[] args){
        Solution784 s = new Solution784();
        s.letterCasePermutation("a1b2");
    }
}
