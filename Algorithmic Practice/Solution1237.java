import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Solution1237 {

    interface CustomFunction {
        int f(int x, int y);
    }

    final int maxSize = 1000;
    public List<List<Integer>> findSolution(CustomFunction customfunction, int z) {
        List<List<Integer>> res = new ArrayList<>();



        for(int x = 1; x <= maxSize; x++) {
            int left = 1, right = maxSize;
            while(left <= right){
                int y = left + (right - left) / 2;
                int midNum = customfunction.f(x, y);
                if(midNum == z){
                    res.add(Arrays.asList(x, y));
                    break;
                }
                else if(midNum < z){
                    left = y + 1;
                }
                else {
                    right = y - 1;
                }
            }
        }


        return res;
    }

    public static void main(String[] args){
        Solution1237 s = new Solution1237();
        s.findSolution(new CustomFunction() {
            @Override
            public int f(int x, int y) {
                return x + y;
            }
        }, 5);
    }
}
