public class Solution938 {
    int sum=0;

    public void doTravel(TreeNode tmpNode,int low,int high){
        if(tmpNode==null){
            return;
        }
        boolean travelLeft=true;
        boolean travelRight=true;
        int tmpVal=tmpNode.val;
        if(tmpVal<low) travelLeft=false;
        else if(tmpVal>high) travelRight=false;

        if(travelLeft){
            doTravel(tmpNode.left,low,high);
        }

        if(low<=tmpVal&&tmpVal<=high){
            sum+=tmpVal;
        }

        if(travelRight){
            doTravel(tmpNode.right,low,high);
        }
    }

    public int rangeSumBST(TreeNode root, int low, int high) {
        doTravel(root, low, high);
        return sum;
    }
}
