import java.util.Arrays;
import java.util.Comparator;

public class Solution748 {

    private int[] getCounter(String str){
        int[] counter = new int[26];
        for (int i = 0; i < str.length(); i++) {
            char c = Character.toLowerCase(str.charAt(i));
            if('a' <= c && c <= 'z')
                counter[c - 'a']++;
        }
        return counter;
    }

    private boolean judge(int[] targetCounter, int[] tmpCounter){
        for (int i = 0; i < 26; i++) {
            if(targetCounter[i] > tmpCounter[i])
                return false;
        }
        return true;
    }

    public String shortestCompletingWord(String licensePlate, String[] words) {
        int[] targetCounter = getCounter(licensePlate);
        Arrays.sort(words, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.length() - o2.length();
            }
        });
        for (String word : words) {
            int[] tmpCounter = getCounter(word);
            if(judge(targetCounter, tmpCounter)){
                return word;
            }
        }
        return null;
    }
}
