import java.util.Stack;

public class Solution1614 {
    public int maxDepth(String s) {
        int strLen = s.length();
        int bracketNum = 0;
        int maxDepth = bracketNum;
        for (int i = 0; i < strLen; i++) {
            char c = s.charAt(i);
            if(c == '('){
                bracketNum++;
                maxDepth = Math.max(bracketNum, maxDepth);
            }
            else if(c == ')'){
                bracketNum--;
            }
        }
        return maxDepth;
    }
}
