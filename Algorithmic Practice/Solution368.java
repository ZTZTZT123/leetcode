import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Solution368 {
    public List<Integer> largestDivisibleSubset(int[] nums) {
        Arrays.sort(nums);
        List<Integer>[] dp=new List[nums.length];
        for (int i = 0; i < dp.length; i++) {
            List<Integer> tmpList=new ArrayList<>();
            tmpList.add(nums[i]);
            dp[i]=tmpList;
        }
        for (int i = 0; i < nums.length; i++) {
            int maxSize=0;
            int maxInd=-1;
            for (int j = 0; j < i; j++) {
                int lastNum=nums[j];
                int tmpNum=nums[i];
                if(tmpNum%lastNum==0){
                    List<Integer> tmplist=dp[j];
                    if(tmplist.size()>maxSize){
                        maxSize=tmplist.size();
                        maxInd=j;
                    }
                }
            }
            if(maxInd!=-1) dp[i].addAll(dp[maxInd]);
        }

        int maxInd=0;
        int maxSize=0;
        for (int i = 0; i < dp.length; i++) {
            List<Integer> tmpList=dp[i];
            if(tmpList.size()>maxSize){
                maxSize=tmpList.size();
                maxInd=i;
            }
        }
        return dp[maxInd];
    }

    public static void main(String[] args) {
        Solution368 s=new Solution368();
        System.out.println(s.largestDivisibleSubset(new int[]{1,2,3}));
    }
}
