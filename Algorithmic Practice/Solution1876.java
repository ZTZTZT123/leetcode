public class Solution1876 {

    public int countGoodSubstrings(String s) {
        if(s.length() < 3){return 0;}
        int res = 0;
        char loc1 = s.charAt(0), loc2 = s.charAt(1), loc3 = s.charAt(2);
        for (int i = 3; i < s.length(); i++) {
            if(!(loc1 == loc2 || loc1 == loc3 || loc2 == loc3)){
                res++;
            }
            loc1 = loc2; loc2 = loc3; loc3 = s.charAt(i);
        }if(!(loc1 == loc2 || loc1 == loc3 || loc2 == loc3)){
            res++;
        }
        return res;
    }
}
