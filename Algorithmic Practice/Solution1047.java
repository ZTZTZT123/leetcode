import java.util.Stack;

public class Solution1047 {
    public String removeDuplicates(String S) {
        Stack<Character> mark=new Stack<>();
        for (int i = S.length()-1; i >=0; i--) {
            char tmp=S.charAt(i);
            if(mark.empty()||mark.peek()!=tmp){
                mark.push(tmp);
            }
            else{
                mark.pop();
            }
        }
        StringBuilder str = new StringBuilder();
        while(!mark.empty()){
            str.append(mark.pop());
        }
        return str.toString();
    }

    public static void main(String[] args) {
        Solution1047 s=new Solution1047();
        System.out.println(s.removeDuplicates("azxxzy"));
    }
}
