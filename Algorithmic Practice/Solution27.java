import java.util.ArrayList;
import java.util.List;

public class Solution27 {
    public int removeElement(int[] nums, int val) {
        int n=nums.length;
        List<Integer> newNums=new ArrayList<>(n);
        for (int i:nums) {
            if(i!=val){
                newNums.add(i);
            }
        }
        for (int i = 0; i < newNums.size(); i++) {
            nums[i]=newNums.get(i);
        }
        return newNums.size();
    }
}
