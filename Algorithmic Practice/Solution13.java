import java.util.HashMap;
import java.util.Map;

public class Solution13 {
    static Map<String,Integer> romaChar;

    static Map<String,Integer> specChar;

    static {
        romaChar=new HashMap<>();
        romaChar.put("I",1);
        romaChar.put("V",5);
        romaChar.put("X",10);
        romaChar.put("L",50);
        romaChar.put("C",100);
        romaChar.put("D",500);
        romaChar.put("M",1000);

        specChar=new HashMap<>();
        specChar.put("IV",4);
        specChar.put("IX",9);
        specChar.put("XL",40);
        specChar.put("XC",90);
        specChar.put("CD",400);
        specChar.put("CM",900);
    }

    public int romanToInt(String s) {
        int result=0;
        int ind=0;
        while(ind<s.length()){
            String twoStr=s.substring(ind,(ind+2)<=s.length()? (ind+2):(ind+1));
            String tmpStr=String.valueOf(s.charAt(ind));
            if(specChar.containsKey(twoStr)){
                result+=specChar.get(twoStr);
                ind+=2;
            }
            else{
                result+=romaChar.get(tmpStr);
                ind++;
            }
        }
        return result;
    }
}
