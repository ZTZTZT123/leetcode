class Solution91 {
    public boolean doJudge(String str){
        if(str.charAt(0)=='0') return false;
        int tmpNum=Integer.parseInt(str);
        return (1<=tmpNum&&tmpNum<=26);
    }

    public int numDecodings(String s) {
        int n = s.length();
        int[] dp = new int[n];
        dp[0]=s.charAt(0)=='0'? 0:1;
        for (int i = 1; i < n; ++i) {
            String lstChar=String.valueOf(s.charAt(i-1));
            String tmpChar=String.valueOf(s.charAt(i));
            String tmpStr=lstChar+tmpChar;
            if(doJudge(tmpChar)){
                dp[i]+=dp[i-1];
            }
            if(doJudge(tmpStr)){
                int num= i-2>=0? dp[i-2]:1;
                dp[i]+=num;
            }
        }
        return dp[n-1];
    }

    public static void main(String[] args) {
        Solution91 s=new Solution91();
        System.out.println(s.numDecodings("06"));
    }
}
