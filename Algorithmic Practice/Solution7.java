public class Solution7 {
    public int reverse(int x) {
        boolean isNeg=false;
        if(x<0){
            isNeg=true;
            x=-x;
        }
        long res=0;
        while(x>0){
            res=res*10+x%10;
            x/=10;
        }
        res=isNeg? -res:res;
        res=res>=Integer.MIN_VALUE&&res<=Integer.MAX_VALUE? res:0;
        return (int) res;
    }
}
