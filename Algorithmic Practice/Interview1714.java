import java.util.PriorityQueue;

public class Interview1714 {

    private PriorityQueue<Integer> queue = new PriorityQueue<>();

    public int[] smallestK(int[] arr, int k) {
        for (int i = 0; i < arr.length; i++) {
            queue.offer(arr[i]);
        }
        int[] res = new int[k];
        for (int i = 0; i < k; i++) {
            res[i] = queue.poll();
        }
        return res;
    }
}
