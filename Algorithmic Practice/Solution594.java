import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Solution594 {
    public int findLHS(int[] nums) {
        int res = 0, len = nums.length;
        Arrays.sort(nums);
        for (int i = 0, j = 0; j < len; j++) {
            while(i < j && nums[i] + 1 < nums[j]){i++;}
            if(nums[i] + 1 == nums[j]){res = Math.max(res, j - i + 1);}
        }
        return res;
    }
}
