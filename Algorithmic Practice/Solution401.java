import java.util.ArrayList;
import java.util.List;

public class Solution401 {
    static int[] numTable=new int[]{1,2,4,8,16,32};

    static List<Integer>[] hourTable;

    static List<Integer>[] minTable;

    static {
        hourTable=new List[4];
        minTable=new List[6];
        for (int i = 0; i < 4; i++) {
            hourTable[i]=new ArrayList<>();
        }
        for (int i = 0; i < 6; i++) {
            minTable[i]=new ArrayList<>();
        }
        hourTravel(hourTable,3,0,0,0);
        minTravel(minTable,5,0,0,0);
    }

    static private void hourTravel(List<Integer>[] res,int hourCount,int tmpNum,int tmpCount,int ind){
        if(tmpNum>11){
            return;
        }
        if(tmpCount<=hourCount){
            res[tmpCount].add(tmpNum);
        }
        for (int i = ind; i < 4; i++) {
            hourTravel(res,hourCount,tmpNum+numTable[i],tmpCount+1,i+1);
        }
    }

    static private void minTravel(List<Integer>[] res,int minCount,int tmpNum,int tmpCount,int ind){
        if(tmpNum>59){
            return;
        }
        if(tmpCount<=minCount){
            res[tmpCount].add(tmpNum);
        }
        for (int i = ind; i < 6; i++) {
            minTravel(res,minCount,tmpNum+numTable[i],tmpCount+1,i+1);
        }
    }

    public List<String> readBinaryWatch(int turnedOn) {
        List<String> res=new ArrayList<>();
        if(turnedOn>8){
            return res;
        }
        for (int hourCount = 0; hourCount <= Math.min(3,turnedOn); hourCount++) {
            int minCount=turnedOn-hourCount;
            if(hourCount>3||minCount>5){
                continue;
            }
            List<Integer> hourList=hourTable[hourCount];
            List<Integer> minList=minTable[minCount];
            for (Integer hour:hourList) {
                for (Integer min:minList) {
                    res.add(hour+":"+(min<10? "0":"")+min);
                }
            }
        }
        return res;
    }

    public static void main(String[] args) {
        Solution401 s=new Solution401();
        System.out.println(s.readBinaryWatch(1));
    }
}
