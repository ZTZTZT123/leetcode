import java.util.*;

public class XieChengTest {
    static String doTravel(String tmpStr,int count){
        if(count==0){
            return tmpStr;
        }
        return doTravel("G",count-1)+tmpStr+doTravel("R",count-1);
    }

    static String buildingHouse(String n) {
        for (int i = 0; i < n.length(); i++) {
            if (!Character.isDigit(n.charAt(i))) {
                return "N";
            }
        }
        int num=Integer.parseInt(n);
        if(num>12){
            return "O";
        }
        return doTravel("R",num-1);
    }




    static void dotravel(Map<Integer,List<Integer>> mark,Set<Integer> set,int tmpPack){
        List<Integer> tmpList = mark.getOrDefault(tmpPack,null);
        if(tmpList==null){
            return;
        }
        for (int i = 0; i < tmpList.size(); i++) {
            int tmpNum=tmpList.get(i);
            if(set.contains(tmpNum)){
                continue;
            }
            set.add(tmpNum);
            dotravel(mark,set,tmpNum);
        }
    }

    static void test_2(){
        Scanner scanner=new Scanner(System.in);
        int pack=Integer.parseInt(scanner.nextLine());
        List<List<Integer>> list=new ArrayList<>();
        while(scanner.hasNextLine()){
            String str=scanner.nextLine();
            if(str.equals("")){
                break;
            }
            String[] strs=str.split(",");
            List<Integer> tmpList=new ArrayList<>();
            for (int i = 0; i < strs.length; i++) {
                tmpList.add(Integer.parseInt(strs[i]));
            }
            list.add(tmpList);
        }
        Map<Integer,List<Integer>> mark=new HashMap<>();
        for (int i = 0; i < list.size(); i++) {
            List<Integer> tmpList=list.get(i);
            int head=tmpList.get(0);
            for (int j = 1; j < tmpList.size(); j++) {
                int tmpNum=tmpList.get(j);
                List<Integer> resList=mark.getOrDefault(tmpNum,new ArrayList<>());
                resList.add(head);
                mark.put(tmpNum,resList);
            }
        }
        Set<Integer> set=new HashSet<>();
        dotravel(mark,set,pack);
        int sum=0;
        for (int i:set) {
            sum+=i;
        }
        System.out.println(sum==0? -1:sum);
    }

    public static void main(String[] args) {
        test_2();
    }

}
