import java.util.ArrayList;
import java.util.List;

public class Solution838 {
    public String pushDominoes(String dominoes) {
        char[] fixCharArr = dominoes.toCharArray();
        int len = fixCharArr.length;
        List<int[]> opList = new ArrayList<>();
        while(true) {
            for (int i = 0; i < len; i++) {
                char tmpC = fixCharArr[i];
                if (tmpC != '.') {
                    continue;
                }
                char leftC = i == 0 ? 'N' : fixCharArr[i - 1];
                char rightC = i + 1 == len ? 'N' : fixCharArr[i + 1];
                if (leftC == 'R' && rightC != 'L') {
                    opList.add(new int[]{i, 'R'});
                } else if (leftC != 'R' && rightC == 'L') {
                    opList.add(new int[]{i, 'L'});
                }
            }
            if(opList.isEmpty()){
                break;
            }
            for(int[] tmpOp : opList){
                fixCharArr[tmpOp[0]] = (char)tmpOp[1];
            }
            opList.clear();
        }
        return new String(fixCharArr);
    }

    public static void main(String[] args) {
        Solution838 s = new Solution838();
        s.pushDominoes("RR.L");
    }
}
