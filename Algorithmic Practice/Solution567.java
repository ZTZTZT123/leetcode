import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class Solution567 {
    public boolean checkIsEmpty(HashMap<Character,Integer> check){
        for (Integer num: check.values()) {
            if(num!=0){
                return false;
            }
        }
        return true;
    }

    public boolean checkInclusion(String s1, String s2) {
        HashMap<Character,Integer> mark=new HashMap<>();
        for (int i = 0; i < s1.length(); i++) {
            if (mark.containsKey(s1.charAt(i))) {
                mark.put(s1.charAt(i),mark.get(s1.charAt(i))+1);
                continue;
            }
            mark.put(s1.charAt(i),1);
        }
        HashMap<Character,Integer> check=new HashMap<>();
        check.putAll(mark);

        for (int i = 0; i <= s2.length()-s1.length(); i++) {
            if(check.containsKey(s2.charAt(i))==false){
                continue;
            }
            String subStr=s2.substring(i,i+s1.length());
            for (int j = 0; j < subStr.length(); j++){
                Character c=subStr.charAt(j);
                if(check.containsKey(c)==false||check.get(c)<=0) {
                    break;
                }
                check.put(c, check.get(c) - 1);
            }
            if(checkIsEmpty(check)){
                return true;
            }
            check.clear();
            check.putAll(mark);
        }
        return false;
    }

    public static void main(String[] args) {
        Solution567 s=new Solution567();
        System.out.println(s.checkInclusion("ab",
                "eidboaoo"));
    }
}
