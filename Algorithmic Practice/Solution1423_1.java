public class Solution1423_1 {
    private int result=0;
    public int[] extractFirst(int[] cardPoints){
        int[] newArr=new int[cardPoints.length-1];
        for (int i = 1; i < cardPoints.length; i++) {
            newArr[i-1]=cardPoints[i];
        }
        return newArr;
    }

    public int[] extractLast(int[] cardPoints){
        int[] newArr=new int[cardPoints.length-1];
        for (int i = 0; i < cardPoints.length-1; i++) {
            newArr[i]=cardPoints[i];
        }
        return newArr;
    }

    public void doExtract(int[] cardPoints,int K,int tmp){
        if(K==0){
            if(tmp>result){
                result=tmp;
            }
            return;
        }
        int first=cardPoints[0];
        int last=cardPoints[cardPoints.length-1];
        int[] firstArr=extractFirst(cardPoints);
        doExtract(firstArr,K-1,tmp+first);

        int[] lastArr=extractLast(cardPoints);
        doExtract(lastArr,K-1,tmp+last);


    }
    public int maxScore(int[] cardPoints, int k) {
        doExtract(cardPoints,k,0);
        return result;
    }
}
