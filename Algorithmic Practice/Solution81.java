public class Solution81 {
    public boolean binSearch(int[] nums,int start,int end,int target){
        while(start<=end){
            int tmpInd=(start+end)/2;
            int tmpNum=nums[tmpInd];
            if(target==tmpNum){
                return true;
            }
            else if(target<tmpNum){
                end=tmpInd-1;
            }
            else{
                start=tmpInd+1;
            }
        }
        return false;
    }

    public boolean search(int[] nums, int target) {
        int spilitInd=0;
        for (int i = 1; i < nums.length; i++) {
            if(nums[i]<nums[i-1]){
                spilitInd=i;
                break;
            }
        }
        return binSearch(nums,0,spilitInd-1,target)||binSearch(nums,spilitInd,nums.length-1,target);
    }
}
