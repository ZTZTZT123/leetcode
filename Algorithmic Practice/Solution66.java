public class Solution66 {

    private int[] getProcessArr(int[] digits){
        int[] res = new int[digits.length + 1];
        res[0] = 1;
        for (int i = 0; i < digits.length; i++) {
            res[i + 1] = digits[i];
        }
        return res;
    }

    public int[] plusOne(int[] digits) {
        int tmpInd = digits.length - 1;
        digits[tmpInd]++;
        while(digits[tmpInd] >= 10){
            digits[tmpInd] = 0;
            if(--tmpInd < 0){
                return getProcessArr(digits);
            }
            digits[tmpInd]++;
        }
        return digits;
    }
}
