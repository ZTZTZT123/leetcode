public class Solution995 {
    public int getNum(int A,int mark){
        int tmp=A;
        if(mark%2==1){
            tmp=tmp==1? 0:1;
        }
        return tmp;
    }
    public int minKBitFlips(int[] A, int K) {
        int result=0;
        int[] mark=new int[A.length];
        for (int i = 0; i <= A.length-K; i++) {
            if(getNum(A[i],mark[i] )==0){
                result++;
                for (int j = i; j < i+K; j++) {
                    mark[j]++;
                }
            }
        }
        for (int i = A.length-K+1; i < A.length; i++) {
            if(getNum(A[i],mark[i] )==0){
                result=-1;
            }
        }
        return result;
    }
}
