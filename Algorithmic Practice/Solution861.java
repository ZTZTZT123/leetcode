import java.util.ArrayList;

public class Solution861 {
    public void do_row_process(int[][] A,int index){
        for (int i = 0; i < A[index].length; i++) {
            A[index][i]=A[index][i]==1? 0:1;
        }
    }
    public void do_col_process(int[][] A,int index){
        for (int i = 0; i < A.length; i++) {
            A[i][index]=A[i][index]==1? 0:1;
        }
    }
    public int matrixScore(int[][] A) {
        ArrayList<Integer> map_to_val=new ArrayList<Integer>();
        for (int i = 0; i < A[0].length; i++) {
            map_to_val.add((int) Math.pow(2,A[0].length-i-1));
        }

        for (int i = 0; i < A.length; i++) {
            if(A[i][0]==0){
                do_row_process(A,i);
            }
        }

        for (int i = 1; i < A[0].length; i++) {
            int one_num=0,zero_num=0;
            for (int j = 0; j < A.length; j++) {
                if(A[j][i]==0){
                    zero_num=zero_num+1;
                }
                else{
                    one_num=one_num+1;
                }
            }
            if(one_num<zero_num){
                do_col_process(A,i);
            }
        }

        int result_sum=0;
        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < A[0].length; j++) {
                result_sum+=A[i][j]*map_to_val.get(j);
            }
        }
        return result_sum;
    }

    public static void main(String[] args) {
        Solution861 s=new Solution861();
        System.out.println(s.matrixScore(new int[][]{{0,0,1,1},{1,0,1,0},{1,1,0,0}}));
    }
}
