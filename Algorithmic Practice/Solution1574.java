public class Solution1574 {
    public int findLengthOfShortestSubarray(int[] arr) {
        int length = arr.length, right = length - 1;

        for(; right - 1 >= 0 && arr[right - 1] <= arr[right]; right--);

        int res = right;

        for(int left = 0; left < length; left++) {
            for(; right < length && arr[left] > arr[right]; right++);
            res = Math.min(res, right - left - 1);
            if(left + 1 < length && arr[left] > arr[left + 1]){
                break;
            }
        }
        return Math.max(0, res);
    }

    public static void main(String[] args){
        Solution1574 s = new Solution1574();
        System.out.println(s.findLengthOfShortestSubarray(new int[]{10,13,17,21,15,15,9,17,22,22,13}));
    }
}
