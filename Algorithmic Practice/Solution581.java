import java.util.*;

public class Solution581 {
    public int findUnsortedSubarray(int[] nums) {
        int[] copyNums = new int[nums.length];
        System.arraycopy(nums, 0, copyNums, 0, nums.length);
        Arrays.sort(copyNums);
        Integer begin = null, end = null;
        for (int i = 0; i < nums.length; i++) {
            if(copyNums[i] != nums[i]){
                if(begin == null){
                    begin = i;
                }
                end = i;
            }
        }
        if(begin != null && end != null){
            return end - begin + 1;
        }
        return 0;
    }

    public static void main(String[] args) {
        Solution581 s = new Solution581();
        s.findUnsortedSubarray(new int[]{2,3,3,2,4});
    }
}
