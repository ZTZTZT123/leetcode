public class Solution28 {
    public boolean doJudge(String haystack,int beg,int end,String needle){
        for (int i = beg; i < end; i++) {
            int neeInd=i-beg;
            if(needle.charAt(neeInd)!=haystack.charAt(i)){
                return false;
            }
        }
        return true;
    }

    public int strStr(String haystack, String needle) {
        if(needle.equals("")) return 0;
        int travelLen=haystack.length()-needle.length();
        int needleLen=needle.length();
        for (int i = 0; i <= travelLen; i++) {
            if(doJudge(haystack,i,i+needleLen,needle)){
                return i;
            }
        }
        return -1;
    }
}
