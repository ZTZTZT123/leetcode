import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

class ListNode {
    int val;
    ListNode next;
    ListNode(int x) {
        val = x;
        next = null;
    }
}
public class Solution160 {

    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        Set<ListNode> nodes=new HashSet<>();
        ListNode tmpNode=headA;
        while(tmpNode!=null){
            nodes.add(tmpNode);
            tmpNode=tmpNode.next;
        }
        tmpNode=headB;
        while(tmpNode!=null){
            if(nodes.contains(tmpNode)){
                return tmpNode;
            }
            tmpNode=tmpNode.next;
        }
        return null;
    }
}
