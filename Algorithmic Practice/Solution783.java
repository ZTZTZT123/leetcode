import java.util.LinkedList;
import java.util.List;

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode() {}
    TreeNode(int val) { this.val = val; }
    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}



public class Solution783 {
    public void travel(List<Integer> mark,TreeNode root){
        if(root==null){
            return;
        }
        travel(mark,root.left);
        mark.add(root.val);
        travel(mark,root.right);
    }

    public int minDiffInBST(TreeNode root) {
        List<Integer> mark=new LinkedList<>();
        travel(mark,root);
        int minDiff=Integer.MAX_VALUE;
        for (int i = 1; i < mark.size(); i++) {
            int tmpNum=mark.get(i);
            int lastNum=mark.get(i-1);
            minDiff=Math.min(Math.abs(tmpNum-lastNum),minDiff);
        }
        return minDiff;
    }
}
