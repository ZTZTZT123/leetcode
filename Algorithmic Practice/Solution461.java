public class Solution461 {
    public int hammingDistance(int x, int y) {
        String strX=Integer.toBinaryString(x);
        String strY=Integer.toBinaryString(y);
        strX=new StringBuilder(strX).reverse().toString();
        strY=new StringBuilder(strY).reverse().toString();
        int maxLen=Math.max(strX.length(),strY.length());
        int result=0;
        for (int i = 0; i < maxLen; i++) {
            char tmpCharX=i<strX.length()? strX.charAt(i):'0';
            char tmpCharY=i<strY.length()? strY.charAt(i):'0';
            if(tmpCharX!=tmpCharY){
                result++;
            }
        }
        return result;
    }
}
