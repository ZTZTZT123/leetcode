import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Solution386 {

    private void travel(List<Integer> res, int tmpNum, int n){
        if(tmpNum > n){
            return;
        }
        res.add(tmpNum);
        int base = tmpNum * 10;
        for(int i = 0; i < 10; i++){
            travel(res, base + i, n);
        }
    }

    public List<Integer> lexicalOrder(int n) {
        List<Integer> res = new ArrayList<>(n);
        for(int i = 1; i < 10; i++){
            travel(res, i, n);
        }
        return res;
    }

    public static void main(String[] args) {
        Solution386 s = new Solution386();
        s.lexicalOrder(13);
    }
}
