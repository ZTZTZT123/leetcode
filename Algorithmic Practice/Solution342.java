import java.util.HashSet;
import java.util.Set;

public class Solution342 {

    static Set<Integer> mark;

    static{
        mark=new HashSet<>();
        long tmpNum=1;
        while(tmpNum<=Integer.MAX_VALUE){
            mark.add((int) tmpNum);
            tmpNum*=4;
        }
    }

    public boolean isPowerOfFour(int n) {
        return mark.contains(n);
    }
}
