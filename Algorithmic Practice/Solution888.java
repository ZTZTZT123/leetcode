public class Solution888 {
    public int[] fairCandySwap(int[] A, int[] B) {
        int sumA=0;
        int sumB=0;
        int[] res=new int[2];
        for (int tmpA:A) {
            sumA+=tmpA;
        }
        for (int tmpB:B) {
            sumB+=tmpB;
        }
        boolean Abigger=false;
        if(sumA>sumB){
            Abigger=true;
        }
        jump:for (int tmpA:A) {
            for(int tmpB:B){
                if(Abigger==true){
                    if(tmpA<tmpB){
                        continue;
                    }
                }
                else{
                    if(tmpA>tmpB){
                        continue;
                    }
                }
                if(sumA-tmpA+tmpB==sumB-tmpB+tmpA){
                    res[0]=tmpA;
                    res[1]=tmpB;
                    break jump;
                }
            }
        }
        return res;
    }
}
