import java.util.*;

public class Solution299 {
    public String getHint(String secret, String guess) {
        int resA = 0, resB = 0;
        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < secret.length(); i++) {
            char tmpC = secret.charAt(i);
            map.put(tmpC, map.getOrDefault(tmpC, 0) + 1);
        }
        for (int i = 0; i < guess.length(); i++) {
            char tmpC = guess.charAt(i);
            int tmpCount = map.getOrDefault(tmpC, 0);
            if(tmpCount > 0){
                resB++;
                map.put(tmpC, tmpCount - 1);
            }
        }
        for (int i = 0; i < secret.length(); i++) {
            if(secret.charAt(i) == guess.charAt(i)){
                resA++; resB--;
            }
        }
        return resA + "A" + resB + "B";
    }
}
