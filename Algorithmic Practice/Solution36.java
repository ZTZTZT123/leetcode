public class Solution36 {

    public boolean isValidSudoku(char[][] board) {
        boolean[][] rows = new boolean[9][9];
        boolean[][] cols = new boolean[9][9];
        boolean[][][] blocks = new boolean[3][3][9];

        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                char tmpC = board[i][j];
                if(tmpC == '.'){
                    continue;
                }
                int loc = tmpC - '1';

                if(rows[i][loc]){
                    return false;
                }
                rows[i][loc] = true;

                if(cols[j][loc]){
                    return false;
                }
                cols[j][loc] = true;

                if(blocks[i/3][j/3][loc]){
                    return false;
                }
                blocks[i/3][j/3][loc] = true;
            }
        }
        return true;
    }
}
