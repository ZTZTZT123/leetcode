
public class Solution1410 {

    int findNearer(int leftLoc, int rightLoc, String str){
        for (int i = rightLoc - 1; i >= leftLoc; i--) {
            if(str.charAt(i) == '&'){
                return i;
            }
        }
        return leftLoc;
    }
    String mapping(String str){
        if(str.equals("&quot;")){
            return "\"";
        }else if(str.equals("&apos;")){
            return "\'";
        }else if(str.equals("&amp;")){
            return "&";
        }else if(str.equals("&gt;")){
            return ">";
        }else if(str.equals("&lt;")){
            return "<";
        }else if(str.equals("&frasl;")){
            return "/";
        }else {
            return str;
        }
    }

    public String entityParser(String text) {
        StringBuilder sb = new StringBuilder();
        int tmpLoc = 0;
        while(true){
            int rightLoc = text.indexOf(';', tmpLoc);
            int leftLoc = text.indexOf('&', tmpLoc);
            if(leftLoc == -1 || rightLoc == -1){
                sb.append(text.substring(tmpLoc));
                break;
            }
            leftLoc = findNearer(leftLoc, rightLoc, text);
            sb.append(text.substring(tmpLoc, leftLoc));
            if(leftLoc < rightLoc){
                sb.append(mapping(text.substring(leftLoc, rightLoc + 1)));
            }
            tmpLoc = rightLoc + 1;
        }
        return sb.toString();
    }
}
