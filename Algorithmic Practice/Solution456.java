public class Solution456 {

    public boolean find132pattern(int[] nums) {
        int[][] DP=new int[nums.length][2];//[0]:1的大小。[1]:size
        for (int i = 0; i < DP.length; i++) {
            DP[i][1]=1;
        }
        for (int i = 0; i < DP.length; i++) {
            for (int j = 0; j < i; j++) {
                if(DP[i][1]==1){
                    if(nums[j]<nums[i]){
                        DP[i][1]++;
                        DP[i][0]=nums[j];
                    }
                }
                else if(DP[i][1]==2){
                    if(nums[j]<nums[i]) {
                        DP[i][0] = Math.min(DP[i][0], nums[j]);
                    }
                }
                if(DP[j][1]==2){
                    if(nums[j]>nums[i]&&nums[i]>DP[j][0]){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static void main(String[] args) {
        Solution456 s=new Solution456();
        System.out.println(s.find132pattern(new int[]{1,2,3,4}));
    }
}
