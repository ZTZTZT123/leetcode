public class Solution474 {
    int maxRes=0;
    int m;
    int n;


    public int[] getCount(String str){
        int[] count=new int[2];
        for (int i = 0; i < str.length(); i++) {
            char c=str.charAt(i);
            count[c-'0']++;
        }
        return count;
    }


    public void travel(int[][] counts,int tmpZero,int tmpOne,int tmpCount,int Ind){
        if(tmpZero>m||tmpOne>n){
            return;
        }
        maxRes=Math.max(maxRes,tmpCount);
        for (int i = Ind; i < counts.length; i++) {
            int[] count=counts[i];
            travel(counts,tmpZero+count[0],tmpOne+count[1],tmpCount+1,i+1);
        }
    }


    public int findMaxForm(String[] strs, int m, int n) {
        this.m=m;
        this.n=n;
        int[][] counts=new int[strs.length][2];
        for (int i = 0; i < strs.length; i++) {
            counts[i]=getCount(strs[i]);
        }
        travel(counts,0,0,0,0);
        return maxRes;
    }
}
