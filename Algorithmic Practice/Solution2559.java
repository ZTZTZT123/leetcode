import java.util.*;
import java.util.stream.Collectors;

public class Solution2559 {

    static Set<Character> vowelCharSet;

    static {
        vowelCharSet = new HashSet<>();
        vowelCharSet.add('a');
        vowelCharSet.add('e');
        vowelCharSet.add('i');
        vowelCharSet.add('o');
        vowelCharSet.add('u');
    }

    public boolean judge(String word){
        return vowelCharSet.contains(word.charAt(0)) && vowelCharSet.contains(word.charAt(word.length() - 1));
    }


    public int[] getPrefixArr(String[] words){
        int[] prefixArr = new int[words.length];
        prefixArr[0] = judge(words[0]) ? 1 : 0;
        for (int i = 1; i < words.length; i++) {
            prefixArr[i] = prefixArr[i - 1] + (judge(words[i]) ? 1 : 0);
        }
        return prefixArr;
    }


    public int doQueryWithPrefixArr(int[] prefixArr, int[] query){
        int left = query[0] - 1, right = query[1];
        int leftCount = left < 0 ? 0 : prefixArr[left], rightCount = prefixArr[right];
        return rightCount - leftCount;
    }



    public int[] vowelStrings(String[] words, int[][] queries) {
        int[] prefixArr = getPrefixArr(words);
        int[] res = new int[queries.length];
        for (int i = 0; i < queries.length; i++) {
            res[i] = doQueryWithPrefixArr(prefixArr, queries[i]);
        }
        return res;
    }
}
