public class Solution643 {
    public double findMaxAverage(int[] nums, int k) {
        int left=0;
        int right=0;
        int result=0;
        int val=0;
        while(right< nums.length){
            val+=nums[right];
            while(right-left+1>k){
                val-=nums[left];
                left++;
            }
            if(right-left+1==k&&(val>result||result==0)){
                result=val;
            }
            right++;
        }
        return (double)result/k;
    }

    public static void main(String[] args) {
        Solution643 s=new Solution643();
        System.out.println(s.findMaxAverage(new int[]{-1},1));
    }
}
