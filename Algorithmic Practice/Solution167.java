import java.util.Arrays;

public class Solution167 {

    private int binarySearch(int[] numbers, int target){
        int left = 0, right = numbers.length - 1;
        while(left <= right){
            int midInd = (left + right) / 2;
            int midVal = numbers[midInd];
            if(target == midVal){
                return midInd;
            } else if(target < midVal){
                right = midInd - 1;
            } else {
                left = midInd + 1;
            }
        }
        return -1;
    }

    private int[] getResArr(int a, int b){
        if(a < b){
            return new int[]{a + 1, b + 1};
        }
        return new int[]{b + 1, a + 1};
    }

    public int[] twoSum(int[] numbers, int target) {
        for (int i = 0; i < numbers.length; i++) {
            int tmpVal = numbers[i];
            int tmpTarget = target - tmpVal;
            if(tmpVal == tmpTarget){
                if(numbers[i + 1] == tmpTarget){
                    return getResArr(i, i + 1);
                }
            }
            int otherInd = binarySearch(numbers, tmpTarget);
            if(otherInd != -1){
                return getResArr(i, otherInd);
            }
        }
        return null;
    }
}
