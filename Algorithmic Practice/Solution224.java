
import java.util.*;
import java.util.regex.Pattern;

public class Solution224 {
    public String removeSpace(String s){
        StringBuilder result=new StringBuilder("");
        for (int i = 0; i < s.length(); i++) {
            char tmp=s.charAt(i);
            if(tmp!=' '){
                result.append(tmp);
            }
        }
        return result.toString();
    }

    public  boolean isInteger(String str) {
        Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");
        return pattern.matcher(str).matches();
    }

    public int parseOperands(String s){
        if(isInteger(s)){
            return Integer.parseInt(s);
        }
        Stack<Integer> mark=new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            char tmp=s.charAt(i);
            if(tmp=='('){
                mark.push(i);
            }
            else if(tmp==')'){
                int beg=mark.pop();
                int brackResult=doCalculate(s.substring(beg+1,i));
                return parseOperands(s.substring(0,beg)+String.valueOf(brackResult)+s.substring(i+1));
            }
        }
        return -1;
    }

    public int doCalculate(String s){
        s=s.concat("+");
        int beg=0;
        char symbol='*';
        int result=100000;
        int mark=0;
        for (int i = 0; i < s.length(); i++) {
            char tmp=s.charAt(i);
            if(tmp=='('){
                mark++;
            }
            else if(tmp==')'){
                mark--;
            }
            else if((tmp=='-'||tmp=='+')&&mark==0){
                if(i==0||(s.charAt(i-1)=='+'||s.charAt(i-1)=='-')){
                    continue;
                }
                if(symbol=='*'){
                    result=parseOperands(s.substring(beg,i));
                    symbol=tmp;
                    beg=i+1;
                }
                else{
                    if(symbol=='+'){
                        result+=parseOperands(s.substring(beg,i));
                    }
                    else{
                        result-=parseOperands(s.substring(beg,i));
                    }
                    symbol=tmp;
                    beg=i+1;
                }
            }
        }
        return result;
    }

    public int calculate(String s) {
        s=removeSpace(s);
        return doCalculate(s);
    }

    public static void main(String[] args) {
        Solution224 s=new Solution224();
        System.out.println(s.calculate("(1-(3-4))"));
    }
}
