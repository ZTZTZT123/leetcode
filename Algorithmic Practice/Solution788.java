import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Solution788 {

  private boolean judgeIsGoodNum(int num){
    boolean isGood = false;
    for (; num > 0; num /= 10) {
      int tmpNum = num % 10;
      if(tmpNum == 2 || tmpNum == 5 || tmpNum == 6 || tmpNum == 9){
        isGood = true;
      }
      else if(tmpNum == 3 || tmpNum == 4 || tmpNum == 7){
        return false;
      }
    }
    return isGood;
  }



  public int rotatedDigits(int n) {
    int count = 0;
    for (int i = 0; i <= n; i++) {
      if(judgeIsGoodNum(i)){
        count++;
      }
    }
    return count;
  }

  public static void main(String[] args) {
    Solution788 s = new Solution788();
    s.rotatedDigits(857);
  }
}
