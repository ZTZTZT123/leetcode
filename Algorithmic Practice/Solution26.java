import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Solution26 {

    public int removeDuplicates(int[] nums) {
        List<Integer> newNums=new ArrayList<>(nums.length);
        if(nums.length==0) return 0;
        newNums.add(nums[0]);
        for (int i = 1; i < nums.length; i++) {
            int tmpNum=nums[i];
            if(newNums.get(newNums.size()-1)==tmpNum){
                continue;
            }
            newNums.add(tmpNum);
        }
        for (int i = 0; i < newNums.size(); i++) {
            nums[i]=newNums.get(i);
        }
        return newNums.size();
    }

    public static void main(String[] args) {
        Solution26 s=new Solution26();
        System.out.println(s.removeDuplicates(new int[]{1,2,2}));
    }
}
