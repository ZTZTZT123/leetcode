import java.util.*;
public class Solution152 {
    public int maxProduct(int[] nums) {
        int[] dpMax=new int[nums.length];
        int[] dpMin=new int[nums.length];
        dpMax[0]=nums[0];
        dpMin[0]=nums[0];
        Integer result=nums[0];
        for (int i = 1; i < nums.length; i++) {
            int[] sorted=new int[3];
            int tmp=nums[i];
            sorted[0]=dpMax[i-1]*tmp;
            sorted[1]=tmp;
            sorted[2]=dpMin[i-1]*tmp;
            Arrays.sort(sorted);
            dpMax[i]=sorted[2];
            dpMin[i]=sorted[0];
            if(result<sorted[2]){
                result=sorted[2];
            }
        }
        return result;
    }

    public static void main(String[] args) {
        Solution152 s=new Solution152();
        System.out.println(s.maxProduct(new int[]{-2,0}));
    }
}
