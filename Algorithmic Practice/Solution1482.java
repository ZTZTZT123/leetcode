import java.util.*;

public class Solution1482 {
    public boolean isSuccess(int[] bloomDay,int days,int m,int k){
        int sum=0,flowers=0;
        for(int i=0;i<bloomDay.length&&sum<m;i++){
            if(bloomDay[i]<=days){
                flowers++;
                if(flowers==k){
                    sum++;
                    flowers=0;
                }
            }else{
                flowers=0;
            }
        }
        return sum>=m;
    }

    public int minDays(int[] bloomDay, int m, int k) {
        int low=0;
        int high=Arrays.stream(bloomDay).max().getAsInt();
        while(low<high){
            int mid=(low+high)/2;
            if(isSuccess(bloomDay,mid,m,k)){
                high=mid;
            }
            else{
                low=mid+1;
            }
        }
        if(isSuccess(bloomDay,(low+high)/2,m,k)){
            return (low+high)/2;
        }
        return -1;
    }


    public static void main(String[] args) {
        Solution1482 s=new Solution1482();
        //System.out.println(s.minDays(new int[]{1,10,2,9,3,8,4,7,5,6}, 4,  2));

        System.out.println(s.minDays(new int[]{30,49,11,66,54,22,2,57,35},
        3,
        3));
    }
}
