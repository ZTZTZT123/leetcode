class Solution492 {
    public static int[] constructRectangle(int area) {
        for (int i = (int)Math.pow(area, 0.5); ; i--) {
            if(area % i == 0) return new int[]{area / i, i};
        }
    }

    public static void main(String[] args) {
        System.out.println((int)(2.0));
        System.out.println(constructRectangle(4));
    }
}
