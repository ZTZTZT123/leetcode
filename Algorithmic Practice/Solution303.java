public class Solution303 {
    public int[] arr;
    public Solution303(int[] nums) {
        arr=new int[nums.length];
        int befSum=0;
        for (int i = 0; i < nums.length; i++) {
            arr[i]=nums[i]+befSum;
            befSum+=nums[i];
        }
    }

    public int sumRange(int i, int j) {
        int beg=i==0? 0:arr[i-1];
        int end=arr[j];
        return end-beg;
    }
}
