import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Solution526 {
    int n;
    int res;

    boolean doJudge(int a, int b){
        if(a % b == 0 || b % a == 0){
            return true;
        }
        return false;
    }

    private void travel(int size, int[] occur){
        if(size == n){
            res++;
        }
        for (int i = 1; i <= n; i++) {
            if(occur[i] == 1){
                continue;
            }
            if(doJudge(i, size+1)){
                occur[i] = 1;
                travel(size+1, occur);
                occur[i] = 0;
            }
        }
    }

    public int countArrangement(int n) {
        this.n = n;
        int[] occur = new int[n+1];
        travel(0, occur);
        return res;
    }

    public static void main(String[] args) {
        Solution526 s =new Solution526();
        System.out.println(s.countArrangement(2));
    }
}
