public class Solution633 {
    public boolean judgeSquareSum(int c) {
        int a=0;
        int b=(int)Math.pow(c,0.5);
        while(a<=b){
            double aRes=Math.pow(a,2);
            double bRes=Math.pow(b,2);
            double sum=aRes+bRes;
            if(sum==c){
                return true;
            }
            else if(sum<c){
                a++;
            }
            else{
                b--;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        Solution633 s=new Solution633();
        System.out.println(s.judgeSquareSum(2));
    }
}
