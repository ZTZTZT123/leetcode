import java.util.ArrayList;
import java.util.List;

public class Solution1208 {
    public int equalSubstring(String s, String t, int maxCost) {
        int result=0;
        int[] newArr=new int[s.length()];
        for (int i = 0; i < s.length(); i++) {
            newArr[i]=Math.abs(s.charAt(i)-t.charAt(i));
        }
        int left=0;
        int right=0;
        int tmpCost=0;
        while(right< newArr.length){
            tmpCost+= newArr[right];
            while(tmpCost>maxCost){
                tmpCost-= newArr[left];
                left++;
            }
            if(result<right-left+1){
                result=right-left+1;
            }
            right++;
        }
        return result;
    }

    public static void main(String[] args) {
        Solution1208 s=new Solution1208();
        System.out.println(s.equalSubstring("abcd","cdef",
                1));

    }
}
