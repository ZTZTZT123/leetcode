public class Solution941 {
    public boolean validMountainArray(int[] arr) {
        if(arr.length < 3){return false;}
        int last = arr[0];
        boolean isCrossTop = false;
        for (int i = 1; i < arr.length; i++) {
            int tmp = arr[i];
            if(last == tmp){return false;}
            if(!isCrossTop){
                if(last > tmp) {
                    if (i == 1) {return false;}
                    isCrossTop = true;
                }
            }
            else {
                if(last < tmp){return false;}
            }
            last = tmp;
        }
        return isCrossTop;
    }

    public static void main(String[] args) {
        Solution941 s = new Solution941();
        System.out.println(s.validMountainArray(new int[]{0,1,2,4,2,1}));
    }
}
