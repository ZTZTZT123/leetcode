import java.util.List;
import java.util.Map;
import java.util.Stack;

public class Solution1190 {

    public String processBrackets(String str, int Ind, Stack<Integer> locStack){
        StringBuilder result=new StringBuilder();
        for (int i = Ind; i < str.length(); i++) {
            char tmpChar=str.charAt(i);
            if(tmpChar=='('){
                String bkRes=processBrackets(str,i+1,locStack);
                result.append(bkRes);
                i=locStack.peek();
            }
            else if(tmpChar==')'){
                locStack.push(i);
                return result.reverse().toString();
            }
            else{
                result.append(tmpChar);
            }
        }
        return "";
    }

    public String reverseParentheses(String s) {
        StringBuilder res=new StringBuilder();
        Stack<Integer> locStack=new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            char tmpChar=s.charAt(i);
            if(tmpChar=='('){
                String bkRes=processBrackets(s,i+1,locStack);
                res.append(bkRes);
                i=locStack.peek();
            }
            else{
                res.append(tmpChar);
            }
        }
        return res.toString();
    }

    public static void main(String[] args) {

    }
}
