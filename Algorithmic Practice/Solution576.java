import java.util.*;

interface Cache{
    Object get(Object args);
    void put(Object args,Object res);
}


class LRUCache implements Cache{
    private Map<Object,Object> resMark;
    private int cacheSize;

    LRUCache(int cSize){
        resMark=new LinkedHashMap<>(cSize);
        cacheSize=cSize;
    }

    @Override
    public Object get(Object args){
        Object res = resMark.get(args);
        if(res!=null){
            resMark.remove(args);
            resMark.put(args,res);
        }
        return res;
    }

    @Override
    public void put(Object args,Object res){
        while(resMark.size()>=cacheSize){
            resMark.remove(resMark.keySet().iterator().next());
        }
        resMark.put(args, res);
    }
}

interface Function {
    Object run(List<Object> args,CacheFuncUtil util);
}


class CacheFuncUtil {

    private Cache cache;

    private Function runFunc;

    CacheFuncUtil(Cache usedCache,Function function){
        cache=usedCache;
        runFunc=function;
    }

    Object runCacheFunc(List<Object> args){
        Object res=cache.get(args);
        if(res==null){
            res=runFunc.run(args,this);
            cache.put(args,res);
        }
        return res;
    }
}


class Travel implements Function{
    static int[] directX = new int[]{0,1,0,-1};
    static int[] directY = new int[]{1,0,-1,0};

    int m;
    int n;
    int mod;

    @Override
    public Object run(List<Object> args, CacheFuncUtil util) {
        Integer tmpRow = (Integer) args.get(0);
        Integer tmpCol = (Integer) args.get(1);
        Integer move = (Integer) args.get(2);
        if(tmpRow>=m || tmpRow<0 || tmpCol>=n || tmpCol<0){
            return 1;
        }
        if(move == 0){
            return 0;
        }
        int res = 0;
        for (int i = 0; i < 4; i++) {
            res=(res+(Integer) util.runCacheFunc(Arrays.asList(tmpRow+directY[i], tmpCol+directX[i], move-1)))%mod;
        }
        return res;
    }
}


public class Solution576 {
    public int findPaths(int m, int n, int maxMove, int startRow, int startColumn) {
        Travel travel = new Travel();
        travel.m = m;
        travel.n = n;
        travel.mod = (int) 1e9+7;
        CacheFuncUtil funcUtil = new CacheFuncUtil(new LRUCache(10000), travel);
        return (Integer) funcUtil.runCacheFunc(Arrays.asList(startRow, startColumn, maxMove));
    }

    public static void main(String[] args) {
        Solution576 s = new Solution576();
        System.out.println(s.findPaths(8,50,23,5,26));
    }
}
