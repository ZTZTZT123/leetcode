import java.util.HashMap;
import java.util.Map;

public class Solution1347 {
    public int[] getMark(String s){
        int[] mark=new int[26];
        for (int i = 0; i < s.length(); i++) {
            int tmp=s.charAt(i)-'a';
            mark[tmp]++;
        }
        return mark;
    }

    public int minSteps(String s, String t) {
        int[] markS=getMark(s);
        int[] markT=getMark(t);
        for (int i = 0; i < markS.length; i++) {
            markS[i]=Math.max(markS[i]-markT[i],0);
        }
        int result=0;
        for (int i = 0; i < markS.length; i++) {
            result+=markS[i];
        }
        return result;
    }
}
