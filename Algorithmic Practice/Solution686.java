import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Solution686 {

    public int repeatedStringMatch(String a, String b) {
        int aLen = a.length(), bLen = b.length();
        List<Integer> starts = new ArrayList<>();
        char bStart = b.charAt(0);
        for (int i = 0; i < aLen; i++) {
            if(a.charAt(i) == bStart)
                starts.add(i);
        }

        for (Integer start : starts) {
            boolean isSuccess = true, isIn = false;
            int turnCount = 0;
            for(int bInd = 0, aInd = start; bInd < bLen; bInd++){
                isIn = true;
                if(a.charAt(aInd) != b.charAt(bInd)){
                    isSuccess = false;
                    break;
                }
                if(++aInd == aLen){
                    turnCount++;
                    aInd = 0;
                    isIn = false;
                }
            }
            if(isSuccess){
                return turnCount + (isIn? 1:0);
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        Solution686 s = new Solution686();
        System.out.println(s.repeatedStringMatch("aaac",
                "aac"));
    }
}
