public class Solution48 {
    public void print(int[][] matrix){
        for(int i=0;i<matrix.length;i++){
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(matrix[i][j]+",");
            }
            System.out.println();
        }
    }
    public void rotate(int[][] matrix) {
        int[][] remote_result=new int[matrix[0].length][matrix.length];
        for (int i = 0; i < matrix.length; i++) {
            int col_index = matrix.length - 1 - i;
            for (int j = 0; j < matrix[0].length; j++) {
                remote_result[j][col_index] = matrix[i][j];
            }
        }
        for(int i=0;i<matrix.length;i++){
            for (int j = 0; j < matrix[0].length; j++) {
                matrix[i][j]=remote_result[i][j];
            }
        }
        print(matrix);
    }

    public static void main(String[] args) {
        Solution48 s= new Solution48();
        s.rotate(new int[][]{{1, 2, 3},
                    {4, 5, 6},
                    {7, 8, 9}});
    }
}
