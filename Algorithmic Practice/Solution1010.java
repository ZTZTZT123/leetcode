import java.util.Arrays;

public class Solution1010 {

    static final int maxLen = 60;


    public int combination(int n, int k) {
        long result = 1;
        for(int i = 1; i <= k; i++) {
            result = result * (n - i + 1) / i;
        }
        return (int)result;
    }

    public int numPairsDivisibleBy60(int[] time) {
        int res = 0;

        int[] stateArr = new int[maxLen];

        Arrays.stream(time).forEach(len -> stateArr[len % maxLen]++);

        res += stateArr[0] >= 2 ? combination(stateArr[0], 2) : 0;
        res += stateArr[30] >= 2 ? combination(stateArr[30], 2) : 0;

        for (int i = 1, j = 59; i < j; i++, j--) {
            res += stateArr[i] * stateArr[j];
        }

        return res;
    }


    public static void main(String[] args) {
        Solution1010 s = new Solution1010();
        System.out.println(s.numPairsDivisibleBy60(new int[]
                {60,60,60}));
    }
}
