import java.util.HashMap;

public class Solution839 {
    private class UnionUtil{
        HashMap<Integer,Integer> Util;
        int Count;
        UnionUtil(){
            Util=new HashMap<>();
            Count=0;
        }
        int getCount(){
            return Count;
        }
        int find(int element){
            if(Util.containsKey(element)==false){
                Util.put(element,element);
                Count++;
            }
            if(Util.get(element)!=element){
                Util.put(element,find(Util.get(element)));
            }
            return Util.get(element);
        }
        boolean union(int x,int y){
            int rootx=find(x);
            int rooty=find(y);
            if(rootx==rooty){
                return false;
            }
            Util.put(rootx,rooty);
            Count--;
            return true;
        }
    }
    
    public boolean checkSimilar(String str1,String str2){
        int count=0;
        for (int i = 0; i < str1.length(); i++) {
            if(str1.charAt(i)!=str2.charAt(i)){
                count++;
            }
        }
        if(count==0||count==2){
            return true;
        }
        return false;
    }

    public int numSimilarGroups(String[] strs) {
        UnionUtil util=new UnionUtil();
        for(int i=0;i< strs.length;++i){
            util.find(i);
        }
        for (int i = 0; i < strs.length; i++) {
            for (int j = i+1; j < strs.length; j++) {
                if(checkSimilar(strs[i],strs[j])){
                    util.union(i,j);
                }
            }
        }
        return util.Count;
    }

}
