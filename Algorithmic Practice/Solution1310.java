public class Solution1310 {
    public int[] xorQueries(int[] arr, int[][] queries) {
        int[] result=new int[queries.length];
        int[] prefix=new int[arr.length+1];
        for (int i = 1; i < arr.length+1; i++) {
            prefix[i]=prefix[i-1]^arr[i-1];
        }
        for (int i = 0; i < queries.length; i++) {
            int[] tmpQuery=queries[i];
            int start=tmpQuery[0];
            int end=tmpQuery[1];
            int tmpResult=prefix[start]^prefix[end+1];
            result[i]=tmpResult;
        }
        return result;
    }
}
