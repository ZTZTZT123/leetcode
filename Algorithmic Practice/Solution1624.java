import java.util.HashMap;
import java.util.Map;

public class Solution1624 {
    public int maxLengthBetweenEqualCharacters(String s) {
        int maxLength = -1;
        Integer[] firstLocMap = new Integer[26];
        for(int i = 0; i < s.length(); i++){
            char c = s.charAt(i);
            Integer firstLoc = firstLocMap[c - 'a'];
            if(firstLoc != null){
                maxLength = Math.max(maxLength, i - firstLoc - 1);
            }
            else{
                firstLocMap[c - 'a'] = i;
            }
        }
        return maxLength;
    }
}
