import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class TimeMap {
    /** Initialize your data structure here. */
    private Map<String, Map<Integer,String>> mapper;

    public TimeMap() {
        mapper=new HashMap<>();
    }

    public void set(String key, String value, int timestamp) {
        Map<Integer,String> tmpMap = mapper.getOrDefault(key,new TreeMap<>());
        tmpMap.put(timestamp,value);
        mapper.put(key,tmpMap);
    }

    public String get(String key, int timestamp) {
        TreeMap<Integer,String> tmpMap = (TreeMap<Integer, String>) mapper.getOrDefault(key,new TreeMap<>());
        Map.Entry et=tmpMap.floorEntry(timestamp);
        return et==null? "":(String)et.getValue();
    }
}
