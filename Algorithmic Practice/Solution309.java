import java.util.Arrays;

public class Solution309 {
    public int maxProfit(int[] prices) {
        if(prices.length == 0){ return 0; }
        int[] lastDp = new int[3]; //0：持有股票、 1：不持有在冰冻期、 2：不持有不在冰冻期
        lastDp[0] = -prices[0];
        for (int i = 1; i < prices.length; i++) {
            int tmpPrice = prices[i];
            int[] tmpDp = new int[3];
            tmpDp[0] = Math.max(lastDp[0], lastDp[2] - tmpPrice);
            tmpDp[1] = lastDp[0] + tmpPrice;
            tmpDp[2] = Math.max(lastDp[1], lastDp[2]);
            lastDp = tmpDp;
        }
        return Math.max(lastDp[0], Math.max(lastDp[1], lastDp[2]));
    }
}
