import java.util.HashMap;
import java.util.Map;

public class Solution523 {
    public int[] getPrefix(int[] nums){
        int[] prefix=new int[nums.length];
        int sum=0;
        for (int i = 0; i < nums.length; i++) {
            sum+=nums[i];
            prefix[i]=sum;
        }
        return prefix;
    }


    public boolean checkSubarraySum(int[] nums, int k) {
        int[] prefix=getPrefix(nums);
        Map<Integer,Integer> remainders=new HashMap<>();
        remainders.put(0,-1);
        for (int i = 0; i < nums.length; i++) {
            int tmpSum=prefix[i];
            int tmpRemainder=tmpSum%k;
            int ndLoc=remainders.getOrDefault(tmpRemainder,i);
            if(i-ndLoc>=2){
                return true;
            }
            if(ndLoc==i) {
                remainders.put(tmpRemainder, i);
            }
        }
        return false;
    }

    public static void main(String[] args) {
        Solution523 s=new Solution523();
        System.out.println(s.checkSubarraySum(new int[]{5,0,0,0},
        3));
    }
}
