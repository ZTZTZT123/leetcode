import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Interview1710 {
    class NumMap{
        private int[] Mapper;

        private int offset;

        NumMap(int[] nums){
            this.Mapper=getMapper(nums);
        }

        int getVal(int loc){
            return Mapper[loc-this.offset];
        }

        void setVal(int loc, int val){
            Mapper[loc-this.offset]=val;
        }

        int[] getMapper(int[] nums){
            int min=Integer.MAX_VALUE, max=Integer.MIN_VALUE;
            for (int num:nums) {
                min=Math.min(min,num);
                max=Math.max(max,num);
            }
            this.offset=min;
            return new int[max-this.offset+1];
        }
    }

    public int majorityElement(int[] nums) {
        Map<Integer,Integer> count = new HashMap<>(nums.length);
        int maxCount = 0;
        int maxVal = 0;
        for (int num:nums) {
            int tmpNewCount = count.getOrDefault(num,0)+1;
            if(maxCount<tmpNewCount){
                maxCount = tmpNewCount;
                maxVal = num;
            }
            count.put(num,tmpNewCount);
        }
        return maxCount>(nums.length/2) ? maxVal:-1;
    }

    public static void main(String[] args) {
        Interview1710 s = new Interview1710();
        System.out.println(s.majorityElement(new int[]{3,2,3}));
    }

}
