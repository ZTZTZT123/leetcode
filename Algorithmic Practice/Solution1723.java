import java.util.Arrays;

public class Solution1723 {
    Integer result=Integer.MAX_VALUE;

    public void travel(int[] mark,int[] job,int tmpInd,int used,int tmpRes){
        if(tmpRes>=result){
            return;
        }
        if(tmpInd==job.length){
            result=tmpRes;
            return;
        }
        int tmpMission=job[tmpInd];
        if(used<mark.length){
            mark[used]+=tmpMission;
            travel(mark,job,tmpInd+1,used+1,Math.max(mark[used],tmpRes));
            mark[used]-=tmpMission;
        }
        for (int i = 0; i < used; i++) {
            mark[i]+=tmpMission;
            travel(mark,job,tmpInd+1,used,Math.max(mark[i],tmpRes));
            mark[i]-=tmpMission;
        }
    }


    public int minimumTimeRequired(int[] jobs, int k) {
        int[] mark=new int[k];
        travel(mark,jobs,0,0,0);
        return result;
    }
}
