import java.util.*;

public class Solution451 {
    public List<Map.Entry<Character,Integer>> getSortEntryList(Map<Character,Integer> mark){
        List<Map.Entry<Character, Integer>> list = new ArrayList<Map.Entry<Character, Integer>>(mark.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<Character, Integer>>() {
            @Override
            public int compare(Map.Entry<Character, Integer> o1, Map.Entry<Character, Integer> o2) {
                int compare = (o1.getValue()).compareTo(o2.getValue());
                return -compare;
            }
        });
        return list;
    }

    public String frequencySort(String s) {
        Map<Character,Integer> mark=new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            char tmpChar=s.charAt(i);
            mark.put(tmpChar,mark.getOrDefault(tmpChar,0)+1);
        }
        var sortList=getSortEntryList(mark);
        StringBuilder sb=new StringBuilder();
        for (Map.Entry<Character,Integer> et:sortList) {
            int tmpCount=et.getValue();
            for (int i = 0; i < tmpCount; i++) {
                char tmp=et.getKey();
                sb.append(tmp);
            }
        }
        return sb.toString();
    }
}
