public class Solution1603 {
    int[] marker;
    public Solution1603(int big, int medium, int small) {
        marker=new int[]{0,big,medium,small};
    }

    public boolean addCar(int carType) {
        if(marker[carType]>0){
            marker[carType]--;
            return true;
        }
        else{
            return false;
        }
    }
}
