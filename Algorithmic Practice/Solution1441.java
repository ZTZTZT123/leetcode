import java.util.ArrayList;
import java.util.List;

public class Solution1441 {

    final static String pushStr = "Push";

    final static String popStr = "Pop";

    private void appendRemoveOp(List<String> opList){
        opList.add(pushStr);
        opList.add(popStr);
    }

    public List<String> buildArray(int[] target, int n) {
        List<String> opList = new ArrayList<>();
        if(target.length == 0){
            return opList;
        }
        for(int i = 1; i < target[0]; i++) {
            appendRemoveOp(opList);
        }
        int lastNum = target[0] - 1;
        for (int tmpNum : target) {
            for(int i = 0; i < tmpNum - lastNum - 1; i++) {
              appendRemoveOp(opList);
            }
            opList.add(pushStr);
            lastNum = tmpNum;
        }
        return opList;
    }

    public static void main(String[] args){
        Solution1441 s = new Solution1441();
        s.buildArray(new int[]{1,3},3);
    }
}
