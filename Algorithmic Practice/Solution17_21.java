import java.util.*;

public class Solution17_21 {
    public void putWaterLoc(List<Integer> waterLoc,int start,int end){
        for (int i = start; i < end; i++) {
            waterLoc.add(i);
        }
    }

    public void addWater(List<Integer> waterLoc,int[] height){
        for (Integer loc:waterLoc) {
            height[loc]++;
        }
    }

    public void printArr(int[] height){
        for (int h:height) {
            System.out.print(h+",");
        }
        System.out.println();
    }

    public int trap(int[] height) {
        if(height.length==0){
            return 0;
        }
        int maxHeight=Arrays.stream(height).max().getAsInt();
        int result=0;
        for (int i = 0; i < maxHeight; i++) {
            Integer start=null;
            int lastHeight=0;
            for (int j = 0; j < height.length; j++) {
                int tmpHeight=height[j];
                if(tmpHeight<=i){
                    if(start==null&&lastHeight>i){
                        start=j;
                    }
                }
                else if(tmpHeight>i){
                    if(start!=null){
                        result+=(j-start);
                        start=null;
                    }
                }
                lastHeight=tmpHeight;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        Solution17_21 s=new Solution17_21();
        System.out.println(s.trap(new int[]{0,1,0,2,1,0,1,3,2,1,2,1}));
    }
}
