public class Solution238 {
    public int[] productExceptSelf(int[] nums) {
        int len = nums.length;
        int[] prefixArr = new int[len], suffixArr = new int[len];
        for (int i = 0, tmpProduct = 1; i < len; tmpProduct *= nums[i++]) {
            prefixArr[i] = tmpProduct;
        }
        for (int i = len - 1, tmpProduct = 1; i >= 0; tmpProduct *= nums[i--]) {
            suffixArr[i] = tmpProduct;
        }

        int[] res = new int[len];
        for (int i = 0; i < len; i++) {
            res[i] = prefixArr[i] * suffixArr[i];
        }
        return res;
    }
}
