import java.util.*;

public class Solution90 {
    public void getSubSet(Set<List<Integer>> result,List<Integer> tmpPath,int tmpInd,int[] nums){
        result.add(new ArrayList<>(tmpPath));
        for (int i = tmpInd; i < nums.length; i++) {
            tmpPath.add(nums[i]);
            getSubSet(result,tmpPath,i+1,nums);
            tmpPath.remove(tmpPath.size()-1);
        }
    }

    public List<List<Integer>> subsetsWithDup(int[] nums) {
        Arrays.sort(nums);
        Set<List<Integer>> result=new HashSet<>();
        getSubSet(result,new ArrayList<>(),0,nums);
        return new ArrayList<>(result);
    }
}
