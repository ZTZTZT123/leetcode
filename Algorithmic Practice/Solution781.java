import java.util.HashMap;

public class Solution781 {


    public int numRabbits(int[] answers) {
        int[] mark=new int[1000];
        for (int i:answers) {
            mark[i]++;
        }
        int result=0;
        result+=mark[0];
        for (int i = 1; i < mark.length; i++) {
            int count=mark[i];
            int unitSize=i+1;
            int unitCount=count/unitSize+(count%unitSize==0? 0:1);
            result+=unitCount*unitSize;
        }
        return result;
    }
}
