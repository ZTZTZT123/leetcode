import java.util.Stack;

public class Solution946 {
    public boolean validateStackSequences(int[] pushed, int[] popped) {
        Stack<Integer> mark=new Stack<>();
        int index=0;
        for (int i = 0; i < popped.length; i++) {
            int tmp=popped[i];
            while(mark.empty()||tmp!= mark.peek()){
                if(index== pushed.length){
                    return false;
                }
                mark.push(pushed[index++]);
            }
            mark.pop();
        }
        return mark.empty();
    }
}
