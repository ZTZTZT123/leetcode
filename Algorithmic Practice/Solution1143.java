public class Solution1143 {
    public int longestCommonSubsequence(String text1, String text2) {
        int[][] mark=new int[text1.length()+1][text2.length()+1];
        for (int i = 1; i < text1.length()+1; i++) {
            char chT1=text1.charAt(i-1);
            for (int j = 1; j < text2.length()+1; j++) {
                char chT2=text2.charAt(j-1);
                if(chT1==chT2){
                    mark[i][j]=mark[i-1][j-1]+1;
                }
                else{
                    mark[i][j]=Math.max(mark[i-1][j],mark[i][j-1]);
                }
            }
        }
        return mark[text1.length()][text2.length()];
    }

    public static void main(String[] args) {
        Solution1143 s=new Solution1143();
        System.out.println(s.longestCommonSubsequence("abcde","ace"));
    }
}
