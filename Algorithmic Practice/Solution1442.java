public class Solution1442 {
    public int getMid(int[] prefix,int start, int end){
        return (end-1<0? 0:prefix[end-1])^(start-1<0? 0:prefix[start-1]);
    }

    public int countTriplets(int[] arr) {
        int[] prefix=new int[arr.length];
        prefix[0]=arr[0];
        for (int i = 1; i < arr.length; i++) {
            prefix[i]=prefix[i-1]^arr[i];
        }
        int result=0;
        for (int i = 0; i < arr.length-1; i++) {
            for (int j = i+1; j < arr.length; j++) {
                for (int k = j; k < arr.length; k++) {
                    if(getMid(prefix,i,j)==getMid(prefix,j,k+1)){
                        result++;
                    }
                }
            }
        }
        return result;
    }
}
