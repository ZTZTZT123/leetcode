import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Solution1078 {
    public String[] findOcurrences(String text, String first, String second) {
        List<String> res = new ArrayList<>();
        String[] arr = text.split(" ");
        String firstStr = arr[0], secondStr = arr[1];
        for (int i = 2; i < arr.length; i++) {
            String tmpStr = arr[i];
            if(firstStr.equals(first) && secondStr.equals(second)){
                res.add(tmpStr);
            }
            firstStr = secondStr;
            secondStr = tmpStr;
        }
        String[] resArr = new String[res.size()];
        for (int i = 0; i < res.size(); i++) {
            resArr[i] = res.get(i);
        }
        return resArr;
    }

    public static void main(String[] args) {
        Solution1078 s = new Solution1078();
        s.findOcurrences("alice is a good girl she is a good student",
                "a",
                "good");
    }
}
