public class Solution1480 {
    public int[] runningSum(int[] nums) {
        int tmpNum = 0;
        int[] res = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            tmpNum += nums[i];
            res[i] = tmpNum;
        }
        return res;
    }
}
