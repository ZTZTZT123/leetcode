public class Solution434 {
    public int countSegments(String s) {
        if(s.length() == 0){
            return 0;
        }
        int res = 0;
        char lastC = ' ';
        for (int i = 0; i < s.length(); i++) {
            char tmpC = s.charAt(i);
            if(tmpC != ' ' && lastC == ' '){
                res++;
            }
            lastC = tmpC;
        }
        return res;
    }
}
