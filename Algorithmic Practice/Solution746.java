import java.util.ArrayList;
import java.util.HashMap;

public class Solution746 {
    public int minCostClimbingStairs(int[] cost) {
        int[] dp=new int[cost.length+1];
        dp[0]=0;
        dp[1]=0;
        for (int i = 2; i < dp.length; i++) {
            dp[i]=Math.min(dp[i-2]+cost[i-2],dp[i-1]+cost[i-1]);
        }
        return dp[dp.length-1];
    }

    public static void main(String[] args) {
        Solution746 s=new Solution746();
        System.out.println(s.minCostClimbingStairs(new int[]{1, 100, 1, 1, 1, 100, 1, 1, 100, 1}));
    }
}
