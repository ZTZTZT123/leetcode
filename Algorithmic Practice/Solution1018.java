import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Solution1018 {
    public int process(int tmp){
        return tmp%10;
    }
    public List<Boolean> prefixesDivBy5(int[] A) {
        int last=0;
        List<Boolean> resList=new ArrayList<>();
        for (int tmpInt:A) {
            last=process(2*last+tmpInt);
            if(last%5==0){
                resList.add(true);
                continue;
            }
            resList.add(false);
        }
        return resList;
    }

    public static void main(String[] args) {
        Solution1018 s=new Solution1018();
        System.out.println(s.prefixesDivBy5(new int[]{1,0,0,1,0,1,0,0,1,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,1,0,1,0,0,0,0,1,1,0,1,0,0,0,1}));
    }
}
