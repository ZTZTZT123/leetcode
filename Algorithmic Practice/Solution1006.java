public class Solution1006 {
    public String removeSpace(String s){
        StringBuilder result=new StringBuilder("");
        for (int i = 0; i < s.length(); i++) {
            char tmp=s.charAt(i);
            if(tmp!=' '){
                result.append(tmp);
            }
        }
        return result.toString();
    }

    public int parseOperands(String s){
        s=s.concat("*");
        int beg=0;
        char symbol='*';
        int result=1;
        for (int i = 0; i < s.length(); i++) {
            char tmp=s.charAt(i);
            if(tmp=='*'||tmp=='/'){
                String strNum=s.substring(beg,i);
                int tmpNum=Integer.parseInt(strNum);
                result=symbol=='*'? result*tmpNum:result/tmpNum;
                symbol=tmp;
                beg=i+1;
            }
        }
        return result;
    }

    public int doCalculate(String s){
        s=s.concat("+");
        int beg=0;
        char symbol='+';
        int result=0;
        for (int i = 0; i < s.length(); i++) {
            char tmp=s.charAt(i);
            if(tmp=='-'||tmp=='+'){
                if(i==0||(s.charAt(i-1)=='+'||s.charAt(i-1)=='-')){
                    continue;
                }
                if(symbol=='+'){
                    result+=parseOperands(s.substring(beg,i));
                }
                else{
                    result-=parseOperands(s.substring(beg,i));
                }
                symbol=tmp;
                beg=i+1;
            }
        }
        return result;
    }

    public int clumsy(int N) {
        StringBuilder sb=new StringBuilder();
        char[] ops=new char[]{'*','/','+','-'};
        int opInd=0;
        for (int i = N; i >0; i--) {
            sb.append(i);
            if (i != 1) {
                sb.append(ops[opInd++ % 4]);
            }
        }
        return doCalculate(sb.toString());
    }
}
