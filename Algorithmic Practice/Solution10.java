public class Solution10 {
    public boolean if_char_match(char target,char p){
        if(p=='.'||p==target){
            return true;
        }
        return false;
    }
    public boolean isMatch(String s, String p) {
        int index=0;
        for (int i = 0; i < p.length(); i++) {
            if(p.charAt(i)=='*'){
                continue;
            }
            if(i+1<p.length()&&p.charAt(i+1)=='*'){
                while (index<s.length()){
                    if(if_char_match(s.charAt(index),p.charAt(i))) {
                        if (isMatch(s.substring(index), p.substring(i + 1)) == true) {
                            return true;
                        }
                        ++index;
                    }
                    else{
                        break;
                    }
                }
            }
            else{
                if(index>=s.length()||if_char_match(s.charAt(index),p.charAt(i))==false){
                    return false;
                }
                ++index;
            }
        }
        if(index>=s.length()){
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        Solution10 s=new Solution10();
        System.out.println(s.isMatch("mississippi" , "mis*is*p*."));
        System.out.println((55+132+315)%165);
    }
}
