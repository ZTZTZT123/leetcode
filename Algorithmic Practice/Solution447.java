import java.util.*;

public class Solution447 {

    private Integer getDistancePow(int[] point, int[] tmpPoint){
        return (point[0]-tmpPoint[0])*(point[0]-tmpPoint[0]) + (point[1]-tmpPoint[1])*(point[1]-tmpPoint[1]);
    }

    public int numberOfBoomerangs(int[][] points) {
        int res = 0;
        Map<Integer, Integer> store = new HashMap<>();
        for (int i = 0; i < points.length; i++) {
            store.clear();
            int[] point = points[i];
            for (int j = 0; j < points.length; j++) {
                int[] tmpPoint = points[j];
                int distance = getDistancePow(point, tmpPoint);
                store.put(distance, store.getOrDefault(distance, 0)+1);
            }
            for (Map.Entry et:store.entrySet()) {
                int count = (Integer) et.getValue();
                res += count * (count-1);
            }
        }
        return res;
    }

    public static void main(String[] args) {
        Solution447 s = new Solution447();
        System.out.println(s.numberOfBoomerangs(new int[][]
                {{0,0},{1,0},{2,0}}));
    }
}
