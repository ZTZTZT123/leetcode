import java.util.ArrayList;
import java.util.List;

public class Solution1137 {
    public int tribonacci(int n) {
        List<Integer> res = new ArrayList<>(n);
        res.add(0);
        res.add(1);
        res.add(1);
        while(res.size()<n+1){
            int tmpSize = res.size();
            res.add(res.get(tmpSize-3)+res.get(tmpSize-2)+ res.get(tmpSize-1));
        }
        return res.get(n);
    }
}
