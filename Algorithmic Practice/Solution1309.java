public class Solution1309 {

    private int subStrToNum(String s, int start, int end){
        int res = 0;
        for (int i = start; i < end; i++) {
            res = res * 10 + s.charAt(i) - '0';
        }
        return res;
    }

    public String freqAlphabets(String s) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); ) {
            int sharpInd = i + 2;
            if(sharpInd < s.length() && s.charAt(sharpInd) == '#'){
                sb.append((char)('a' - 1 + subStrToNum(s, i, sharpInd)));
                i = sharpInd + 1;
            }
            else{
                sb.append((char)('a' + s.charAt(i) - '1'));
                i++;
            }
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        Solution1309 s = new Solution1309();
        System.out.println(s.freqAlphabets("10#11#12"));
    }
}
