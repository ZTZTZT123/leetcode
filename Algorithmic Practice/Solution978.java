import java.util.Arrays;
import java.util.Collections;

public class Solution978 {
    public int maxTurbulenceSize(int[] arr) {
        int last=0;
        int[] dp=new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            dp[i]=1;
        }
        for (int i = 1; i < arr.length; i++) {
            int tmp=0;
            if(arr[i]>arr[i-1]){
                tmp=1;
            }
            if(arr[i]<arr[i-1]){
                tmp=-1;
            }
            if(tmp==0){
                last=tmp;
                continue;
            }
            else if (last+tmp==0||last==0){
                dp[i]=dp[i-1]+1;
            }
            else{
                dp[i]++;
            }
            last=tmp;
        }
        return Arrays.stream(dp).max().getAsInt();
    }

    public static void main(String[] args) {
        Solution978 s=new Solution978();
        System.out.println(s.maxTurbulenceSize(new int[]{9,4,2,10,7,8,8,1,9}));
    }
}
