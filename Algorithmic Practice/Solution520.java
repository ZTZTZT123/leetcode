public class Solution520 {
    public boolean detectCapitalUse(String word) {
        int upperCount = 0;
        boolean isFirstUpper = false;
        for (int i = 0; i < word.length(); i++) {
            char tmpC = word.charAt(i);
            if('A' <= tmpC && tmpC <= 'Z'){
                if(i == 0){
                    isFirstUpper = true;
                }
                upperCount++;
            }
        }
        if(upperCount == word.length() || (upperCount == 1 && isFirstUpper) || upperCount == 0){
            return true;
        }
        return false;
    }
}
