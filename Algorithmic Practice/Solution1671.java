import java.util.Arrays;

public class Solution1671 {
    public int minimumMountainRemovals(int[] nums) {
        int[] leftDP=new int[nums.length];
        int[] rightDP=new int[nums.length];
        for (int i = 0; i < leftDP.length; i++) {
            leftDP[i]=1;
        }
        for (int i = 0; i < rightDP.length; i++) {
            rightDP[i]=1;
        }
        for (int i = 1; i < leftDP.length; i++) {
            int tmpTop=nums[i];
            for (int j = 0; j < i; j++) {
                int tmpNum=nums[j];
                if(tmpTop>tmpNum){
                    leftDP[i]= Math.max(leftDP[i],leftDP[j]+1 );
                }
            }
        }
        for (int i = nums.length-2; i >=0 ; i--) {
            int tmpTop=nums[i];
            for (int j = nums.length-1; j >i ; j--) {
                int tmpNum=nums[j];
                if(tmpTop>tmpNum){
                    rightDP[i]=Math.max(rightDP[i],rightDP[j]+1 );
                }
            }
        }
        int result=0;
        for (int i = 1; i < nums.length-1; i++) {
            if(leftDP[i]>1&&rightDP[i]>1) {
                int tmpRes = leftDP[i] + rightDP[i] - 1;
                result = Math.max(result, tmpRes);
            }
        }
        return nums.length-result;
    }

    public static void main(String[] args) {
        Solution1671 s=new Solution1671();
        System.out.println(s.minimumMountainRemovals(new int[]{
                100,92,89,77,74,66,64,66,64
        }));
    }
}
