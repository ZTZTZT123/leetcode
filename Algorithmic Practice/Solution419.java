public class Solution419 {
    char[][] board;
    boolean[][] occurArr;
    int m, n;

    private boolean probing(int i, int j){
        return 0 <= i && i < m && 0 <= j && j < n && board[i][j] == 'X' && !occurArr[i][j];
    }

    private void maintainOccurArr(int i, int j){
        boolean isCol = false;
        for (int k = 1; probing(i, j + k); k++) {
            occurArr[i][j + k] = true;
            isCol = true;
        }
        if(isCol){
            return;
        }
        for (int k = 1; probing(i + k, j); k++) {
            occurArr[i + k][j] = true;
        }
    }

    public int countBattleships(char[][] board) {
        this.board = board;
        this.m = board.length; this.n = board[0].length;
        this.occurArr = new boolean[m][n];
        int res = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if(board[i][j] == 'X' && !occurArr[i][j]){
                    maintainOccurArr(i, j);
                    res++;
                }
            }
        }
        return res;
    }
}
