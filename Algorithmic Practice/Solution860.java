import java.util.*;
class Solution860 {
    public interface Do_sth {
        Boolean process(HashMap<Integer, Integer> cash_map);
    }
    public static boolean lemonadeChange(int[] bills) {
        if (bills[0] > 5) {
            return false;
        }
        HashMap<Integer, Integer> cash_map = new HashMap<Integer, Integer>();
        cash_map.put(5, 0);
        cash_map.put(10, 0);
        cash_map.put(20, 0);
        HashMap<Integer, Do_sth> strategy = new HashMap<Integer, Do_sth>();
        strategy.put(5, new Do_sth() {
            @Override
            public Boolean process(HashMap<Integer, Integer> cash_map) {
                cash_map.put(5, cash_map.get(5) + 1);
                return true;
            }
        });
        strategy.put(10, new Do_sth() {
            @Override
            public Boolean process(HashMap<Integer, Integer> cash_map) {
                if(cash_map.get(5)<1){
                    return false;
                }
                cash_map.put(10, cash_map.get(10) + 1);
                cash_map.put(5, cash_map.get(5) -1);
                return true;
            }
        });
        strategy.put(20, new Do_sth() {
            @Override
            public Boolean process(HashMap<Integer, Integer> cash_map) {
                if(cash_map.get(10)>=1&&cash_map.get(5)>=1){
                    cash_map.put(10, cash_map.get(10) - 1);
                    cash_map.put(5, cash_map.get(5) -1);
                    cash_map.put(20, cash_map.get(20) +1);
                    return true;
                }
                if(cash_map.get(5)>=3){
                    cash_map.put(5, cash_map.get(5) -3);
                    cash_map.put(20,cash_map.get(20)+1);
                    return true;
                }
                return false;
            }
        });
        for (int i = 0; i < bills.length; ++i) {
            System.out.println(bills[i]);
            if(!strategy.get(bills[i]).process(cash_map)){
                return false;
            }
            System.out.println(cash_map);
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println(Solution860.lemonadeChange(new int[]{5,5,10,20,5,5,5,5,5,5,5,5,5,10,5,5,20,5,20,5}));
    }
}
