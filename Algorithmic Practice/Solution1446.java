public class Solution1446 {
    public int maxPower(String s) {
        if(s.isEmpty()){return 0;}
        int len = 1, res = 1;
        char lastC = s.charAt(0);
        for (int i = 1; i < s.length(); i++) {
            char tmpC = s.charAt(i);
            len = (tmpC == lastC ? len + 1 : 1);
            res = Math.max(res, len);
            lastC = tmpC;
        }
        return res;
    }
}
