import java.util.Arrays;

/**
 * @FileName Solution2187
 * @Description
 * @Author tong.zhou
 * @date 2024-10-05
 **/
public class Solution2187 {

    public boolean judgeFinish(int[] timeArr, long timeElapsed, int totalTrips){
        long tripCount = 0;
        for (int time : timeArr) {
            if(tripCount >= totalTrips){
                return true;
            }
            tripCount += (timeElapsed / time);
        }
        return tripCount >= totalTrips;
    }


    public long binarySearchMinTime(int[] time, int totalTrips){
        long left = 0, right = (long) totalTrips * Arrays.stream(time).max().orElse(0);
        while(left <= right){
            long mid = left + ((right - left) / 2);
            boolean finish = judgeFinish(time, mid, totalTrips);
            if(finish) {
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }
        return left;
    }



    public long minimumTime(int[] time, int totalTrips) {
        return binarySearchMinTime(time, totalTrips);
    }


    public static void main(String[] args) {
        Solution2187 solution2187 = new Solution2187();
        solution2187.minimumTime(new int[]{5,10,10}, 9);
    }
}
