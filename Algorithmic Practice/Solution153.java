public class Solution153 {
    public int findMin(int[] nums) {
        int left=0;
        int right=nums.length-1;
        while(left<=right){
            int mid=(left+right)/2;
            int tmpNum=nums[mid];
            int rightNum=nums[right];
            if(tmpNum==rightNum){
                return tmpNum;
            }
            else if(tmpNum<rightNum){
                right=mid;
            }
            else{
                left=mid+1;
            }
        }
        return -1;
    }
}
