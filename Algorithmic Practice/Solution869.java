public class Solution869 {

    boolean res;

    private void travel(StringBuilder sb, String strNum, boolean[] isChosen){
        if(res){ return; }
        if(sb.length() == strNum.length()){
            int num = Integer.valueOf(sb.toString());
            if((num&(num-1)) == 0){
                res = true;
            }
            return;
        }
        for (int i = 0; i < strNum.length(); i++) {
            if(isChosen[i]){ continue; }
            char tmpC = strNum.charAt(i);
            if(sb.length() == 0 && tmpC == '0'){ continue; }
            sb.append(tmpC);
            isChosen[i] = true;
            travel(sb, strNum, isChosen);
            sb.deleteCharAt(sb.length() - 1);
            isChosen[i] = false;
        }
    }

    public boolean reorderedPowerOf2(int n) {
        String strNum = String.valueOf(n);
        travel(new StringBuilder(), strNum, new boolean[strNum.length()]);
        return res;
    }
}
