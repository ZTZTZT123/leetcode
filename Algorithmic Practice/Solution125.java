public class Solution125 {
    public static boolean isPalindrome(String s) {
        int left = 0, right = s.length() - 1;
        for( ; ;left++, right--){
            for (;left < right && !Character.isLetterOrDigit(s.charAt(left)); left++);
            for (;left < right && !Character.isLetterOrDigit(s.charAt(right)); right--);
            if(left >= right){
                return true;
            }
            if(Character.toLowerCase(s.charAt(left)) != Character.toLowerCase(s.charAt(right))){
                return false;
            }
        }
    }

    public static void main(String[] args) {
        isPalindrome("a");
    }
}
