import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.TreeSet;

public class Solution1337 {

    class RowInfo implements Comparable{
        int rowNum;
        int soldierNum;
        public RowInfo(int rowNum, int soldierNum) {
            this.rowNum = rowNum;
            this.soldierNum = soldierNum;
        }
        @Override
        public int compareTo(Object o) {
            RowInfo ri = (RowInfo) o;
            int sub = this.soldierNum - ri.soldierNum;

            return sub == 0 ? this.rowNum - ri.rowNum : sub;
        }
    }

    public int[] kWeakestRows(int[][] mat, int k) {
        int[] res = new int[k];
        PriorityQueue<RowInfo> rowInfoSet = new PriorityQueue<>();
        int rowNum = 0;
        for (int[] tmpRow:mat) {
            int soldierNum = 0;
            for (int tmpNum:tmpRow) {
                if(tmpNum == 1){
                    soldierNum++;
                }
                else{
                    break;
                }
            }
            rowInfoSet.add(new RowInfo(rowNum, soldierNum));
            rowNum++;
        }
        for (int i = 0; i < res.length; i++) {
            res[i] = rowInfoSet.poll().rowNum;
        }
        return res;
    }

    public static void main(String[] args) {
        Solution1337 s = new Solution1337();
        System.out.println(s.kWeakestRows(new int[][]{{1,1,1,1,1,1},{1,1,1,1,1,1},{1,1,1,1,1,1}},3));
    }
}
