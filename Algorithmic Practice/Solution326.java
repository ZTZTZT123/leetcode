public class Solution326 {
    public boolean isPowerOfThree(int n) {
        int left = 0, right = 22;
        while(left <= right){
            int mid = left + (right - left) / 2;
            double tmpNum = Math.pow(3, mid);
            if(tmpNum == n){
                return true;
            }
            else if(tmpNum > n){
                right = mid - 1;
            }
            else {
                left = mid + 1;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        Solution326 s = new Solution326();
        System.out.println(s.isPowerOfThree(28));
    }
}
