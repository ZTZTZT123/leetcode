import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class Solution1035 {
    public int maxUncrossedLines(int[] nums1, int[] nums2) {
        int m=nums1.length+1;
        int n=nums2.length+1;
        int[][] dp=new int[m][n];
        for (int i = 1; i < m; i++) {
            int[] tmpRow=dp[i];
            for (int j = 1; j < n; j++) {
                if(nums1[i-1]==nums2[j-1]){
                    tmpRow[j]=dp[i-1][j-1]+1;
                }
                else{
                    tmpRow[j]=Math.max(tmpRow[j-1],dp[i-1][j]);
                }
            }
        }
        return dp[m-1][n-1];
    }
}
