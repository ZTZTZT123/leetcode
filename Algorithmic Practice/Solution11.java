public class Solution11 {
    public int maxArea(int[] height) {
        int maxArea = 0;
        for(int left = 0, right = height.length - 1; left < right; ){
            int leftHeight = height[left], rightHeight = height[right];
            maxArea = Math.max(maxArea, Math.min(leftHeight, rightHeight) * (right - left));
            if(leftHeight < rightHeight){
                while(left < right && height[left] <= leftHeight){
                    left++;
                }
            }
            else{
                while(left < right && height[right] <= rightHeight){
                right--;
                }
            }
        }
        return maxArea;
    }
}
