public class Solution566 {
    public int[][] matrixReshape(int[][] nums, int r, int c) {
        int rowCount= nums.length;
        int colCount=nums[0].length;
        if(rowCount*colCount!=r*c){
            return nums;
        }
        int[][] result=new int[r][c];
        for (int i = 0; i < r*c; i++) {
            int tmpor=i/colCount;
            int tmpoc=i%colCount;
            int tmpnr=i/c;
            int tmpnc=i%c;
            result[tmpnr][tmpnc]=nums[tmpor][tmpoc];
        }
        return result;
    }
}
