public class Solution1189 {

    private int[] gettextInfo(String text){
        int[] textInfo = new int[26];
        for (int i = 0; i < text.length(); i++) {
            textInfo[text.charAt(i) - 'a']++;
        }
        return textInfo;
    }

    private int maxNumberOfStr(String text, String str){
        int[] textInfo = gettextInfo(text);
        int[] strInfo = gettextInfo(str);
        int retVal = Integer.MAX_VALUE;
        for (int i = 0; i < strInfo.length; i++) {
            int tmpCharCount = strInfo[i];
            if(tmpCharCount > 0){
                retVal = Math.min(retVal, textInfo[i] / tmpCharCount);
            }
        }
        return retVal;
    }

    public int maxNumberOfBalloons(String text) {
        return maxNumberOfStr(text, "balloon");
    }
}
