import java.util.*;

public class Solution524 {

    private boolean doJudge(String str, String tarStr){
        int ind = 0;
        for (int i = 0; i < str.length(); i++) {
            char tmpC = str.charAt(i);
            boolean isFound = false;
            while(ind < tarStr.length()){
                if(tarStr.charAt(ind++) == tmpC){
                    isFound = true;
                    break;
                }
            }
            if(!isFound){
                return false;
            }
        }
        return true;
    }

    private int compare(String o1, String o2){
        int sub = o2.length() - o1.length();
        if(sub != 0){
            return sub;
        }
        return o1.compareTo(o2);
    }

    public String findLongestWord(String s, List<String> dictionary) {
        String res = "";
        for (String tmpStr:dictionary) {
            if(doJudge(tmpStr, s)){
                if(compare(res, tmpStr) > 0){
                    res = tmpStr;
                }
            }
        }
        return res;
    }

    public static void main(String[] args) {
        Solution524 s =new Solution524();
        System.out.println(s.findLongestWord("wsmzffsupzgauxwokahurhhikapmqitytvcgrfpavbxbmmzdhnrazartkzrnsmoovmiofmilihynvqlmwcihkfskwozyjlnpkgdkayioieztjswgwckmuqnhbvsfoevdynyejihombjppgdgjbqtlauoapqldkuhfbynopisrjsdelsfspzcknfwuwdcgnaxpevwodoegzeisyrlrfqqavfziermslnlclbaejzqglzjzmuprpksjpqgnohjjrpdlofruutojzfmianxsbzfeuabhgeflyhjnyugcnhteicsvjajludwizklkkosrpvhhrgkzctzwcghpxnbsmkxfydkvfevyewqnzniofhsriadsoxjmsswgpiabcbpdjjuffnbvoiwotrbvylmnryckpnyemzkiofwdnpnbhkapsktrkkkakxetvdpfkdlwqhfjyhvlxgywavtmezbgpobhikrnebmevthlzgajyrmnbougmrirsxi",
                Arrays.asList("nbmxgkduynigvzuyblwjepn","xrecdmubinrzfmkyvrdxxkg")));
    }
}
