import java.util.*;

public class Interview1003 {
    public String strSort(String str){
        char[] charArr = new char[str.length()];
        for (int i = 0; i < str.length(); i++) {
            charArr[i] = str.charAt(i);
        }
        Arrays.sort(charArr);
        return new String(charArr);
    }

    public List<List<String>> groupAnagrams(String[] strs) {
        Map<String,List<String>> counter = new HashMap<>(strs.length);
        for (String str:strs) {
            String sortStr = strSort(str);
            List<String> strArr = counter.getOrDefault(sortStr,new ArrayList<>());
            strArr.add(str);
            counter.put(sortStr,strArr);
        }
        List<List<String>> res = new ArrayList<>(counter.size());
        for (Map.Entry et:counter.entrySet()) {
            res.add((List<String>) et.getValue());
        }
        return res;
    }
}
