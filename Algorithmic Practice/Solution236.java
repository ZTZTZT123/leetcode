

public class Solution236 {

    TreeNode firstCommonFather = null;

    private char getSubTreeContainInfo(TreeNode node, TreeNode p, TreeNode q){
        if(node == null || firstCommonFather != null){
            return 0;
        }
        char res;
        if(node == p){ res = 1; }
        else if(node == q){ res = 2; }
        else{ res = 0; }
        res |= getSubTreeContainInfo(node.left, p, q);
        res |= getSubTreeContainInfo(node.right, p, q);
        if(res == 3 && firstCommonFather == null){
            firstCommonFather = node;
        }
        return res;
    }

    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        getSubTreeContainInfo(root, p, q);
        return firstCommonFather;
    }
}
