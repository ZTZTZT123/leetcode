public class Solution2000 {

    private int getCharPos(String word, char ch){
        for (int i = 0; i < word.length(); i++) {
            if(word.charAt(i) == ch)
                return i;
        }
        return -1;
    }

    public String reversePrefix(String word, char ch) {
        int charPos = getCharPos(word, ch);
        if(charPos == -1){
            return word;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = charPos; i >= 0; i--) {
            sb.append(word.charAt(i));
        }
        for (int i = charPos + 1; i < word.length(); i++) {
            sb.append(word.charAt(i));
        }
        return sb.toString();
    }
}
