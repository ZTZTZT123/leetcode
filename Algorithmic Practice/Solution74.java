public class Solution74 {
    public int getLocNum(int[][] matrix,int loc){
        int m=matrix[0].length;
        int i=loc/m;
        int j=loc%m;
        return matrix[i][j];
    }

    public boolean searchMatrix(int[][] matrix, int target) {
        int n=matrix.length;
        int m=matrix[0].length;
        int count=n*m;
        int left=0;
        int right=count-1;
        while(left<=right){
            int mid=(left+right)/2;
            int tmpNum=getLocNum(matrix,mid);
            if(tmpNum==target){
                return true;
            }
            else if(tmpNum<target){
                left=mid+1;
            }
            else {
                right=mid-1;
            }
            if(!(0<=left&&left<count&&0<=right&&right<count)){
                return false;
            }
        }
        return false;
    }
}
