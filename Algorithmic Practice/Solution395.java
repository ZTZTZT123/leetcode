import java.util.*;

public class Solution395 {
    int result=0;
    public List subString(String s,char c){
        List<Integer> indexs=new ArrayList<>();
        indexs.add(-1);
        for (int i = 0; i < s.length(); i++) {
            if(s.charAt(i)==c){
                indexs.add(i);
            }
        }
        indexs.add(s.length());
        List<String> result=new ArrayList<>();
        for (int i = 0; i < indexs.size()-1; i++) {
            result.add(s.substring(indexs.get(i)+1, indexs.get(i+1)));
        }
        return result;
    }
    public void divide(String s,int k){
        if(s.equals("")){
            return;
        }
        int[] mark=new int[26];
        for (int i = 0; i < s.length(); i++) {
            mark[s.charAt(i)-'a']++;
        }
        char minChar='*';
        int minCount=10000;
        for (int i = 0; i < mark.length; i++) {
            if(mark[i]==0){
                continue;
            }
            if(mark[i]<minCount){
                minCount=mark[i];
                minChar= (char) ('a'+i);
            }
        }
        if(minCount>=k){
            result=Math.max(result,s.length());
        }
        List<String> strList=subString(s,minChar);
        for (int i = 0; i < strList.size(); i++) {
            divide(strList.get(i),k);
        }
    }
    public int longestSubstring(String s, int k) {
        divide(s,k);
        return result;
    }

    public static void main(String[] args) {
        Solution395 s=new Solution395();
        System.out.println(s.longestSubstring("aaabb",
                3));
    }
}
