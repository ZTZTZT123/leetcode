public class Solution168 {

    public String convertToTitle(int columnNumber) {
        StringBuilder res=new StringBuilder();
        int tmpNum=columnNumber;
        while(tmpNum>0){
            tmpNum--;
            int loc=tmpNum%26;
            res.insert(0,(char)( loc+'A'));
            tmpNum=tmpNum/26;
        }
        return res.toString();
    }

    public static void main(String[] args) {
        Solution168 s=new Solution168();
        System.out.println(s.convertToTitle(701));
    }
}
