public class Solution665 {
    public Boolean check(int[] nums){
        for (int i = 0; i < nums.length-1; i++) {
            if(nums[i]>nums[i+1]){
                return false;
            }
        }
        return true;
    }
    public boolean checkPossibility(int[] nums) {
        for (int i = 0; i < nums.length-1; i++) {
            if(nums[i]>nums[i+1]){
                int first=nums[i];
                int last=nums[i+1];
                nums[i]=last;
                if(check(nums)==true){
                    return true;
                }
                nums[i]=first;
                nums[i+1]=first;
                return check(nums);
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Solution665 s=new Solution665();
        System.out.println(s.checkPossibility(new int[]{3,4,2,3}));
    }
}
