public class Solution1764 {
    public boolean canChoose(int[][] groups, int[] nums) {
        int lenSum=0;
        int[] groupLens=new int[groups.length];
        for (int i = groups.length-1; i >=0 ; i--) {
            lenSum+=groups[i].length;
            groupLens[i]=lenSum;
        }
        int index=0;
        out2:for (int i = 0; i < groups.length; i++) {
            int[] tmpGroup=groups[i];
            int leftLen=groupLens[i];
            out1:while(index< nums.length){
                int tmpLeftLen= nums.length-index;
                if(tmpLeftLen<leftLen){
                    return false;
                }
                int it=index;
                for (int j = 0; j < tmpGroup.length; j++) {
                    if(tmpGroup[j]!=nums[it]){
                        index++;
                        continue out1;
                    }
                    it++;
                }
                index=it;
                continue out2;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Solution1764 s=new Solution1764();
        System.out.println(s.canChoose(new int[][]{{1,-1,-1},{3,-2,0}},
        new int[]{1,-1,0,1,-1,-1,3,-2,0}));
    }
}
