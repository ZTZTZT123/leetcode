import java.util.Comparator;
import java.util.PriorityQueue;

public class Solution1738 {
    public int kthLargestValue(int[][] matrix, int k) {
        int m=matrix.length;
        int n=matrix[0].length;
        int[][] dp=new int[m][n];
        PriorityQueue<Integer> maxHeap = new PriorityQueue<Integer>(new Comparator<Integer>(){
            @Override
            public int compare(Integer i1,Integer i2){
                return i2-i1;
            }
        });
        for (int i = 0; i < m; i++) {
            int[] tmpArr=matrix[i];
            int[] tmpDp=dp[i];
            for (int j = 0; j < n; j++) {
                int a=i==0||j==0? 0:dp[i-1][j-1];
                int b=i==0? 0:dp[i-1][j];
                int c=j==0? 0:tmpDp[j-1];
                int tmpNum=tmpArr[j]^a^b^c;
                maxHeap.add(tmpNum);
                tmpDp[j]=tmpNum;
            }
        }
        for (int i = 0; i < k-1; i++) {
            maxHeap.poll();
        }
        return maxHeap.peek();
    }
}
