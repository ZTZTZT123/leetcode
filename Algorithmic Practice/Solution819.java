import java.util.*;

public class Solution819 {
    public String toLower(String str){
        StringBuilder result=new StringBuilder("");
        for (int i = 0; i < str.length(); i++) {
            char tmp=str.charAt(i);
            if(Character.isUpperCase(tmp)){
                tmp=Character.toLowerCase(tmp);
            }
            result.append(tmp);
        }
        return result.toString();
    }

    public Map<String,Integer> processPar(String str){
        str=str.concat(" ");
        HashMap<String,Integer> markCount=new HashMap<>();
        int beg=0;
        for (int i = 0; i < str.length(); i++) {
            char tmp=str.charAt(i);
            if(Character.isLetter(tmp)==false){
                String word=str.substring(beg,i);
                word=toLower(word);
                if(markCount.containsKey(word)){
                    markCount.put(word,markCount.get(word)+1);
                }
                else{
                    markCount.put(word,1);
                }
                beg=i+1;
            }
        }
        return markCount;
    }

    public Set<String> processBan(String[] strArr){
        HashSet<String> markExit=new HashSet<>();
        for (String str:strArr) {
            markExit.add(str);
        }
        return markExit;
    }

    public String mostCommonWord(String paragraph, String[] banned) {
        Map<String,Integer> markCount=processPar(paragraph);
        Set<String> markExit=processBan(banned);
        int maxCount=0;
        String maxWord="";
        for (Map.Entry<String,Integer> et:markCount.entrySet()) {
            if(markExit.contains(et.getKey())){
                continue;
            }
            if(maxWord.equals("")||et.getValue()>maxCount){
                maxCount=et.getValue();
                maxWord=et.getKey();
            }
        }
        return maxWord;
    }
}
