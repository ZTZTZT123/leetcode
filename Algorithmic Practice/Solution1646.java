public class Solution1646 {
    public int getMaximumGenerated(int n) {
        int res = 0;
        int[] target = new int[n+1];
        for (int i = 0; i <= n; i++) {
            if(i < 2){
                target[i] = i;
            }
            if((i & 1) == 0){
                target[i] = target[i>>1];
            }
            else{
                target[i] = target[i>>1] + target[(i>>1)+1];
            }
            res = Math.max(res, target[i]);
        }
        return res;
    }
}
