/**
 * @FileName Solution134
 * @Description
 * @Author tong.zhou
 * @date 2024-10-06
 **/
public class Solution134 {
    public int canCompleteCircuit(int[] gas, int[] cost) {
        int len = gas.length;
        int tmpTryInd = 0;
        while(tmpTryInd < len){
            int step = 0;
            int tmpGasSurplus = 0;
            while(step != len){
                int ind = (tmpTryInd + step) % len;
                int gasCostSub = gas[ind] - cost[ind];
                tmpGasSurplus += gasCostSub;
                if(tmpGasSurplus < 0){
                    break;
                }
                step++;
            }
            if(step == len){
                return tmpTryInd;
            }
            else {
                tmpTryInd = tmpTryInd + step + 1;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        Solution134 s = new Solution134();
        s.canCompleteCircuit(new int[]{2,3,4}, new int[]{3,4,3});
    }
}
