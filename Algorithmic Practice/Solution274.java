import java.util.Arrays;

public class Solution274 {


    public int hIndex(int[] citations) {
        Arrays.sort(citations);
        int n = citations.length;
        int left = 0;
        int right = n-1;
        while(left<right){
            int mid = (left+right)/2;
            int tmpCount = n - mid;
            if(tmpCount>citations[mid]){
                left = mid+1;
            }
            else{
                right = mid;
            }
        }
        return n-left;
    }

    public static void main(String[] args) {
        Solution274 s=new Solution274();
        System.out.println(s.hIndex(new int[]{100}));
    }
}
