import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Solution233 {

    public int countDigitOne(int n) {
        long mulk = 1;
        int ans = 0;
        for (int k = 0; n >= mulk; ++k) {
            ans += (n / (mulk * 10)) * mulk + Math.min(Math.max(n % (mulk * 10) - mulk + 1, 0), mulk);
            mulk *= 10;
        }
        return ans;
    }

    public static void main(String[] args) {
        Solution233 s =new Solution233();
        System.out.println(s);
    }
}
