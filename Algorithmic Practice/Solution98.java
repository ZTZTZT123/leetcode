class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode() {}
    TreeNode(int val) { this.val = val; }
    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}
public class Solution98 {
    boolean result=true;
    Integer tmpSmall=null;
    public void travel(TreeNode tmpNode){
        if(tmpNode==null||result==false){
            return;
        }
        travel(tmpNode.left);
        if(tmpSmall!=null&&tmpSmall>tmpNode.val){
            result=false;
        }
        tmpSmall=tmpNode.val;
        travel(tmpNode.right);
    }

    public boolean isValidBST(TreeNode root) {
        travel(root);
        return result;
    }
}
