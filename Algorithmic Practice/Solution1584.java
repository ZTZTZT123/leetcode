import java.util.*;


public class Solution1584 {

    private class UnionUtil{
        HashMap<Integer,Integer> Util;
        int Count;
        UnionUtil(){
            Util=new HashMap<>();
            Count=0;
        }
        int getCount(){
            return Count;
        }
        int find(int element){
            if(Util.containsKey(element)==false){
                Util.put(element,element);
                Count++;
            }
            if(Util.get(element)!=element){
                Util.put(element,find(Util.get(element)));
            }
            return Util.get(element);
        }
        boolean union(int x,int y){
            int rootx=find(x);
            int rooty=find(y);
            if(rootx==rooty){
                return false;
            }
            Util.put(rootx,rooty);
            Count--;
            return true;
        }
    }



    public int minCostConnectPoints(int[][] points) {
        class edge{
            int start;
            int end;
            int len;

            public edge(int start, int end, int len) {
                this.start = start;
                this.end = end;
                this.len = len;
            }
        }


        List<edge> edgeList= new ArrayList<edge>();
        for (int i = 0; i < points.length; i++) {
            for (int j = i+1; j < points.length; j++) {
                edge tmp=new edge(i,j,Math.abs(points[i][0]-points[j][0])+Math.abs(points[i][1]-points[j][1]));
                edgeList.add(tmp);
            }
        }

        edgeList.sort(new Comparator<edge>() {
            @Override
            public int compare(edge o1, edge o2) {
                return o1.len-o2.len;
            }
        });

        UnionUtil unionUtil=new UnionUtil();

        for (int i = 0; i < points.length; i++) {
            unionUtil.find(i);
        }

        int res=0;
        for (edge tmp:edgeList) {
            if(unionUtil.find(tmp.start)== unionUtil.find(tmp.end)&& 1 == unionUtil.Count){
                return res;
            }
            if(unionUtil.union(tmp.start, tmp.end)) {
                res = res + tmp.len;
            }
        }
        return res;
    }

    public static void main(String[] args) {
        Solution1584 s=new Solution1584();
        System.out.println(s.minCostConnectPoints(new int[][]{{0,0},{2,2},{3,10},{5,2},{7,0}}));

    }
}
