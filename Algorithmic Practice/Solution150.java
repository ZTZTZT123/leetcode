import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class Solution150 {
    public boolean isAllNum(String token) {
        return !("+".equals(token) || "-".equals(token) || "*".equals(token) || "/".equals(token));
    }

    public int evalRPN(String[] tokens) {
        Stack<Integer> calMark=new Stack<>();
        for (int i = 0; i < tokens.length; i++) {
            String tmp=tokens[i];
            if(isAllNum(tmp)){
                calMark.push(Integer.parseInt(tmp));
            }
            else{
                int opNum_R=calMark.pop();
                int opNum_L=calMark.pop();
                if (tmp.equals("+")) {
                    calMark.push(opNum_L+opNum_R);
                } else if (tmp.equals("-")) {
                    calMark.push(opNum_L-opNum_R);
                } else if (tmp.equals("*")) {
                    calMark.push(opNum_L*opNum_R);
                } else {
                    calMark.push(opNum_L/opNum_R);
                }
            }
            System.out.println(calMark.toString());
        }
        return calMark.peek();
    }

    public static void main(String[] args) {
        Solution150 s=new Solution150();
        System.out.println(s.evalRPN(new String[]{"2","1","+","3","*"}));
    }
}
