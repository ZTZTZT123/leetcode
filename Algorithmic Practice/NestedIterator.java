import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

    // This is the interface that allows for creating nested lists.
    // You should not implement it, or speculate about its implementation
interface NestedInteger {

    // @return true if this NestedInteger holds a single integer, rather than a nested list.
    public boolean isInteger();

    // @return the single integer that this NestedInteger holds, if it holds a single integer
    // Return null if this NestedInteger holds a nested list
    public Integer getInteger();

    // @return the nested list that this NestedInteger holds, if it holds a nested list
    // Return null if this NestedInteger holds a single integer
    public List<NestedInteger> getList();
}



public class NestedIterator implements Iterator<Integer> {

    List<Integer> Arr;

    int Ind=0;

    public NestedIterator(List<NestedInteger> nestedList) {
        Arr=new ArrayList<>();
        for (NestedInteger ni:nestedList) {
            travel(ni);
        }
    }

    public void travel(NestedInteger tmp){
        if(tmp.isInteger()){
            Arr.add(tmp.getInteger());
            return;
        }
        for (NestedInteger ni:tmp.getList()) {
            travel(ni);
        }
    }

    @Override
    public Integer next() {
        return Arr.get(Ind++);
    }

    @Override
    public boolean hasNext() {
        return Ind<Arr.size();
    }
}
