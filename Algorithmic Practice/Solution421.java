

public class Solution421 {


    public int findMaximumXOR(int[] nums) {
        int result=0;
        for (int i = 0; i < nums.length; i++) {
            int keepNum=nums[i];
            for (int j = i+1; j < nums.length; j++) {
                result=Math.max(keepNum^nums[j],result);
            }
        }
        return result;
    }

    public static void main(String[] args) {
        Solution421 s=new Solution421();
        System.out.println(s.findMaximumXOR(
                new int[]{3,10,5,25,2,8}));
    }
}
