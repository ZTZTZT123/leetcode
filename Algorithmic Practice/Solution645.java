public class Solution645 {

    public int[] findErrorNums(int[] nums) {
        int[] res=new int[2];
        int[] map=new int[nums.length+1];
        for (int tmpNum:nums) {
            map[tmpNum]++;
        }
        for (int i = 0; i < map.length; i++) {
            int tmpCount=map[i];
            if(tmpCount==0){
                res[1]=i;
            }
            else if(tmpCount==2){
                res[0]=i;
            }
        }
        return res;
    }
}
