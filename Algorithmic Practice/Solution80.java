public class Solution80 {
    public void remove(int[] nums,int index){
        for (int i = index; i < nums.length-1; i++) {
            nums[i]=nums[i+1];
        }
    }

    public int removeDuplicates(int[] nums) {
        Integer tmpNum=null;
        Integer tmpCount=null;
        int removeCount=0;
        int[] newNums=nums.clone();
        for (int i = 0; i < newNums.length; i++) {
            Integer ptr=newNums[i];
            if(tmpNum==null||ptr.equals(tmpNum)==false){
                tmpNum=ptr;
                tmpCount=1;
            }
            else{
                tmpCount++;
                if(tmpCount>2){
                    remove(nums,i-removeCount);
                    removeCount++;
                }
            }
        }
        return nums.length-removeCount;
    }

    public static void main(String[] args) {
        Solution80 s=new Solution80();
        System.out.println(s.removeDuplicates(new int[]{1,1,1,2,2,3}));
    }
}
