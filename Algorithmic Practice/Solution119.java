import java.util.ArrayList;
import java.util.List;

public class Solution119 {
    public List<Integer> getRow(int rowIndex) {
        List<List<Integer>> resList=new ArrayList<>(rowIndex+1);
        for (int i = 0; i <rowIndex+1; i++) {
            resList.add(new ArrayList<>());
        }
        resList.get(0).add(1);
        for (int i = 1; i < rowIndex+1; i++) {
            for (int j = 0; j <= i; j++) {
                int left=0;
                int right=0;
                int lastSize=resList.get(i-1).size();
                if(j!=0){
                    left=resList.get(i-1).get(j-1);
                }
                if(j!=lastSize){
                    right=resList.get(i-1).get(j);
                }
                resList.get(i).add(left+right);
            }
        }
        return resList.get(rowIndex);
    }
}
