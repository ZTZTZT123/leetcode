public class Solution796 {

    private boolean offsetCompare(String s, String goal, int offset){
        for (int i = 0; i < s.length(); i++) {
            if(s.charAt((i+offset)%s.length()) != goal.charAt(i)){
                return false;
            }
        }
        return true;
    }

    public boolean rotateString(String s, String goal) {
        if (s.length() != goal.length()){
            return false;
        }
        for (int i = 1; i < s.length(); i++) {
            if(offsetCompare(s, goal, i)){
                return true;
            }
        }
        return false;
    }
}
