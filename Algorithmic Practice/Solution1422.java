public class Solution1422 {
    public int maxScore(String s) {
        int[] totalCount = new int[2];
        for (int i = 0; i < s.length(); i++) {
            totalCount[s.charAt(i) - '0']++;
        }

        int maxScore = 0;
        int[] tmpCount = new int[2];
        for (int i = 0; i < s.length() - 1; i++) {
            tmpCount[s.charAt(i) - '0']++;
            maxScore = Math.max(maxScore, tmpCount[0] + totalCount[1] - tmpCount[1]);
        }
        return maxScore;
    }
}
