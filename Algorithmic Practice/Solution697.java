import java.util.*;

public class Solution697 {
    public int findShortestSubArray(int[] nums) {
        HashMap<Integer,int[]> mark=new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int tmp=nums[i];
            if(mark.containsKey(tmp)==false){
                mark.put(tmp,new int[]{1,i,i});
            }
            else{
                int[] tmpArr=mark.get(tmp);
                tmpArr[0]++;
                tmpArr[2]=i;
                mark.put(tmp,tmpArr);
            }
        }
        int result=50000;
        int maxCount=0;
        for (Map.Entry<Integer,int[]> et:
             mark.entrySet()) {
            int[] tmpArr= et.getValue();
            if(maxCount<tmpArr[0]){
                maxCount=tmpArr[0];
                result=tmpArr[2]-tmpArr[1]+1;
            }
            else if(maxCount==tmpArr[0]){
                result=Math.min(result,tmpArr[2]-tmpArr[1]+1);
            }
        }
        return result;
    }

    public static void main(String[] args) {
        Solution697 s=new Solution697();
        System.out.println(s.findShortestSubArray(new int[]{1,2,2,3,1,4,2}));
    }
}
