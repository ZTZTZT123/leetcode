import com.sun.source.tree.Tree;

public class Solution897 {
    TreeNode resultRoot;
    TreeNode resultTail;

    public void doTravel(TreeNode tmpNode){
        if(tmpNode==null){
            return;
        }
        doTravel(tmpNode.left);
        if(resultRoot.val==-1) {
            resultRoot.val=tmpNode.val;
        }
        else{
            resultTail.right=new TreeNode(tmpNode.val);
            resultTail=resultTail.right;
        }
        doTravel(tmpNode.right);
    }

    public TreeNode increasingBST(TreeNode root) {
        resultRoot=new TreeNode(-1);
        resultTail=resultRoot;
        doTravel(root);
        return resultRoot;
    }
}
