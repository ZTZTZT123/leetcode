import java.util.*;

public class Solution399 {

    class EdgeInfo{
        String upStr;
        String downStr;
        double value;

        public EdgeInfo(String upStr, String downStr, double value) {
            this.upStr = upStr;
            this.downStr = downStr;
            this.value = value;
        }
    }

    Map<String, List<EdgeInfo>> graph = new HashMap<>();

    private Double dfs(String tmpStr, String targetStr, double value, Set<String> pathStrSet){
        if(targetStr.equals(tmpStr)){
            return value;
        }
        List<EdgeInfo> edgeInfoList = graph.get(tmpStr);
        for (EdgeInfo edgeInfo : edgeInfoList) {
            if(!pathStrSet.contains(edgeInfo.downStr)){
                pathStrSet.add(edgeInfo.downStr);
                Double valueRes = dfs(edgeInfo.downStr, targetStr, value * edgeInfo.value, pathStrSet);
                if(valueRes != null){
                    return valueRes;
                }
                pathStrSet.remove(edgeInfo.downStr);
            }
        }
        return null;
    }

    public double[] calcEquation(List<List<String>> equations, double[] values, List<List<String>> queries) {
        for (int i = 0; i < equations.size(); i++) {
            List<String> equation = equations.get(i);
            String upStr = equation.get(0), downStr = equation.get(1);
            double value = values[i];
            if(!graph.containsKey(upStr)){
                graph.put(upStr, new ArrayList<>());
            }
            if(!graph.containsKey(downStr)){
                graph.put(downStr, new ArrayList<>());
            }
            graph.get(upStr).add(new EdgeInfo(upStr, downStr, value));
            graph.get(downStr).add(new EdgeInfo(downStr, upStr, 1 / value));
        }

        double[] res = new double[queries.size()];
        for (int i = 0; i < queries.size(); i++) {
            List<String> query = queries.get(i);
            String upStr = query.get(0), downStr = query.get(1);
            if(!graph.containsKey(upStr) || !graph.containsKey(downStr)){
                res[i] = -1;
                continue;
            }
            Double value = dfs(upStr, downStr, 1, new HashSet<>());
            res[i] = value == null ? -1 : value;
        }
        return res;
    }

    public static void main(String[] args) {
        Solution399 s = new Solution399();
        s.calcEquation(Arrays.asList(Arrays.asList("a", "b"), Arrays.asList("b", "c")), new double[]{2,3}, Arrays.asList(Arrays.asList("a", "c"), Arrays.asList("b", "a"), Arrays.asList("x", "x")));
    }
}
