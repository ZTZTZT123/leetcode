import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class Solution260 {
    public int[] singleNumber(int[] nums) {
        Set<Integer> res = new HashSet<>();
        for (int num:nums) {
            if(res.contains(num)){
                res.remove(num);
            }
            else{
                res.add(num);
            }
        }
        int[] resArr = new int[res.size()];
        Iterator<Integer> it = res.iterator();
        for (int i = 0; i < resArr.length && it.hasNext(); i++) {
            resArr[i] = it.next();
        }
        return resArr;
    }
}
