public class Solution154 {
    public int findMin(int[] nums) {
        int left=0;
        int right=nums.length-1;
        while(left<=right){
            int mid=(left+right)/2;
            int tmpNum=nums[mid];
            int rightNum=nums[right];
            if(mid==right){
                return tmpNum;
            }
            else if(tmpNum<rightNum){
                right=mid;
            }
            else if(tmpNum>rightNum){
                left=mid+1;
            }
            else{
                right--;
            }
        }
        return -1;
    }
}
