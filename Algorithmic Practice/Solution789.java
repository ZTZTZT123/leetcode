public class Solution789 {

    private int getManhattanDistance(int[] start, int[] end){
        return Math.abs(start[0]-end[0]) + Math.abs(start[1]-end[1]);
    }

    public boolean escapeGhosts(int[][] ghosts, int[] target) {
        int userDistance = getManhattanDistance(new int[]{0, 0}, target);
        for (int[] ghost:ghosts) {
            int ghostDistance = getManhattanDistance(ghost, target);
            if(ghostDistance <= userDistance){
                return false;
            }
        }
        return true;
    }
}
