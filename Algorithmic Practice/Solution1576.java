public class Solution1576 {

    private boolean doModCharArr(char[] arr, int startInd){
        int tmpInd = startInd;
        boolean isNeedMod = false;
        for (; tmpInd < arr.length; tmpInd++) {
            if(arr[tmpInd] == '?'){
                isNeedMod = true;
                break;
            }
        }
        if(!isNeedMod){
            return true;
        }
        char lastC = tmpInd == 0 ? '?' : arr[tmpInd - 1];
        char nextC = tmpInd == arr.length - 1 ? '?' : arr[tmpInd + 1];
        for (char c = 'a'; c <= 'z'; c++) {
            if(lastC != c && nextC != c){
                arr[tmpInd] = c;
                if(doModCharArr(arr, tmpInd + 1)){
                    return true;
                }
            }
        }
        arr[tmpInd] = '?';
        return false;
    }

    public String modifyString(String s) {
        char[] arr = s.toCharArray();
        doModCharArr(arr, 0);
        return new String(arr);
    }
}
