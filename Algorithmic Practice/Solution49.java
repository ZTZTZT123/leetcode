import java.util.*;
public class Solution49 {
    public List<List<String>> groupAnagrams(String[] strs) {
        Map<String,List<String>> mark=new HashMap<>();
        for (int i = 0; i < strs.length; i++) {
            char[] chars = strs[i].toCharArray();
            Arrays.sort(chars);
            String sorted_str=String.valueOf(chars);
            List<String> temp=mark.getOrDefault(sorted_str,new ArrayList<>());
            temp.add(strs[i]);
            mark.put(sorted_str,temp);
        }
        return new ArrayList<>(mark.values());
    }

    public static void main(String[] args) {
        Solution49 s=new Solution49();
        System.out.println(s.groupAnagrams(new String[]{"eat", "tea", "tan", "ate", "nat", "bat"}));
    }
}
