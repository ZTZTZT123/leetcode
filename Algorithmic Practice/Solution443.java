public class Solution443 {

    public int compress(char[] chars) {
        char lastC = ' ';
        int repeat = 1;
        StringBuilder sb = new StringBuilder();
        for (char c:chars) {
            if(c == lastC){
                repeat++;
            }
            else{
                if(lastC != ' '){
                    sb.append(lastC);
                }
                if(repeat >= 2) {
                    sb.append(repeat);
                }
                repeat = 1;
            }
            lastC = c;
        }
        if(lastC != ' '){
            sb.append(lastC);
        }
        if(repeat >= 2) {
            sb.append(repeat);
        }
        for (int i = 0; i < sb.length(); i++) {
            chars[i] = sb.charAt(i);
        }
        return sb.length();
    }

    public static void main(String[] args) {
        Solution443 s = new Solution443();
        System.out.println(s.compress(new char[]{'a','a','b','b','c','c','c'}));
    }
}
