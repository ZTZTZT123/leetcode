import java.util.*;

public class Solution554 {
    public int leastBricks(List<List<Integer>> wall) {
        Map<Integer,Integer> mark=new HashMap<>();
        int width=0;
        for (int i=0;i<wall.size();i++) {
            List<Integer> row=wall.get(i);
            int tmpSum=0;
            for (int j = 0; j < row.size(); j++) {
                tmpSum+=row.get(j);
                mark.put(tmpSum,mark.getOrDefault(tmpSum,0)+1);
                if(j==row.size()-1){
                    width=tmpSum;
                }
            }
        }
        int maxCount=0;
        for (Map.Entry et:mark.entrySet()) {
            if((int)et.getKey()==width){
                continue;
            }
            maxCount=Math.max(maxCount, (Integer) et.getValue());
        }
        return wall.size()-maxCount;
    }
}
