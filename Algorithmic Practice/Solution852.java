public class Solution852 {
    public int peakIndexInMountainArray(int[] arr) {
        int left=0, right=arr.length-1;
        while(left<right){
            int mid=left+((right-left)>>1);
            if(arr[mid]<arr[mid+1]){
                left=mid+1;
            }
            else{
                right=mid;
            }
        }
        return left;
    }

    public static void main(String[] args) {
        Solution852 s=new Solution852();
        System.out.println(s.peakIndexInMountainArray(new int[]{3,4,5,1}));
    }
}
