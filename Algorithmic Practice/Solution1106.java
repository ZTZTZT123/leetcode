import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Solution1106 {

    private List<String> getEachSubExpression(String expression){
        List<String> eachSubExpression = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        int leftBracketCount = 0;
        for(int i = 0; i < expression.length(); i++) {
            char c = expression.charAt(i);
            if(c == ',' && leftBracketCount == 1){
                eachSubExpression.add(sb.toString());
                sb = new StringBuilder();
                continue;
            }
            if(c == ')'){
                leftBracketCount--;
            }
            if(leftBracketCount >= 1){
                sb.append(c);
            }
            if(c == '('){
                leftBracketCount++;
            }
        }
        eachSubExpression.add(sb.toString());
        return eachSubExpression;
    }

    public boolean parseBoolExpr(String expression) {
        if("t".equals(expression)){
            return true;
        }
        if("f".equals(expression)){
            return false;
        }
        List<String> eachSubExpression = getEachSubExpression(expression);
        List<Boolean> eachSubRes = eachSubExpression.stream().map(p -> parseBoolExpr(p)).collect(Collectors.toList());
        char firstChar = expression.charAt(0);
        if(firstChar == '!'){
            return !eachSubRes.get(0);
        }
        if(firstChar == '|'){
            return eachSubRes.stream().reduce(false, (a, b) -> a | b);
        }
        if(firstChar == '&') {
            return eachSubRes.stream().reduce(true, (a, b) -> a & b);
        }
        return true;
    }

    public static void main(String[] args){
        Solution1106 s = new Solution1106();
        System.out.println(s.parseBoolExpr("|(f,f,f,t)"));
    }
}
