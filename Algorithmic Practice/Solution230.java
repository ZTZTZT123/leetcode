import java.util.ArrayList;
import java.util.List;

public class Solution230 {
    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode() {}
        TreeNode(int val) { this.val = val; }
        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    int k;
    Integer res;

    private void travel(TreeNode tmpNode){
        if(tmpNode == null || res != null){ return; }
        travel(tmpNode.left);
        if(--k == 0){ res = tmpNode.val; }
        travel(tmpNode.right);
    }

    public int kthSmallest(TreeNode root, int k) {
        this.k = k;
        travel(root);
        return res;
    }
}
