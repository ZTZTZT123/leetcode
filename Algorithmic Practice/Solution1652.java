public class Solution1652 {

    public int getCircleInd(int i, int j, int direct, int len){
        int ind = (i + (j * direct)) % len;
        if(ind < 0){
            return len + ind;
        }
        return ind;
    }


    public int[] getCircleReplaceArr(int[] code, int path, int direct){
        int len = code.length;
        int[] res = new int[len];
        for (int i = 0; i < len; i++) {
            int value = 0;
            for (int j = 1; j <= path; j++) {  
                value += code[getCircleInd(i, j, direct, len)];
            }
            res[i] = value;
        }
        return res;
    }

    public int[] decrypt(int[] code, int k) {
        if(k > 0) {
            return getCircleReplaceArr(code, Math.abs(k), 1);
        }
        else if(k < 0){
            return getCircleReplaceArr(code, Math.abs(k), -1);
        }
        else {
            return new int[code.length];
        }
    }
}
