import java.util.HashSet;
import java.util.Set;

public class Solution231 {
    static Set<Integer> mark;
    static{
        mark=new HashSet<>();
        long tmpNum=1;
        while(tmpNum<=Integer.MAX_VALUE){
            mark.add((int) tmpNum);
            tmpNum*=2;
        }
    }
    public boolean isPowerOfTwo(int n) {
        return mark.contains(n);
    }
}
