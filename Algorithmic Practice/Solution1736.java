public class Solution1736 {
    private String processHour(String hour){
        char[] hourArr = hour.toCharArray();
        if(hourArr[0] == '?'){
            hourArr[0] = '4'<=hourArr[1]&&hourArr[1]<='9'? '1':'2';
        }
        if(hourArr[1] == '?'){
            hourArr[1] = hourArr[0] == '2'? '3' : '9';
        }
        return new String(hourArr);
    }

    private String processMinute(String minute){
        char[] minuteArr = minute.toCharArray();
        if(minuteArr[0] == '?'){
            minuteArr[0] = '5';
        }
        if(minuteArr[1] == '?'){
            minuteArr[1] = '9';
        }
        return new String(minuteArr);
    }

    public String maximumTime(String time) {
        String[] strArr = time.split(":");
        return processHour(strArr[0])+":"+processMinute(strArr[1]);
    }
}
