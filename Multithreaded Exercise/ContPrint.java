import org.w3c.dom.ls.LSOutput;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class PrintOneThread implements Runnable{
    @Override
    public void run() {
        try {
            Foo.staticFoo.first(new Runnable() {
                @Override
                public void run() {
                    System.out.println(1);
                }
            });
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}



class PrintTwoThread implements Runnable{
    @Override
    public void run() {
        try {
            Foo.staticFoo.second(new Runnable() {
                @Override
                public void run() {
                    System.out.println(2);
                }
            });
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}



class PrintThreeThread implements Runnable{
    @Override
    public void run() {
        try {
            Foo.staticFoo.third(new Runnable() {
                @Override
                public void run() {
                    System.out.println(3);
                }
            });
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}



class Foo {

    public static Foo staticFoo=new Foo();

    int num;
    Lock lock;
    //精确的通知和唤醒线程
    Condition condition1, condition2, condition3;

    public Foo() {
        num = 1;
        lock = new ReentrantLock();
        condition1 = lock.newCondition();
        condition2 = lock.newCondition();
        condition3 = lock.newCondition();
    }

    public void first(Runnable printFirst) throws InterruptedException {
        lock.lock();
        try {
            while (num != 1) {
                condition1.await();
            }
            // printFirst.run() outputs "first". Do not change or remove this line.
            printFirst.run();
            num = 2;
            condition2.signal();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public void second(Runnable printSecond) throws InterruptedException {
        lock.lock();
        try {
            while (num != 2) {
                condition2.await();
            }
            // printSecond.run() outputs "second". Do not change or remove this line.
            printSecond.run();
            num = 3;
            condition3.signal();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public void third(Runnable printThird) throws InterruptedException {
        lock.lock();
        try {
            while (num != 3) {
                condition3.await();
            }
            // printThird.run() outputs "third". Do not change or remove this line.
            printThird.run();
            num = 1;
            condition1.signal();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            Thread pThree=new Thread(new PrintThreeThread());
            pThree.start();
        }
        for (int i = 0; i < 10; i++) {
            Thread pOne=new Thread(new PrintOneThread());
            pOne.start();
        }
        for (int i = 0; i < 10; i++) {
            Thread pTwo=new Thread(new PrintTwoThread());
            pTwo.start();
        }
    }

}
